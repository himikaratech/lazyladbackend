<?php

namespace LazyladRest;

if ((@include 'HTTP/Request2.php') == 'OK') 
    define("Lazylad_USE_CURL", FALSE);
else
    define("Lazylad_USE_CURL", TRUE);

class LazyladError extends \Exception { }

class LazyladRest
{
    private $api;
    private $auth_token;
    private $ch;
    
    function __construct($auth_token, $url = "http://localhost") 
    {
        if ((!isset($auth_token)) || (!$auth_token))
            throw new LazyladError("no auth_token specified");

//         $this->api = $url."/".$version;
		$this->api = $url;
        $this->auth_token = $auth_token;
        $this->ch = NULL;
    }
    
    private function curl_request($method, $path, $params) 
    {
        $url = $this->api.$path;
        
        $this->ch = @curl_init();
        
        if (curl_errno($this->ch))
            return array("status" => 0, "response" => array("error" => curl_error($this->ch)));
               
        $options = array(
            CURLOPT_URL => $url,
            CURLOPT_USERAGENT => "Lazylad/Curl",
            CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_CONNECTTIMEOUT => 30,
            CURLOPT_HEADER => FALSE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_VERBOSE => FALSE,
            CURLOPT_SSL_VERIFYPEER => FALSE);
        
        $headers = array('Authorization: '.$this->auth_token, 'Connection: close');
        
        if ($method === "POST") 
        {
            $json_params = json_encode($params);
            $options[CURLOPT_POSTFIELDS] = $json_params;
            $options[CURLOPT_POST] = TRUE;
            
            array_push($headers, "Content-Type: application/json");
            array_push($headers, 'Content-Length: '.strlen($json_params));
        }
        
        $options[CURLOPT_HTTPHEADER] = $headers;
        curl_setopt_array($this->ch, $options);
        $res = @curl_exec($this->ch);
        
        $status = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        
        if ($res === FALSE) 
        {
            $err = curl_error($this->ch);
            @curl_close($this->ch);
            return array("status" => $status, "response" => array("error" => $err));
        }
        
        @curl_close($this->ch);
        
        $result = json_decode($res, TRUE);
        return array("status" => $status, "response" => $result);
    }
    
    private function http2_request($method, $path, $params) 
    {
        $url = $this->api.$path;
        
        $http_method = \HTTP_Request2::METHOD_POST;
        
        if (!strcmp($method, "GET")) 
            $http_method = \HTTP_Request2::METHOD_GET;
        else if (!strcmp($method, "DELETE")) 
            $http_method = \HTTP_Request2::METHOD_DELETE;
            
        $req = new \HTTP_Request2($url, $http_method);
                
        if ($http_method === \HTTP_Request2::METHOD_POST && $params)
            $req->setBody(json_encode($params));
        
        $req->setAdapter('curl');
        $req->setConfig(array('timeout' => 30, 'ssl_verify_peer' => FALSE));
        
        $req->setHeader(array('Authorization' => $this->auth_token, 
                              'Connection' => 'close', 
                              'User-Agent' => 'Lazylad/http2_request', 
                              'Content-type' => 'application/json'));
        
        $r = $req->send();
        $status = $r->getStatus();
        $body = $r->getbody();
        $response = json_decode($body, true);
        return array("status" => $status, "response" => $response);
    }
            
    private function request($method, $path, $params=array()) 
    {        
        if (Lazylad_USE_CURL === TRUE)
            return $this->curl_request($method, $path, $params);
        
        return $this->http2_request($method, $path, $params);
    }
    
    private function pop($params, $key) 
    {
        $val = $params[$key];
        
        if (!$val)
            throw new LazyladError($key." parameter not found");
        
        unset($params[$key]);
        return $val;
    }
    
    public function makeTransaction($params)
    {        
        $result = $this->request('POST', '/newserver/account/makeTransaction', $params);
        if(200 != $result['status'])
        	return null;
        else 
        	return $result['response'];
    }
    
    public function performReferral($params)
    {        
        $result = $this->request('POST', '/newserver/referral/performReferral', $params);
        if(200 != $result['status'])
        	return null;
        else 
        	return $result['response'];
    }
    
    public function orderPaymentDetails($params)
    {        
        $result = $this->request('POST', '/newserver/account/orderPaymentDetails', $params);
        if(200 != $result['status'])
        	return null;
        else 
        	return $result['response'];
    }

    public function orderOfferDiff($params)
    {
        $result = $this->request('POST', '/newserver/oms/order_offer_diff', $params);
        if(200 != $result['status'])
            return 0;
        else
            return $result['response']['offer_diff'];
    }

    public function getWalletDetails($params)
    {
    	$result = $this->request('POST', '/newserver/account/getWalletDetails', $params);
        if(200 != $result['status'])
        	return null;
        else 
        	return $result['response'];
    }
    
    public function logisticsCreateOrder($params)
    {
    	$result = $this->request('POST', '/newserver/logistics/create_order', $params);
        if(200 != $result['status'])
        	return null;
        else 
        	return $result['response'];
    }
    
	public function verifyCoupon($params)
    {
        $result = $this->request('POST', '/newserver/oms/checkCouponCodeValidityFinal', $params);
        if($result['status']==200 ){
                $result['response']['error'] = 1;
                return $result['response'];
        }
        else{
                $result['response']['error'] = 0;
                return $result['response'];
        }
    }
    public function config_api_call($params)
    {
        $result = $this->request('POST', '/newserver/userapp/configDetails_v2', $params);
        if($result['status']==200 ){
                $result['response']['error'] = 1;
                return $result['response'];
        }
        else{
                $result['response']['error'] = 0;
                return $result['response'];
        }
    }
    public function post_order_final($params)
    {
        $result = $this->request('POST', '/newserver/oms/postOrderDetailsToServerFinal', $params);
        return $result['response'];
    }
    public function confirm_items_service_provider($params)
    {
        $result = $this->request('POST', '/newserver/seller/confirm_items_service_provider_with_category', $params);
        if($result['status']==200 ){
                $result['response']['error'] = 1;
                return $result['response'];
        }
        else{
                $result['response']['error'] = 0;
                return $result['response'];
        }
    }
}

?>
