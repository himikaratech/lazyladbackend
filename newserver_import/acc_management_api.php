<?php

define("ORDER_PAY_WALLET_CUSTOMER",'ORDER_PAY_WALLET_CUSTOMER');
define("ORDER_PAY_ONLINE_CUSTOMER",'ORDER_PAY_ONLINE_CUSTOMER');
define("ORDER_CANCEL_CUSTOMER",'ORDER_CANCEL_CUSTOMER');
define("ORDER_FINISH_CUSTOMER",'ORDER_FINISH_CUSTOMER');
define("ORDER_FINISH_SELLER",'ORDER_FINISH_SELLER');
define("ORDER_CANCEL_SELLER",'ORDER_CANCEL_SELLER');
define("ORDER_RETURN_CUSTOMER",'ORDER_RETURN_CUSTOMER');
define("ORDER_RETURN_SELLER",'ORDER_RETURN_SELLER');
define("REDEEM_CASH_COUPON",'REDEEM_CASH_COUPON');
define("RECHARGE",'RECHARGE');
define("SETTLE_UP",'SETTLE_UP');
define("REFFERAL_BONUS",'REFFERAL_BONUS');

define('AM_RS_DB_USERNAME', 'anguluxf');
define('AM_RS_DB_PASSWORD', 'android*15');
define('AM_RS_DB_HOST', 'localhost');
define('AM_RS_DB_NAME', 'account_referral');

class AM_RS_DbConnect {

    private $conn;

    function __construct() {        
    }

    /**
     * Establishing database connection
     * @return database connection handler
     */
    function connect() {

        // Connecting to mysql database
        $this->conn = new mysqli(AM_RS_DB_HOST, AM_RS_DB_USERNAME, AM_RS_DB_PASSWORD, AM_RS_DB_NAME);

        // Check for database connection error
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }

        // returing connection resource
        return $this->conn;
    }

}

class AM_RS_DbHandler {
    private $conn;

    function __construct() {
    	$conn = null;
    	
        // opening db connection
        $db = new AM_RS_DbConnect();
        $this->conn = $db->connect();
    }
    
    function __destruct() {
       if($this->conn)
       		$this->conn->close();
       	$this->conn = null;
   	}

}
?>