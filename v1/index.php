<?php
require_once '../include/DbHandler.php';
require_once '../utilities/helper_functions.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
// User id from db - Global Variable
$user_id = NULL;
/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }

    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Required field(s) ' . substr($error_fields, 0, -2) . ' is missing or empty';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email address is not valid';
        echoRespnse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoRespnse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

//********* Some Utility Calls **********
function echoDummySuccessResponse() {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status(200);

    // setting response content type to json
    $app->contentType('application/json');

	$response = array();
	$response["error"] = 1;
	$response["data_recieved"] = 1;
    echo json_encode($response);
}

function echoDummyFailureResponse() {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status(200);

    // setting response content type to json
    $app->contentType('application/json');

	$response = array();
	$response["error"] = 1;
	$response["data_recieved"] = 0;
    echo json_encode($response);
}

//**********************************************

/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->post('/register', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('name', 'email', 'password', 'latitude', 'longitude', 'area', 'city', 'number', 'serviceType'));

    $response = array();

    // reading post params
    $name = $app->request->post('name');
    $email = $app->request->post('email');
    $password = $app->request->post('password');
    $latitude = $app->request->post('latitude');
    $longitude = $app->request->post('longitude');
    $area = $app->request->post('area');
    $city = $app->request->post('city');
    $number = $app->request->post('number');
    $serviceType = $app->request->post('serviceType');

    // validating email address
    validateEmail($email);

    $db = new DbHandler();
    $res = $db->createServiceProvider($name, $email, $password, $latitude, $longitude, $area, $city, $number, $serviceType);

    if ($res == USER_CREATED_SUCCESSFULLY) {
        $response["error"] = false;
        $response["message"] = "You are successfully registered";
        echoRespnse(201, $response);
    } else if ($res == USER_CREATE_FAILED) {
        $response["error"] = true;
        $response["message"] = "Oops! An error occurred while registereing";
        echoRespnse(200, $response);
    } else if ($res == USER_ALREADY_EXISTED) {
        $response["error"] = true;
        $response["message"] = "Sorry, this email already existed";
        echoRespnse(200, $response);
    }
});

$app->get('/hello', function() {

echo 'hello';
});

$app->get('/cities', function()  {
    $response = array();
    $db = new DbHandler();
    $response=$db->getCities();
    echoRespnse(200, $response);

});

$app->get('/ping', function()  {
    //$response = array();
    //$db = new DbHandler();
    $response="pong";
    $response = array();
    $response= "pong";
    echoRespnse(200, $response);

});


/*
API Name : getCities
Type : GET
Input-Parameters : None
Response
A) 
Json - {“error” : 0}
Meaning - Some Error on server, query or database. Request failed
B) 
Json - {
“error: 1
cities: [
		{
			id: "1"
			city_code: "1"
			city_name: "Gurgaon"
		},
		{
			id: "3"
			city_code: "3"
			city_name: "Chandigarh"
		}
	]
}
Meaning - success as shown by “error” equals 1 and cities array shows the details
*/
$app->get('/getCities', function() use($app) {
    $response = array();
    $db = new DbHandler();
    $response=$db->getCitiesFinal();
    echoRespnse(200, $response);

});

$app->get('/getCitiesforSeller', function() use($app) {
    $response = array();
    $db = new DbHandler();
    $response=$db->getCitiesforSeller();
    echoRespnse(200, $response);

});

$app->post('/areas', function() use ($app) {
    $city_id = "1";

    $response = array();
    $db = new DbHandler();
    $response=$db->getAreas($city_id);
    echoRespnse(200, $response);

});

/*
API Name : /getAreas/:cityCode
Type : GET
Input-Parameters : an Integer in url itself

Response
A) if cityCode is not a valid integer city code in database
Json - 
{
“error”: 1
“areas”: [0] //empty array
}

B) if cityCode is a valid integer city code in database
Json - {
“error”: 1
“areas”: [
		{
			id: 62
			area_code: "62"
			area_name: "Sector 1"
			city_code: "3"
		},
		{
			id: 63
			area_code: "63"
			area_name: "Sector 2"
			city_code: "3"
		}
	]
}
Meaning - success and areas array shows the details
*/
$app->get('/getAreas/:cityCode', function($city_code) use ($app) {
    

    $response = array();
    $db = new DbHandler();
    $response=$db->getAreasInCity($city_code);
    echoRespnse(200, $response);

});

$app->get('/getAreasSeller/:cityCode', function($city_code) use ($app) {
    

    $response = array();
    $db = new DbHandler();
    $response=$db->getAreasInCitySeller($city_code);
    echoRespnse(200, $response);

});

$app->get('/getAreasforSeller/:cityCode/:sellerAreaCode', function($city_code,$seller_area_code) use ($app) {
    

    $response = array();
    $db = new DbHandler();
    $response=$db->getAreasInCityforSeller($city_code,$seller_area_code);
    echoRespnse(200, $response);

});

$app->post('/itemsManagSystem', function() use ($app) {
    $serv_prov_id = $app->request()->post('servProv_id');

    $response = array();
    $db = new DbHandler();
    $response=$db->getItemsManagSystem($serv_prov_id);
    echoRespnse(200, $response);

});


$app->post('/currentOrders', function() use ($app) {
    $serv_prov_id = $app->request()->post('servProv_id');
    $reg_id=$app->request()->post('reg_id');
    $response = array();
    $db = new DbHandler();
    $response=$db->getCurrentOrders($serv_prov_id, $reg_id);
    echoRespnse(200, $response);

});

$app->post('/currentOrdersLimited', function() use ($app) {
    $serv_prov_id = $app->request()->post('servProv_id');
    $before_order_code = $app->request()->post('before_order_code');
    $reg_id=$app->request()->post('reg_id');
    $response = array();
    $db = new DbHandler();
    $response=$db->getCurrentOrdersLimited($serv_prov_id, $reg_id, $before_order_code);
    echoRespnse(200, $response);

});

$app->post('/currentOrdersLimited_v2', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$serv_prov_id = arrayValue($data,'servProv_id');
    	$before_order_code = arrayValue($data,'before_order_code');
    	$reg_id = arrayValue($data,'reg_id');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$serv_prov_id = $app->request()->post('servProv_id');
    	$before_order_code = $app->request()->post('before_order_code');
    	$reg_id=$app->request()->post('reg_id');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 
	
    $response = array();
    $db = new DbHandler();
    $response=$db->getCurrentOrdersLimited_v2($serv_prov_id, $reg_id, $before_order_code);
    echoRespnse(200, $response);

});

$app->post('/scheduledOrdersLimited', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$serv_prov_id = arrayValue($data,'servProv_id');
    	$before_order_code = arrayValue($data,'before_order_code');
    	$reg_id = arrayValue($data,'reg_id');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$serv_prov_id = $app->request()->post('servProv_id');
    	$before_order_code = $app->request()->post('before_order_code');
    	$reg_id=$app->request()->post('reg_id');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 
    
    $response = array();
    $db = new DbHandler();
    $response=$db->getScheduledOrdersLimited($serv_prov_id, $reg_id, $before_order_code);
    echoRespnse(200, $response);

});

$app->post('/scheduledOrdersLimited_v2', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$serv_prov_id = arrayValue($data,'servProv_id');
    	$before_order_code = arrayValue($data,'before_order_code');
    	$reg_id = arrayValue($data,'reg_id');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$serv_prov_id = $app->request()->post('servProv_id');
    	$before_order_code = $app->request()->post('before_order_code');
    	$reg_id=$app->request()->post('reg_id');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 
    
    $response = array();
    $db = new DbHandler();
    $response=$db->getScheduledOrdersLimited_v2($serv_prov_id, $reg_id, $before_order_code);
    echoRespnse(200, $response);

});

$app->post('/scheduledOrdersForLazyLadsLimited', function() use ($app) {

    $before_order_code = $app->request()->post('before_order_code');

    $response = array();
    $db = new DbHandler();
    $response=$db->getScheduledOrdersForLazyLadsLimited($before_order_code);
    echoRespnse(200, $response);

});

$app->post('/scheduled_order_internal_lazylad', function() use ($app) {

    $before_order_code = $app->request()->post('before_order_code');
    $order_filter_str = $app->request()->post('order_filter');
    
    $order_filter = json_decode($order_filter_str,true);

    $response = array();
    $db = new DbHandler();
    $response=$db->getScheduledOrdersForLazyLadsLimited_v2($before_order_code, $order_filter);
    echoRespnse(200, $response);

});

$app->post('/searchOrdersForLazyLadsLimited', function() use ($app) {

    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$search_string = arrayValue($data,'search_string');
    	$previous_order_code = arrayValue($data,'previous_order_code');
    	$order_filter_str = arrayValue($data,'order_filter');
    	
    	$order_filter = json_decode($order_filter_str,true);
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
	
		$search_string = $app->request()->post('search_string');
    	$previous_order_code = $app->request()->post('previous_order_code');
    	$order_filter_str = $app->request()->post('order_filter');
    
    	$order_filter = json_decode($order_filter_str,true);
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 

    $response = array();
    $db = new DbHandler();
    $response=$db->getSearchOrdersForLazyLadsLimited($search_string, $previous_order_code, $order_filter);
    echoRespnse(200, $response);

});

$app->post('/currentOrdersForLazyLads', function() use ($app) {
    $response = array();
    $db = new DbHandler();
    $response=$db->getCurrentOrdersForLazyLads();
    echoRespnse(200, $response);

});

$app->post('/current_order_internal_lazylad', function() use ($app) {

	$before_order_code = $app->request()->post('before_order_code');
	$order_filter_str = $app->request()->post('order_filter');
	
	$order_filter = json_decode($order_filter_str,true);

    $response = array();
    $db = new DbHandler();
    $response=$db->getCurrentOrdersForLazyLadsLimited($before_order_code, $order_filter);
    echoRespnse(200, $response);

});

$app->post('/currentOrdersDetails', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$serv_prov_id = arrayValue($data,'servProv_id');
    	$current_order_code = arrayValue($data,'currentOrder_code');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$serv_prov_id = $app->request()->post('servProv_id');
    	$current_order_code = $app->request()->post('currentOrder_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 

    $response = array();
    $db = new DbHandler();
    $response=$db->getCurrentOrdersDetails($serv_prov_id, $current_order_code);
    echoRespnse(200, $response);

});

$app->post('/currentOrdersDetailsForLazyLads', function() use ($app) {
    $req = $app->request;
    $mtype = $req->getMediaType();
    if($mtype == "application/json"){
        //For application/json
        $data = json_decode($app->request->getBody(),true);
        $current_order_code = arrayValue($data,'currentOrder_code');
    }
    elseif($mtype == "application/x-www-form-urlencoded"){
        $current_order_code = $app->request()->post('currentOrder_code');
    }
    else{
        echoRespnse(400, "Bad Request");
        return;
    } 
    $response = array();
    $db = new DbHandler();
    $response=$db->getCurrentOrdersDetailsForLazyLads($current_order_code);
    echoRespnse(200, $response);

});

$app->post('/pendingOrdersDetails', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$serv_prov_id = arrayValue($data,'servProv_id');
    	$pending_order_code = arrayValue($data,'pendingOrder_code');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$serv_prov_id = $app->request()->post('servProv_id');
    	$pending_order_code = $app->request()->post('pendingOrder_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 

    $response = array();
    $db = new DbHandler();
    $response=$db->getPendingOrdersDetails($serv_prov_id, $pending_order_code);
    echoRespnse(200, $response);

});

$app->post('/pendingOrdersDetailsForLazyLads', function() use ($app) {
    $req = $app->request;
    $mtype = $req->getMediaType();
    if($mtype == "application/json"){
        //For application/json
        $data = json_decode($app->request->getBody(),true);
        $pending_order_code = arrayValue($data,'pendingOrder_code');
    }
    elseif($mtype == "application/x-www-form-urlencoded"){
        $pending_order_code = $app->request()->post('pendingOrder_code');
    }
    else{
        echoRespnse(400, "Bad Request");
        return;
    } 

    $response = array();
    $db = new DbHandler();
    $response=$db->getPendingOrdersDetailsForLazyLads($pending_order_code);
    echoRespnse(200, $response);

});


$app->post('/deliveredOrdersDetails', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$serv_prov_id = arrayValue($data,'servProv_id');
    	$delivered_order_code = arrayValue($data,'deliveredOrder_code');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$serv_prov_id = $app->request()->post('servProv_id');
    	$delivered_order_code = $app->request()->post('deliveredOrder_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 

    $response = array();
    $db = new DbHandler();
    $response=$db->getDeliveredOrdersDetails($serv_prov_id, $delivered_order_code);
    echoRespnse(200, $response);

});

$app->post('/deliveredOrdersDetailsForLazyLads', function() use ($app) {
    $delivered_order_code = $app->request()->post('deliveredOrder_code');

    $response = array();
    $db = new DbHandler();
    $response=$db->getDeliveredOrdersDetailsForLazyLads($delivered_order_code);
    echoRespnse(200, $response);

});

/*

API Name : /getUserPreviousOrderDetails/:orderCode/:userCode
Type : GET

Input-Parameters : two integer orderCode and userCode(user id) in url itself

Response

A) 
Case: When userCode or orderCode is wrong or the order did not contain any items or when this order is not from this user 
Json - 
{
error: 1
previous_orders_details: [0]
}

B)

Case: Otherwise
Json - 
{
error: 1
previous_orders_details: 
	[
		{
			item_code: "3387"
			item_name: "01. kid's 1st. activity 3+ - environment"
			item_img_flag: 1
			item_img_address: "http://www.angulartechnologies.com/item_images/de0418fbe9189615afa6040e59e53458.image.129x175.jpg"
			item_short_desc: ""
			item_quantity: "1"
			item_cost: 90
			item_description: "Books"
		},
		{
			item_code: "3388"
			item_name: "02. kid's 1st. activity 3+ - good habits"
			item_img_flag: 1
			item_img_address: "http://www.angulartechnologies.com/item_images/897dbca1143f3faea2fec6219bf1df07.image.129x175.jpg"
			item_short_desc: ""
			item_quantity: "1"
			item_cost: 90
			item_description: "Books"
		},
		{
			item_code: "2737"
			item_name: "Letter Receipt Register (200 Pgs)"
			item_img_flag: 1
			item_img_address: "http://www.angulartechnologies.com/item_images/f51911261a82fcfc95c61a7cebde95ad.image.175x171.jpg"
			item_short_desc: ""
			item_quantity: "1"
			item_cost: 271.43
			item_description: "Copy & Register"
		}
	]
}
*/

$app->get('/getUserPreviousOrderDetails/:orderCode/:userCode', function($order_code, $user_code) use ($app) {

    $response = array();
   
    $db = new DbHandler();
    $response=$db->getUserPreviousOrdersDetails($order_code, $user_code);
    echoRespnse(200, $response);

});

/*

API Name : /getConfirmedPreviousOrderDetails/:orderCode/:userCode
Type : GET

Input-Parameters : two integer orderCode and userCode(user id) in url itself

Response

A) 
Case: When userCode or orderCode is wrong or the order did not contain any items or 
when this order is not from this user or when seller confirmed order with 0 items 
Json - 
{
error: 1
previous_orders_confirmed_details: [0]
}

B)

Case: Otherwise
Json - 
{
error: 1
previous_orders_confirmed_details: 
	[
		{
			item_code: "3387"
			item_name: "01. kid's 1st. activity 3+ - environment"
			item_img_flag: 1
			item_img_address: "http://www.angulartechnologies.com/item_images/de0418fbe9189615afa6040e59e53458.image.129x175.jpg"
			item_short_desc: ""
			item_quantity: "1"
			item_cost: 90
			item_description: "Books"
		},
		{
			item_code: "3388"
			item_name: "02. kid's 1st. activity 3+ - good habits"
			item_img_flag: 1
			item_img_address: "http://www.angulartechnologies.com/item_images/897dbca1143f3faea2fec6219bf1df07.image.129x175.jpg"
			item_short_desc: ""
			item_quantity: "1"
			item_cost: 90
			item_description: "Books"
		},
		{
			item_code: "2737"
			item_name: "Letter Receipt Register (200 Pgs)"
			item_img_flag: 1
			item_img_address: "http://www.angulartechnologies.com/item_images/f51911261a82fcfc95c61a7cebde95ad.image.175x171.jpg"
			item_short_desc: ""
			item_quantity: "1"
			item_cost: 271.43
			item_description: "Copy & Register"
		}
	]
}
*/
$app->get('/getConfirmedPreviousOrderDetails/:orderCode/:userCode', function($order_code, $user_code) use ($app) {

    $response = array();
   
    $db = new DbHandler();
    $response=$db->getConfirmedPreviousOrdersDetails($order_code, $user_code);
    echoRespnse(200, $response);

});

$app->post('/DeliveredOrders', function() use ($app) {
    $serv_prov_id = $app->request()->post('servProv_id');

    $response = array();
    $db = new DbHandler();
    $response=$db->getDeliveredOrders($serv_prov_id);
    echoRespnse(200, $response);

});

$app->post('/DeliveredOrdersforDayDate', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$serv_prov_id = arrayValue($data,'servProv_id');
    	$day_of_orders = arrayValue($data,'day');
    	$date_of_orders = arrayValue($data,'date');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$serv_prov_id = $app->request()->post('servProv_id');
    	$day_of_orders = $app->request()->post('day');
    	$date_of_orders = $app->request()->post('date');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 

    $response = array();
    $db = new DbHandler();
    $response=$db->getDeliveredOrdersforDayDate($serv_prov_id,$day_of_orders,$date_of_orders);
    echoRespnse(200, $response);

});

$app->post('/DeliveredOrdersForLazyLads', function() use ($app) {

    $response = array();
    $db = new DbHandler();
    $response=$db->getDeliveredOrdersForLazyLads();
    echoRespnse(200, $response);

});

$app->post('/delivered_order_internal_lazylad', function() use ($app) {

	$before_order_code = $app->request()->post('before_order_code');
	$order_filter_str = $app->request()->post('order_filter');
	
	$order_filter = json_decode($order_filter_str,true);

    $response = array();
    $db = new DbHandler();
    $response=$db->getDeliveredOrdersForLazyLadsLimited($before_order_code, $order_filter);
    echoRespnse(200, $response);

});

$app->post('/pendingOrders', function() use ($app) {
    $serv_prov_id = $app->request()->post('servProv_id');

    $response = array();
    $db = new DbHandler();
    $response=$db->getPendingOrders($serv_prov_id);
    echoRespnse(200, $response);

});

$app->post('/pendingOrdersLimited', function() use ($app) {
    $serv_prov_id = $app->request()->post('servProv_id');
    $before_order_code = $app->request()->post('before_order_code');
    $reg_id=$app->request()->post('reg_id');
    $response = array();
    $db = new DbHandler();
    $response=$db->getPendingOrdersLimited($serv_prov_id, $reg_id, $before_order_code);
    echoRespnse(200, $response);

});

$app->post('/pendingOrdersLimited_v2', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$serv_prov_id = arrayValue($data,'servProv_id');
    	$before_order_code = arrayValue($data,'before_order_code');
    	$reg_id = arrayValue($data,'reg_id');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$serv_prov_id = $app->request()->post('servProv_id');
    	$before_order_code = $app->request()->post('before_order_code');
    	$reg_id=$app->request()->post('reg_id');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 
    
    $response = array();
    $db = new DbHandler();
    $response=$db->getPendingOrdersLimited_v2($serv_prov_id, $reg_id, $before_order_code);
    echoRespnse(200, $response);

});

$app->post('/pendingOrdersForLazyLads', function() use ($app) {

    $response = array();
    $db = new DbHandler();
    $response=$db->getPendingOrdersForLazyLads();
    echoRespnse(200, $response);

});

$app->post('/pending_order_internal_lazylad', function() use ($app) {

	$before_order_code = $app->request()->post('before_order_code');
	$order_filter_str = $app->request()->post('order_filter');
	
	$order_filter = json_decode($order_filter_str,true);

    $response = array();
    $db = new DbHandler();
    $response=$db->getPendingOrdersForLazyLadsLimited($before_order_code, $order_filter);
    echoRespnse(200, $response);

});

$app->post('/confirmCurrentOrders', function() use ($app) {
    $current_order_code = $app->request()->post('current_order_code');
    $user_code= $app->request()->post('user_code');
    $response = array();
    $db = new DbHandler();
    $response=$db->ConfirmCurrentOrder($current_order_code, $user_code);
    echoRespnse(200, $response);

});

$app->post('/confirmCurrentOrderItemDetails', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$JSONARRAY = arrayValue($data,"JsonDataArray");
   
		$jsonObj=json_decode($JSONARRAY, true);
		$current_order_code = arrayValue($data,'current_order_code');
		$user_code= arrayValue($data,'user_code');
		$serv_prov_code= arrayValue($data,'serv_prov_id');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$JSONARRAY = $app->request()->post("JsonDataArray");
   
		$jsonObj=json_decode($JSONARRAY, true);
		$current_order_code = $app->request()->post('current_order_code');
		$user_code= $app->request()->post('user_code');
		$serv_prov_code= $app->request()->post('serv_prov_id');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 
    
    $response = array();
    $db = new DbHandler();
    $response=$db->ConfirmCurrentOrderItemDetails($jsonObj, $current_order_code, $user_code, $serv_prov_code);
    echoRespnse(200, $response);

});

$app->post('/confirmCurrentOrderItemDetails_v2', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$JSONARRAY = arrayValue($data,"JsonDataArray");
   
		$jsonObj=json_decode($JSONARRAY, true);
		$current_order_code = arrayValue($data,'current_order_code');
		$user_code= arrayValue($data,'user_code');
		$serv_prov_code= arrayValue($data,'serv_prov_id');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$JSONARRAY = $app->request()->post("JsonDataArray");
   
		$jsonObj=json_decode($JSONARRAY, true);
		$current_order_code = $app->request()->post('current_order_code');
		$user_code= $app->request()->post('user_code');
		$serv_prov_code= $app->request()->post('serv_prov_id');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 
    
    $response = array();
    $db = new DbHandler();
    $response=$db->ConfirmCurrentOrderItemDetails_v2($jsonObj, $current_order_code, $user_code, $serv_prov_code);
    echoRespnse(200, $response);

});

$app->post('/confirmCurrentOrderItemDetailsForLazyLads', function() use ($app) {
    $JSONARRAY = $app->request()->post("JsonDataArray");
   
    $jsonObj=json_decode($JSONARRAY, true);
    $current_order_code = $app->request()->post('current_order_code');
    $user_code= $app->request()->post('user_code');
    $response = array();
    $db = new DbHandler();
    $response=$db->ConfirmCurrentOrderItemDetailsForLazyLads($jsonObj, $current_order_code, $user_code);
    echoRespnse(200, $response);

});

$app->post('/confirmPendingOrders', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$pending_order_code = arrayValue($data,'pending_order_code');
    	$user_code = arrayValue($data,'user_code');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$pending_order_code = $app->request()->post('pending_order_code');
    	$user_code= $app->request()->post('user_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    
    $response = array();
    $db = new DbHandler();
    $response=$db->ConfirmPendingOrder($pending_order_code, $user_code);
    echoRespnse(200, $response);

});

$app->post('/confirmPendingOrdersItemDetailsForLazyLads', function() use ($app) {
    $pending_order_code = $app->request()->post('pending_order_code');
    $user_code= $app->request()->post('user_code');
    $response = array();
    $db = new DbHandler();
    $response=$db->ConfirmPendingOrderItemDetailsForLazyLads($pending_order_code, $user_code);
    echoRespnse(200, $response);

});

$app->get('/serviceTypes', function() use ($app) {
    $response = array();
    $db = new DbHandler();
    $response=$db->getServiceTypes();
    echoRespnse(200, $response);

});

$app->get('/serviceTypes_v2', function() use ($app) {
    $response = array();
    $db = new DbHandler();
    $response=$db->getServiceTypes_v2();
    echoRespnse(200, $response);

});

$app->post('/serviceProvidersForServiceType', function() use ($app) {
    
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$stcode=$data['st_code'];
		$areaname=$data['area_name'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$stcode = $app->request->post('st_code');
		$areaname = $app->request->post('area_name');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}

    $response = array();
    $db = new DbHandler();
    $response=$db->getServiceProvidersForServiceType($stcode, $areaname);
    echoRespnse(200, $response);

});

/*
API Name : /serviceProvidersForServiceTypeGSON/:st_code/:area_code
Type : GET
Input-Parameters : two Integers in url itself
a) st_code is Service type hardcoded in app, at present 1 to 6 for categories in home screen
b) area_code - As they are global so no city code required

Response
A) if st_code or area_code is not valid integers and service type, area code in database respectively
Json - 
{
error: 1
service_providers: [0] // empty array
}

B) if cityCode is a valid integer city code in database
Json - {
“error”: 1
“service_providers”: [
		{
			id: 1 // does not mean anything
			sp_code: "27"
			sp_name: "Super Needs Market"
			sp_number: "9818698736"
			sp_type: "1"
			sp_del_time: "90 mins"
			sp_min_order: "200"
		},
		{
			id: 1
			sp_code: "27"
			sp_name: "Super Needs Market"
			sp_number: "9818698736"
			sp_type: "1"
			sp_del_time: "90 mins"
			sp_min_order: "200"
		}
	]
}
Meaning - success and areas array shows the details
*/
$app->get('/serviceProvidersForServiceTypeGSON/:st_code/:area_code', function($st_code, $area_code) use ($app) {
    //$stcode = $app->request->post('st_code');
    //$areaname = $app->request->post('area_name');
    
    
    $response = array();
    //$response["serv_por"]=$st_code;
    //$response["hello"]=$area_name;
    $db = new DbHandler();
    $response=$db->getServiceProvidersForServiceTypeGSON($st_code, $area_code);
    echoRespnse(200, $response);

});

/*
API Name : serviceCategories
Type : POST
Content-Type : application/json or application/x-www-form-urlencoded
input 
A) Json object {“st_code” : “3”}
or
B) form data st_code=3

Response
A)
Json-
{
error: 1
service_categories: [0]
}
Case when st_code value in not valid service type or some other discrepancy in request.

B)
Case: When st_code is valid 
Json - 
{
error: 1
	service_categories: [
		{
			id: 1
			st_code: “1”,
			sc_code: “1”,
			sc_name: "Ration”
		},
		{
			id: 2,
			st_code: “1”,
			sc_code: “2”,
			sc_name: "Biscuit & Namkeen"
		}
	]
}
*/
$app->post('/serviceCategories', function() use ($app) {
	
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$stcode = arrayValue($data, 'st_code');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$stcode = $app->request->post('st_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}

    $response = array();
    $db = new DbHandler();
    $response=$db->getServiceCategory($stcode);
    echoRespnse(200, $response);

});

$app->post('/serviceCategoriesOfferedBySP', function() use ($app) {
   
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$spcode=$data['sp_code'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$spcode = $app->request->post('sp_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
    $response = array();
    $db = new DbHandler();
    $response=$db->getServiceCategoriesOfferedBySP($spcode);
    echoRespnse(200, $response);

});

/*

API Name : cancelUsersPreviousOrder

Type : POST

Content-Type : application/json or application/x-www-form-urlencoded

Input Parameters :
 Json Object
{
	"order_code" : String
} 

Response 
A) Case: When order_code is wrong or there is some error on the database or server and order could not be cancelled
Json- 
{
	error: 0
}
B) Case : When Order successfully cancelled
Json - 
{
	error: 1
}
*/
$app->post('/cancelUsersPreviousOrder', function() use ($app) {
    
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$ordercode=$data['order_code'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$ordercode = $app->request->post('order_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->cancelUserCurrentOrder($ordercode);
    echoRespnse(200, $response);
});

$app->post('/cancelUsersPreviousOrderForDashboard', function() use ($app) {
    
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$ordercode=$data['order_code'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$ordercode = $app->request->post('order_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->cancelUsersPreviousOrderForLazyLads($ordercode);
    echoRespnse(200, $response);
});

$app->post('/checkCouponValidity', function() use ($app) {
    $couponcode = $app->request->post('coupon_code');
    $response = array();
    $db = new DbHandler();
    $response=$db->checkCouponCodeValidity($couponcode);
    echoRespnse(200, $response);

});

/*

API Name : checkCouponValidityFinal

Type : POST

Content-Type : application/json or application/x-www-form-urlencoded

Input Parameters :
 Json Object
{
	"coupon_code" : String
} 

Response 
A) Case: When Coupon code is invalid or when there is some error on the database or server
Json- 
{
	error: 0
	message: "Service Provider Not Valid"
}
B) Case : When coupon code is valid. At present there are two types of coupon determined by coupon_type_id ("1" or "2"). 
"1" means absolute amount discount, "2" means percentage discount. Amount/Percentage is shown by coupon_amount.
Json - 
{
	error: 1
	coupon_type_id: "2"
	coupon_amount: "20"
	minimum_order_amount: "199"
}
*/
$app->post('/checkCouponValidityFinal', function() use ($app) {
    
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$couponcode=$data['coupon_code'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$couponcode = $app->request->post('coupon_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->checkCouponCodeValidityFinal($couponcode);
    echoRespnse(200, $response);

});

$app->post('/checkCouponValidityFinal_v2', function() use ($app) {
    
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$couponcode= arrayValue($data,'coupon_code');
		$user_code= arrayValue($data,'user_code');
		$total_amount= arrayValue($data,'total_amount');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$couponcode = $app->request->post('coupon_code');
		$user_code = $app->request->post('user_code');
		$total_amount = $app->request->post('total_amount');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->verifyCoupon($couponcode, $user_code, $total_amount);
    echoRespnse(200, $response);

});

$app->get('/serviceCategories/:stcode', function($stcode) use ($app) {
    //$stcode = $app->request->post('st_code');
    $response = array();
    $db = new DbHandler();
    $response=$db->getServiceCategory($stcode);
    echoRespnse(200, $response);

});

$app->post('/itemsServiceProviderSpecific', function() use ($app) {
    
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$serv_prov_code=$data['serv_prov_code'];
		$serv_type_code=$data['st_code'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$serv_prov_code = $app->request()->post('serv_prov_code');
		$serv_type_code = $app->request()->post('st_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->getItemsServiceProviderSpecific($serv_prov_code, $serv_type_code);
    echoRespnse(200, $response);

});

$app->post('/itemsServiceProviderCategoryWise', function() use ($app) {

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$serv_prov_code=$data['serv_prov_code'];
		$serv_type_code=$data['st_code'];
		$serv_category_code=$data['sc_code'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$serv_prov_code = $app->request()->post('serv_prov_code');
		$serv_type_code = $app->request()->post('st_code');
		$serv_category_code = $app->request()->post('sc_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
    $response = array();
    $db = new DbHandler();
    $response=$db->getItemsServiceProviderCategoryWise($serv_prov_code, $serv_type_code, $serv_category_code);
    echoRespnse(200, $response);

});

$app->get('/itemsServiceProviderCategoryWise/:serv_prov_code/:serv_type_code/:serv_category_code', function($serv_prov_code, $serv_type_code, $serv_category_code) use ($app) {
    $response = array();
    $db = new DbHandler();
    //$response["serv_prov"]=$serv_prov_code;
    //$response["serv_type"]=$serv_type_code;
    //$response["serv_cat"]=$serv_category_code;
    $response=$db->getItemsServiceProviderCategoryWise($serv_prov_code, $serv_type_code, $serv_category_code);
    echoRespnse(200, $response);

});

$app->get('/itemsServiceProviderCategoryWiseFinal/:serv_prov_code/:serv_type_code/:serv_category_code', function($serv_prov_code, $serv_type_code, $serv_category_code) use ($app) {
    $response = array();
    $db = new DbHandler();
    //$response["serv_prov"]=$serv_prov_code;
    //$response["serv_type"]=$serv_type_code;
    //$response["serv_cat"]=$serv_category_code;
    $response=$db->getItemsServiceProviderCategoryWiseFinal($serv_prov_code, $serv_type_code, $serv_category_code);
    echoRespnse(200, $response);

});


/*

API Name : /itemsServiceProviderCategoryWiseFinalWithMRP/:serv_prov_code/:serv_type_code/:serv_category_code

Type : GET

Input-Parameters : three parameters all of type integer and all are part of url
1) serv_prov_code : seller id
2) serv_type_code : Main service category as shown in home page grid view
3) serv_category_code : service sub category as shown in tab in app

Response

A) 

Case : When serv_prov_code is not a valid integer and valid selled id in database
Json - 
{
	message: "Service Provider Doesn't Exists"
}
B)
Case : When serv_category_code is not a valid integer and valid service sub category code in database 
Json - 
{
	error: 1
	items_service_providers: [0] // empty array
}


B) 
Json - 
{
error: 1
items_service_providers: 
	[
		{
			id: 1309
			item_code: "14"
			sp_code: "3"
			item_type_code: "1"
			item_name: "Maggi Atta 4 pack"
			item_unit: " "
			item_quantity: " "
			item_cost: 95
			item_img_flag: 1
			item_img_address: "http://angulartechnologies.com/item_images/Maggi20Atta20420pack.jpeg"
			item_short_desc: ""
			item_desc: "Ready to Eat"
			item_status: 1
			item_selected: 0
			item_quantity_selected: 0
			item_service_type: "0"
			item_service_category: "3"
			item_mrp: 95
		},
		{
			id: 1310
			item_code: "20"
			sp_code: "3"
			item_type_code: "1"
			item_name: "Maggi Atta 80gms"
			item_unit: " "
			item_quantity: " "
			item_cost: 25
			item_img_flag: 1
			item_img_address: "http://angulartechnologies.com/item_images/Maggi20Atta2080gms.jpeg"
			item_short_desc: ""
			item_desc: "Ready to Eat"
			item_status: 1
			item_selected: 0
			item_quantity_selected: 0
			item_service_type: "0"
			item_service_category: "3"
			item_mrp: 25
		}
	]
}


Meaning - success as shown by “error” equals 1 and items_service_providers array shows the details

*/
$app->get('/itemsServiceProviderCategoryWiseFinalWithMRP/:serv_prov_code/:serv_type_code/:serv_category_code', function($serv_prov_code, $serv_type_code, $serv_category_code) use ($app) {
    $response = array();
    $db = new DbHandler();
    //$response["serv_prov"]=$serv_prov_code;
    //$response["serv_type"]=$serv_type_code;
    //$response["serv_cat"]=$serv_category_code;
    $response=$db->getItemsServiceProviderCategoryWiseFinalWithMRP($serv_prov_code, $serv_type_code, $serv_category_code);
    echoRespnse(200, $response);

});

$app->get('/itemsServiceProviderCategoryWiseFinalWithMRPLimited/:serv_prov_code/:serv_type_code/:serv_category_code', function($serv_prov_code, $serv_type_code, $serv_category_code) use ($app) {
    
    $previous_item_code = $app->request->get("previous_item_code");
    
    $response = array();
    $db = new DbHandler();
    $response=$db->getItemsServiceProviderCategoryWiseFinalWithMRPLimited($serv_prov_code, $serv_type_code, $serv_category_code,
    	$previous_item_code);
    echoRespnse(200, $response);

});

$app->get('/itemsInSpecialOfferServiceProviderCategoryWiseFinalWithMRPLimited/:serv_prov_code/:serv_type_code', function($serv_prov_code, $serv_type_code) use ($app) {

    $previous_item_code = $app->request->get("previous_item_code");

    $response = array();
    $db = new DbHandler();
    $response=$db->getInSpecialOfferItemsServiceProviderCategoryWiseFinalWithMRPLimited($serv_prov_code, $serv_type_code, $previous_item_code);
    echoRespnse(200, $response);

});

$app->get('/searchItemsSPCategoryWiseLimited/:serv_prov_code/:serv_type_code/:serv_category_code', function($serv_prov_code, $serv_type_code, $serv_category_code) use ($app) {
    
    $search_string = $app->request->get("search");
    $previous_item_code = $app->request->get("previous_item_code");
    
    if($previous_item_code == null)
    	$previous_item_code = 0;
    
    $response = array();
    $db = new DbHandler();
    $response=$db->searchItemsSPCategoryWiseLimited($serv_prov_code, $serv_type_code, $serv_category_code,
    $search_string, $previous_item_code);
    echoRespnse(200, $response);

});

$app->get('/itemsServiceProviderCategoryWiseAll/:serv_prov_code/:serv_type_code', function($serv_prov_code, $serv_type_code) use ($app) {
    $response = array();
    $db = new DbHandler();
    //$response["serv_prov"]=$serv_prov_code;
    //$response["serv_type"]=$serv_type_code;
    //$response["serv_cat"]=$serv_category_code;
    $response=$db->getItemsServiceProviderCategoryWiseAll($serv_prov_code, $serv_type_code);
    echoRespnse(200, $response);

});

$app->get('/itemsServiceProviderCategoryWise/:serv_prov_code/:serv_type_code/:serv_category_code/:num_of_items', function($serv_prov_code, $serv_type_code, $serv_category_code, $num_of_items) use ($app) {
    $response = array();
    $db = new DbHandler();
    //$response["serv_prov"]=$serv_prov_code;
    //$response["serv_type"]=$serv_type_code;
    //$response["serv_cat"]=$serv_category_code;
    $response=$db->getItemsServiceProviderCategoryWiseBatchWise($serv_prov_code, $serv_type_code, $serv_category_code, $num_of_items);
    echoRespnse(200, $response);

});

$app->post('/postOrderToServer', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$JSONARRAY=$data['JsonDataArray'];
		$jsonObj=json_decode($JSONARRAY, true);
		$sp_code=$data['sp_code'];
		$st_code=$data['st_code'];
		$user_id=$data['user_id'];
		$user_address_code=$data['user_address_code'];
		$expected_del_time=$data['user_exp_del_time'];
		$total_amount=$data['tot_amount'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$JSONARRAY = $app->request()->post("JsonDataArray");
   
		$jsonObj=json_decode($JSONARRAY, true);
		$sp_code = $app->request()->post("sp_code");
		$st_code = $app->request()->post("st_code");
		$user_id = $app->request()->post("user_id");
		$user_address_code = $app->request()->post("user_address_code");
    
		$expected_del_time = $app->request()->post("user_exp_del_time");
		$total_amount = $app->request()->post("tot_amount");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}

    $response = array();
    $db = new DbHandler();
    $response=$db->postOrderDetailsToServer($jsonObj, $user_id, $user_address_code, $total_amount, $expected_del_time, $sp_code, $st_code);
    echoRespnse(200, $response);

});



/*

API Name : postOrderToServerFinal

Type : POST

Content-Type : application/json or application/x-www-form-urlencoded

Input Parameters :
 Json Object
{
"sp_code" : String // seller id
"st_code" : String // main service type code
"user_id" : String 
"user_exp_del_time" : String 
"user_address_code" : String
"tot_amount" : String // total amount integer in string
"delivery_charges" : String // 20, integer in string, fixed as of now, if order is below min_order_amount of seller details
"discount_amount" : String // double in string as determined by coupon applied
"tot_amount" : String // double in string, total amount after applying discount and delivery charge
"total_items_amount" : String // double in string, total amount before applying discount and delivery charge
"JsonDataArray" : [  // order item details array
	{
	"code" : String
	"itemQuantitySelected" : String
	},
	{
	"code" : String
	"itemQuantitySelected" : String
	},
	{
	"code" : String
	"itemQuantitySelected" : String
	}
]
}
1) user_exp_del_time
It is sent as created by this android code as it is (Today's date)
		Date expectedDeliveryTime = new Date();
        String expDeliveryTime = expectedDeliveryTime.toString();
        
Response 
A) Case: When there is some error on the database or server or with order details. order_code entry will not be present.
Json- 
{
	error: 1
}
B) Case : When order is placed successfully.
Json- 
{
	error: 1
	order_code : String/Number // should be used after converting to integer
}
*/
$app->post('/postOrderToServerFinal', function() use ($app) {
    
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$JSONARRAY=$data['JsonDataArray'];
		$jsonObj=json_decode($JSONARRAY, true);
		$sp_code=$data['sp_code'];
		$st_code=$data['st_code'];
		$user_id=$data['user_id'];
		$user_address_code=$data['user_address_code'];
		$expected_del_time=$data['user_exp_del_time'];
		$total_amount=$data['tot_amount'];
		$delivery_amount=$data['delivery_charges'];
		$discount_amount=$data['discount_amount'];
		$total_items_amount=$data['total_items_amount'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$JSONARRAY = $app->request()->post("JsonDataArray");

		$jsonObj=json_decode($JSONARRAY, true);
		$sp_code = $app->request()->post("sp_code");
		$st_code = $app->request()->post("st_code");
		$user_id = $app->request()->post("user_id");
		$user_address_code = $app->request()->post("user_address_code");
		
		$expected_del_time = $app->request()->post("user_exp_del_time");
		$total_amount = $app->request()->post("tot_amount");
		$delivery_amount=$app->request()->post("delivery_charges");
		$discount_amount=$app->request()->post("discount_amount");
		$total_items_amount=$app->request()->post("total_items_amount");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}

    $response = array();
    $db = new DbHandler();
    $response=$db->postOrderDetailsToServerFinal($jsonObj, $user_id, $user_address_code, $total_amount, $expected_del_time, $sp_code, $st_code, $delivery_amount, $discount_amount, $total_items_amount);
	echoRespnse(200, $response);
});

$app->post('/postOrderToServerFinal_v2', function() use ($app) {
    
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$JSONARRAY=$data['JsonDataArray'];
		$jsonObj=json_decode($JSONARRAY, true);
		$sp_code=$data['sp_code'];
		$st_code=$data['st_code'];
		$user_id=$data['user_id'];
		$user_address_code=$data['user_address_code'];
		$expected_del_time=$data['user_exp_del_time'];
		$total_amount=$data['tot_amount'];
		$delivery_amount=$data['delivery_charges'];
		$discount_amount=$data['discount_amount'];
		$total_items_amount=$data['total_items_amount'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$JSONARRAY = $app->request()->post("JsonDataArray");

		$jsonObj=json_decode($JSONARRAY, true);
		$sp_code = $app->request()->post("sp_code");
		$st_code = $app->request()->post("st_code");
		$user_id = $app->request()->post("user_id");
		$user_address_code = $app->request()->post("user_address_code");
		
		$expected_del_time = $app->request()->post("user_exp_del_time");
		$total_amount = $app->request()->post("tot_amount");
		$delivery_amount=$app->request()->post("delivery_charges");
		$discount_amount=$app->request()->post("discount_amount");
		$total_items_amount=$app->request()->post("total_items_amount");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}

    $response = array();
    $db = new DbHandler();
    $response=$db->postOrderDetailsToServerFinal_v2($jsonObj, $user_id, $user_address_code, $total_amount, $expected_del_time, $sp_code, $st_code, $delivery_amount, $discount_amount, $total_items_amount);
	echoRespnse(200, $response);
});

$app->post('/postOrderToServerFinal_v3', function() use ($app) {
    
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$JSONARRAY=$data['JsonDataArray'];
		$jsonObj=json_decode($JSONARRAY, true);
		$sp_code=$data['sp_code'];
		$st_code=$data['st_code'];
		
		$user_code=$data['user_code'];
		$address_code=$data['address_code'];
		$address_user_code = $data['address_user_code'];
		
		$expected_del_time=$data['user_exp_del_time'];
		$total_amount=$data['tot_amount'];
		$delivery_amount=$data['delivery_charges'];
		$discount_amount=$data['discount_amount'];
		$total_items_amount=$data['total_items_amount'];
		$couponCode=$data["couponCode"];
	}
elseif($mtype == "application/x-www-form-urlencoded"){
		$JSONARRAY = $app->request()->post("JsonDataArray");

		$jsonObj=json_decode($JSONARRAY, true);
		$sp_code = $app->request()->post("sp_code");
		$st_code = $app->request()->post("st_code");
		
		$user_code = $app->request()->post("user_code");
		$address_code = $app->request()->post("address_code");
		$address_user_code = $app->request()->post("address_user_code");
		
		$expected_del_time = $app->request()->post("user_exp_del_time");
		$total_amount = $app->request()->post("tot_amount");
		$delivery_amount=$app->request()->post("delivery_charges");
		$discount_amount=$app->request()->post("discount_amount");
		$total_items_amount=$app->request()->post("total_items_amount");
		$couponCode=$app->request()->post("couponCode");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}

    $response = array();
    $db = new DbHandler();
    $response=$db->postOrderDetailsToServerFinal_v3($jsonObj, $user_code, $address_code, $address_user_code, $total_amount,
     $expected_del_time, $sp_code, $st_code, $delivery_amount, $discount_amount, $total_items_amount, $couponCode);
	echoRespnse(200, $response);
});

$app->post('/postOrderToServerFinal_v4', function() use ($app) {
    
    $req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		// $JSONARRAY = arrayValue($data, 'JsonDataArray');
		$jsonObj=arrayValue($data, 'JsonDataArray');
		
		$order_payment_details_str = arrayValue($data, 'order_payment_details');
		$taxes = arrayValue($data, 'taxes');
		
		$sp_code = arrayValue($data, 'sp_code');
		
		$user_code = arrayValue($data, 'user_code');
		$address_code = arrayValue($data, 'address_code');
		$address_user_code = arrayValue($data, 'address_user_code');
		
		$expected_del_time = arrayValue($data, 'user_exp_del_time');
		$total_amount = arrayValue($data, 'tot_amount');
		$delivery_amount = arrayValue($data, 'delivery_charges');
		$discount_amount = arrayValue($data, 'discount_amount');
		$total_items_amount = arrayValue($data, 'total_items_amount');
		$couponCode = arrayValue($data, "couponCode");
	}
elseif($mtype == "application/x-www-form-urlencoded"){
		// $JSONARRAY = $app->request()->post("JsonDataArray");
		
		$order_payment_details_str = $app->request()->post("order_payment_details");

		$jsonObj=$app->request()->post("JsonDataArray");
		$taxes = $app->request()->post("taxes");
		
		$sp_code = $app->request()->post("sp_code");
		
		$user_code = $app->request()->post("user_code");
		$address_code = $app->request()->post("address_code");
		$address_user_code = $app->request()->post("address_user_code");
		
		$expected_del_time = $app->request()->post("user_exp_del_time");
		$total_amount = $app->request()->post("tot_amount");
		$delivery_amount=$app->request()->post("delivery_charges");
		$discount_amount=$app->request()->post("discount_amount");
		$total_items_amount=$app->request()->post("total_items_amount");
		$couponCode=$app->request()->post("couponCode");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	if(null != $order_payment_details_str)
		$order_payment_details=json_decode($order_payment_details_str, true);
	else 
		$order_payment_details = null;
	
	if(null == $taxes)
		$taxes = 0;
	
    $response = array();
    $db = new DbHandler();
    $response=$db->post_order($jsonObj, $user_code, $address_code, $address_user_code, $total_amount,
     $expected_del_time, $sp_code, $delivery_amount, $discount_amount, $total_items_amount, $couponCode, $order_payment_details_str, $taxes);
	echoRespnse(200, $response);
});


$app->post('/postOrderToServerFinal_v5', function() use ($app) {

    $req = $app->request;
    $mtype = $req->getMediaType();
    if($mtype == "application/json"){
        //For application/json
        $data = json_decode($app->request->getBody(),true);
        $JSONARRAY = arrayValue($data, 'JsonDataArray');
        $jsonObj=json_decode($JSONARRAY, true);

        $order_payment_details_str = arrayValue($data, 'order_payment_details');
        $taxes = arrayValue($data, 'taxes');

        $sp_code = arrayValue($data, 'sp_code');

        $user_code = arrayValue($data, 'user_code');
        $address_code = arrayValue($data, 'address_code');
        $address_user_code = arrayValue($data, 'address_user_code');

        $expected_del_time = arrayValue($data, 'user_exp_del_time');
        $total_amount = arrayValue($data, 'tot_amount');
        $delivery_amount = arrayValue($data, 'delivery_charges');
        $discount_amount = arrayValue($data, 'discount_amount');
        $total_items_amount = arrayValue($data, 'total_items_amount');
        $couponCode = arrayValue($data, "couponCode");
    }
    elseif($mtype == "application/x-www-form-urlencoded"){
        $JSONARRAY = $app->request()->post("JsonDataArray");

        $order_payment_details_str = $app->request()->post("order_payment_details");

        $jsonObj=json_decode($JSONARRAY, true);
        $taxes = $app->request()->post("taxes");

        $sp_code = $app->request()->post("sp_code");

        $user_code = $app->request()->post("user_code");
        $address_code = $app->request()->post("address_code");
        $address_user_code = $app->request()->post("address_user_code");

        $expected_del_time = $app->request()->post("user_exp_del_time");
        $total_amount = $app->request()->post("tot_amount");
        $delivery_amount=$app->request()->post("delivery_charges");
        $discount_amount=$app->request()->post("discount_amount");
        $total_items_amount=$app->request()->post("total_items_amount");
        $couponCode=$app->request()->post("couponCode");
    }
    else{
        echoRespnse(400, "Bad Request");
        return;
    }

    if(null != $order_payment_details_str)
        $order_payment_details=json_decode($order_payment_details_str, true);
    else
        $order_payment_details = null;

    if(null == $taxes)
        $taxes = 0;

    $response = array();
    $db = new DbHandler();
    $response=$db->postOrderDetailsToServerFinal_v5($jsonObj, $user_code, $address_code, $address_user_code, $total_amount,
        $expected_del_time, $sp_code, $delivery_amount, $discount_amount, $total_items_amount, $couponCode, $order_payment_details, $taxes);
    echoRespnse(200, $response);
});


/*

API Name : addNewAddress

Type : POST

Content-Type : application/json or application/x-www-form-urlencoded

Input Parameters :
 Json Object
{
"user_code" : String
"customer_name" : String
"customer_number" : String
"customer_email" : String
"customer_address" : String
}
1) user_code : user code converted to string
2 - 4) self descriptive
5) customer_address : complete address converted to one string

Response 
A) Case: When there is some error on the database or server
Json- 
{
	error: 0
	user_details: [
		{
		"message" : "Error"
		}
	]
}
B) Case : When address is added successfully, user_address_code tells the address code 
corresponding to user. Multiple addresses for user can coexist.
Json - 
{
	error: 1
	user_details: [
		{
		user_code: 10000
		user_address_code: 3
		}
	]
}
*/
$app->post('/addNewAddress', function() use ($app){

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$user_code=$data['user_code'];
		$customer_name=$data['customer_name'];
		$customer_number=$data['customer_number'];
		$customer_email=$data['customer_email'];
		$customer_address=$data['customer_address'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$user_code = $app->request()->post("user_code");
		$customer_name = $app->request()->post("customer_name");
		$customer_number = $app->request()->post("customer_number");
		$customer_email = $app->request()->post("customer_email");
		$customer_address = $app->request()->post("customer_address");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->addNewAddressAndGetAddressCode($user_code, $customer_name, $customer_number, $customer_email, $customer_address);
    echoRespnse(200, $response);


});

$app->post('/addNewAddressDetailed',function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$user_code=$data['user_code'];
		$customer_name=$data['customer_name'];
		$customer_number=$data['customer_number'];
		$customer_email=$data['customer_email'];
		$customer_flat=$data['customer_flat'];
		$customer_subarea=$data['customer_subarea'];
		$customer_area=$data['customer_area'];
		$customer_city=$data['customer_city'];
		
		$address_lat = arrayValue($data, 'address_lat');
		$address_long = arrayValue($data, 'address_long');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$user_code = $app->request()->post("user_code");
		$customer_name = $app->request()->post("customer_name");
		$customer_number = $app->request()->post("customer_number");
		$customer_email = $app->request()->post("customer_email");
		$customer_flat = $app->request()->post("customer_flat");
		$customer_subarea = $app->request()->post("customer_subarea");
		$customer_area = $app->request()->post("customer_area");
		$customer_city = $app->request()->post("customer_city");
		
		$address_lat = $app->request()->post("address_lat");
		$address_long = $app->request()->post("address_long");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->addNewAddressDetailedAndGetAddressCode($user_code, $customer_name, $customer_number, $customer_email,
     $customer_flat,$customer_subarea,$customer_area,$customer_city, $address_lat, $address_long);
    echoRespnse(200, $response);
});

$app->get('/getAllUserAddresses/:user_code',function($user_code) use ($app){

	$response = array();
    $db = new DbHandler();
    $response=$db->getAllUserAddresses($user_code);
    echoRespnse(200,$response);
});

$app->post('/editUserAddressDetailed',function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$user_code=$data['user_code'];
		$address_code=$data['address_code'];
		$customer_name=$data['customer_name'];
		$customer_number=$data['customer_number'];
		$customer_email=$data['customer_email'];
		$customer_flat=$data['customer_flat'];
		$customer_subarea=$data['customer_subarea'];
		$customer_area=$data['customer_area'];
		$customer_city=$data['customer_city'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$user_code = $app->request()->post("user_code");
		$address_code = $app->request()->post("address_code");
		$customer_name = $app->request()->post("customer_name");
		$customer_number = $app->request()->post("customer_number");
		$customer_email = $app->request()->post("customer_email");
		$customer_flat = $app->request()->post("customer_flat");
		$customer_subarea = $app->request()->post("customer_subarea");
		$customer_area = $app->request()->post("customer_area");
		$customer_city = $app->request()->post("customer_city");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->editUserAddressDetailed($user_code, $address_code, $customer_name, $customer_number, $customer_email,
     $customer_flat,$customer_subarea,$customer_area,$customer_city);
    echoRespnse(200, $response);
});

$app->post('/editUserAddressDetailedSynced',function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$changing_user_code=$data['changing_user_code'];
		$user_code=$data['user_code'];
		$address_code=$data['address_code'];
		$customer_name=$data['customer_name'];
		$customer_number=$data['customer_number'];
		$customer_email=$data['customer_email'];
		$customer_flat=$data['customer_flat'];
		$customer_subarea=$data['customer_subarea'];
		$customer_area=$data['customer_area'];
		$customer_city=$data['customer_city'];
		
		$address_lat = arrayValue($data, 'address_lat');
		$address_long = arrayValue($data, 'address_long');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$changing_user_code = $app->request()->post("changing_user_code");
		$user_code = $app->request()->post("user_code");
		$address_code = $app->request()->post("address_code");
		$customer_name = $app->request()->post("customer_name");
		$customer_number = $app->request()->post("customer_number");
		$customer_email = $app->request()->post("customer_email");
		$customer_flat = $app->request()->post("customer_flat");
		$customer_subarea = $app->request()->post("customer_subarea");
		$customer_area = $app->request()->post("customer_area");
		$customer_city = $app->request()->post("customer_city");
		
		$address_lat = $app->request()->post("address_lat");
		$address_long = $app->request()->post("address_long");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->editUserAddressDetailedSynced($changing_user_code, $user_code, $address_code, $customer_name, $customer_number, $customer_email,
     $customer_flat,$customer_subarea,$customer_area,$customer_city, $address_lat, $address_long);
    echoRespnse(200, $response);
});

$app->post('/deleteUserAddress',function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$user_code=$data['user_code'];
		$address_code=$data['address_code'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$user_code = $app->request()->post("user_code");
		$address_code = $app->request()->post("address_code");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->deleteUserAddress($user_code, $address_code);
    echoRespnse(200, $response);
});

$app->post('/deleteUserAddressSynced',function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$changing_user_code=$data['changing_user_code'];
		$user_code=$data['user_code'];
		$address_code=$data['address_code'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$changing_user_code = $app->request()->post("changing_user_code");
		$user_code = $app->request()->post("user_code");
		$address_code = $app->request()->post("address_code");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
    $response = array();
    $db = new DbHandler();
    $response=$db->deleteUserAddressSynced($changing_user_code, $user_code, $address_code);
    echoRespnse(200, $response);
});

$app->post('/signInServProv', function() use ($app) {

    $emailId=$app->request()->post("servProv_email");
    $password=$app->request()->post("servProv_password");
    $regId=$app->request()->post("servProv_regId");
    $response = array();
    $db = new DbHandler();
    $response=$db->signInServiceProvider($emailId, $password, $regId);
    echoRespnse(200, $response);

});

$app->post('/signInServProvAndGetType', function() use ($app) {

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$emailId = arrayValue($data,'servProv_email');
		$password = arrayValue($data,'servProv_password');
		$regId = arrayValue($data,'servProv_regId');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$emailId = $app->request()->post("servProv_email");
    	$password = $app->request()->post("servProv_password");
    	$regId = $app->request()->post("servProv_regId");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 

    $response = array();
    $db = new DbHandler();
    $response=$db->signInServiceProviderAndGetType($emailId, $password, $regId);
    echoRespnse(200, $response);

});


$app->post('/getUserPreviousOrders', function() use ($app){

    $user_code = $app->request()->post("user_code");

    $response = array();
    $db = new DbHandler();
    $response=$db->getUserPreviousOrders($user_code);
    echoRespnse(200, $response);

});

/*

API Name : /getUserPreviousOrdersGSON/:user_code
Type : GET

Input-Parameters : one integer user_code (user id) in url itself

Response

A) 
Case: When user_code is wrong or when there are no order from this user 
Json - 
{
error: 1
previous_orders_details: [0]
}

B)

Case: Query Succeeds
Json - 
{
error: 1
previous_orders_details: 
	[
		{
			order_code: "5"
			order_date: "Mon Mar 16, 2015 14:56"
			order_amount: 5996
			order_status: "Cancelled"
			sp_name: "Just Flowers"
		},
		{
			order_code: "6"
			order_date: "Mon Mar 16, 2015 15:50"
			order_amount: 123
			order_status: "Delivered"
			sp_name: "Sampurna Super Mart"
		},
		{
			order_code: "7"
			order_date: "Mon Mar 16, 2015 15:55"
			order_amount: 153
			order_status: "Delivered"
			sp_name: "Sampurna Super Mart"
		}
	]
}
*/
$app->get('/getUserPreviousOrdersGSON/:user_code', function($user_code) use ($app){

    //$user_code = $app->request()->post("user_code");

    $response = array();
    $db = new DbHandler();
    $response=$db->getUserPreviousOrders($user_code);
    echoRespnse(200, $response);

});

$app->get('/getUserPreviousOrdersGSONLimited/:user_code', function($user_code) use ($app){

    $previous_order_code = $app->request()->get("previous_order_code");

    $response = array();
    $db = new DbHandler();
    $response=$db->getUserPreviousOrdersLimited($user_code, $previous_order_code);
    echoRespnse(200, $response);

});

$app->post('/getNotAvailableItems', function() use ($app){

    $servProv_id = $app->request()->post("servProv_id");

    $response = array();
    $db = new DbHandler();
    $response=$db->getNonAvaliableItemsSrvProv($servProv_id);
    echoRespnse(200, $response);


});

$app->get('/getNotAvailableItemsGSON/:sp_code/:sc_code', function($sp_code, $sc_code) use ($app){

    $response = array();
    $db = new DbHandler();
    $response=$db->getNonAvaliableItemsSrvProvGSON($sp_code, $sc_code);
    echoRespnse(200, $response);

});

$app->get('/getNotAvailableItemsGSONLimited/:sp_code/:sc_code', function($sp_code, $sc_code) use ($app){

	$previous_item_code = $app->request()->get("previous_item_code");

    $response = array();
    $db = new DbHandler();
    $response=$db->getNonAvaliableItemsSrvProvGSONLimited($sp_code, $sc_code,$previous_item_code);
    echoRespnse(200, $response);

});

$app->post('/getAvailableItems', function() use ($app){

    $servProv_id = $app->request()->post("servProv_id");

    $response = array();
    $db = new DbHandler();
    $response=$db->getAvaliableItemsSrvProv($servProv_id);
    echoRespnse(200, $response);

});

$app->get('/getAvailableItemsGSON/:sp_code/:sc_code', function($sp_code, $sc_code) use ($app){

    $response = array();
    $db = new DbHandler();
    $response=$db->getAvaliableItemsSrvProvGSON($sp_code, $sc_code);
    echoRespnse(200, $response);

});

$app->get('/getOutofStockItemsGSON/:sp_code/:sc_code', function($sp_code, $sc_code) use ($app){

    $response = array();
    $db = new DbHandler();
    $response=$db->getOutofStockItemsSrvProvGSON($sp_code, $sc_code);
    echoRespnse(200, $response);

});

$app->get('/getAvailableItemsGSONLimited/:sp_code/:sc_code', function($sp_code, $sc_code) use ($app){

	$previous_item_code = $app->request()->get("previous_item_code");

    $response = array();
    $db = new DbHandler();
    $response=$db->getAvaliableItemsSrvProvGSONLimited($sp_code, $sc_code,$previous_item_code);
    echoRespnse(200, $response);

});

$app->get('/getOutofStockItemsGSONLimited/:sp_code/:sc_code', function($sp_code, $sc_code) use ($app){

	$previous_item_code = $app->request()->get("previous_item_code");

    $response = array();
    $db = new DbHandler();
    $response=$db->getOutofStockItemsSrvProvGSONLimited($sp_code, $sc_code,$previous_item_code);
    echoRespnse(200, $response);

});

$app->get('/searchItemsSrvProvLimited/:sp_code/:sc_code/:item_status', function($sp_code, $sc_code,$item_status) use ($app){

	$search_string = $app->request()->get("search_string");
	$previous_item_code = $app->request()->get("previous_item_code");

    $response = array();
    $db = new DbHandler();
    $response=$db->searchItemsSrvProvLimited($sp_code, $sc_code,$item_status,$search_string,$previous_item_code);
    echoRespnse(200, $response);

});

$app->post('/confirmItemsServiceProvider', function() use ($app) {
    
    $JSONARRAY = $app->request()->post("JsonDataArray");
   
    $jsonObj=json_decode($JSONARRAY, true);
    $sp_code = $app->request()->post("sp_code");
    $response = array();
    $db = new DbHandler();
    $response=$db->ConfirmItemsServiceProvider($jsonObj, $sp_code);
    echoRespnse(200, $response);

});

$app->post('/confirmItemsServiceProviderGSON', function() use ($app) {

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$jsonObj=$data['JsonDataArray'];		
		$sp_code=$data['sp_code'];
		$sc_code=$data['sc_code'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$jsonObj = $app->request()->post("JsonDataArray");
		$sp_code = $app->request()->post("sp_code");
		$sc_code=$app->request->post("sc_code");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}    
    
    $response = array();
    $db = new DbHandler();
    $response=$db->ConfirmItemsServiceProviderWithCategoryGSON($jsonObj, $sp_code, $sc_code);
    echoRespnse(200, $response);

});

$app->post('/confirmItemsServiceProviderInSearchGSON', function() use ($app) {

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$JSONARRAY=$data['JsonDataArray'];
		$jsonObj=json_decode($JSONARRAY, true);
		
		$sp_code=$data['sp_code'];

	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$JSONARRAY = $app->request()->post("JsonDataArray");
   
		$jsonObj=json_decode($JSONARRAY, true);
		$sp_code = $app->request()->post("sp_code");

	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}    
    
    $response = array();
    $db = new DbHandler();
    $response=$db->ConfirmItemsServiceProviderInSearchGSON($jsonObj, $sp_code);
    echoRespnse(200, $response);

});

/*
API Name : addNewUser
Type : POST
Content-Type : application/json or application/x-www-form-urlencoded
input :: 
{“usercode”:”0”, “reg_id”:”APA91bFfWGXuD3SgGtfcPwl1q0_VB8zfauYEDTQGQDpvRwkOac6tgh_Z8KMaNIzupLYZz4z8ji_tJvmbVaMWcG4oGGVrLaq7ADCtcmHs7tTQ22Q7iQWTetqyKRv3hNuCS1lNJvG15DICz9qGkfuuiGw7HV2TOFHs3JfZs-QGI3UJLgnDIghJV_c”
}

user_code is always “0” to tell its a new user
reg_id 
- for android is GCM key of app on user’s device (to send notifications from server)
- for iPhone we need to figure out unique key to send notification and store that in database through this call

Response
Case : if user_code is not “0”
Json : {[]}

Case : if some error occur on server registering user and generating user_code on server
Json : 
{
“error” : 0
	[
		“Message” : “Error”
	]
}

Case : When call succeeds and use_code generated
Json :
{
“error” : 1
	[
		“user_code” : “89”
	]
}

*/
$app->post('/addNewUser', function() use ($app){

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$userCode = arrayValue($data, 'user_code');
		$regId = arrayValue($data,'reg_id');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$userCode = $app->request()->post("user_code");
		$regId = $app->request()->post("reg_id");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
    $response = array();
    $db = new DbHandler();
    $response=$db->AddNewUser($userCode, $regId);
    echoRespnse(200, $response);


});

$app->post('/addNumberWithUser', function() use ($app){

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$userCode = arrayValue($data, 'user_code');
		$phoneNumber = arrayValue($data, 'phone_number');
		$referralCode = arrayValue($data, 'referral_code');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$userCode = $app->request()->post("user_code");
		$phoneNumber = $app->request()->post('phone_number');
		$referralCode = $app->request()->post('referral_code');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
    $response = array();
    $db = new DbHandler();
    $response=$db->AddNumberWithUser($userCode, $phoneNumber, $referralCode);
    echoRespnse(200, $response);


});


$app->post('/addNumberWithUserv2', function() use ($app){

    $req = $app->request;
    $mtype = $req->getMediaType();
    if($mtype == "application/json"){
        //For application/json
        $data = json_decode($app->request->getBody(),true);
        $userCode = arrayValue($data, 'user_code');
        $phoneNumber = arrayValue($data, 'phone_number');
        $verification_source = arrayValue($data, 'verification_source');
    }
    elseif($mtype == "application/x-www-form-urlencoded"){
        $userCode = $app->request()->post("user_code");
        $phoneNumber = $app->request()->post('phone_number');
        $verification_source = $app->request()->post('verification_source');
    }
    else{
        echoRespnse(400, "Bad Request");
        return;
    }
    
    $response = array();
    $db = new DbHandler();
    $response=$db->AddNumberWithUserv2($userCode, $phoneNumber, $verification_source);
    echoRespnse(200, $response);


});
/*

API Name : /getOrderBillForUser/:order_code

Type : GET

Input-Parameters : one integer order_code in url itself

Response

A) 
Case: When order_code is wrong 

Json - 
{
order_code: null
user_name: null
user_address: null
order_date: null
order_tot_amount: null
order_total_cost: null
order_delivery_cost: null
order_discount_cost: null
sp_code: null
order_item_details: [0]
error: 1
}

B)
Case: Some error on database server
Json - 
{
"error" : 0
}


C) 
Case: Query Succeeds
Json - 
{
	order_code: "10000"
	user_name: "Ankit Kumar"
	user_address: "M9/34,ground floor, dlf phase 2, gurgaon ,Sector 49 , Gurgaon"
	order_date: "Wed Apr 08, 2015 4:46"
	order_tot_amount: "1238"
	order_total_cost: "0"
	order_delivery_cost: "0"
	order_discount_cost: "0"
	sp_code: "2" // seller id
	order_item_details: [
		{
			item_code: "553"
			item_name: "Catch Black Salt 200gm"
			item_short_desc: ""
			item_quantity: "1"
			item_cost: 29
			item_desc: "Ration"
		},
		{
			item_code: "491"
			item_name: "Catch Pav Bhaji Masala 100gms"
			item_short_desc: ""
			item_quantity: "1"
			item_cost: 45
			item_desc: "Ration"
		},
		{
			item_code: "513"
			item_name: "Everest Cumin 100GM"
			item_short_desc: ""
			item_quantity: "1"
			item_cost: 45
			item_desc: "Ration"
		}
	]

}
*/
$app->get('/getOrderBillForUser/:order_code', function($order_code) use ($app){

    //$user_code = $app->request()->post("user_code");
    $response = array();
    $db = new DbHandler();
    $response=$db->getUserOrderBill($order_code);
    echoRespnse(200, $response);


});

$app->post('/pushCustomerNotification', function() use ($app){

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$offer_tag = $data['offer_tag'];
        $password = $data['password'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$offer_tag = $app->request()->post("offer_tag");
        $password = $app->request()->post("password");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}

    if($password == "chicharito") {
        $response = array();
        $db = new DbHandler();
        $response = $db->pushCustomerNotification($offer_tag);
        echoRespnse(200, $response);
    }
    else{
        echoRespnse(400, "Invalid Password");
        return;
    }

});


//*************Seller Registration Process API's*******************

$app->post('/checkAvailabilityServProv', function() use ($app){

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		$sellerPhone = $data['seller_phone'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		$sellerPhone = $app->request()->post("seller_phone");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
    $response = array();
    $db = new DbHandler();
    $response=$db->CheckAvailabilityServProvUniqueID($sellerPhone);
    echoRespnse(200, $response);
    
});


$app->get('/getBasicInventoryforShopType/:shop_type', function($shop_type) use ($app){

    $response = array();
    $db = new DbHandler();
    $response=$db->GetBasicInventoryforShopType($shop_type);
    echoRespnse(200, $response);

});

$app->get('/getServiceTypesforSeller', function() use ($app){

    $response = array();
    $db = new DbHandler();
    $response=$db->GetServiceTypesforSeller();
    echoRespnse(200, $response);

});

$app->get('/getServiceTypesforCityForSeller/:city_code', function($city_code) use ($app){

    $response = array();
    $db = new DbHandler();
    $response=$db->GetServiceTypesforSeller($city_code);
    echoRespnse(200, $response);

});

$app->post('/postCompleteRegistrationDetails', function() use ($app) {

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$JsonServiceAreasArrayString=arrayValue($data,'JsonServiceAreasArray');
		$JsonServiceAreasArray=json_decode($JsonServiceAreasArrayString, true);
		
		$JsonInventoryDetailsArrayString=arrayValue($data,'JsonInventoryDetailsArray');
		$JsonInventoryDetailsArray=json_decode($JsonInventoryDetailsArrayString, true);
		
		$seller_name = arrayValue($data,'seller_name');
		$seller_phone = arrayValue($data,'seller_phone');
		$password = arrayValue($data,'password');
		$shop_name = arrayValue($data,'shop_name');
		$shop_address = arrayValue($data,'shop_address');
		$shop_city = arrayValue($data,'shop_city');
		$shop_type = arrayValue($data,'shop_type');
		$shop_open_time = arrayValue($data,'shop_open_time');
		$shop_close_time = arrayValue($data,'shop_close_time');
		$delivery_time = arrayValue($data,'delivery_time');
		$min_delivery_amt = arrayValue($data,'min_delivery_amt');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$JsonServiceAreasArrayString=$app->request()->post("JsonServiceAreasArray");
		$JsonServiceAreasArray=json_decode($JsonServiceAreasArrayString, true);
		
		$JsonInventoryDetailsArrayString=$app->request()->post("JsonInventoryDetailsArray");
		$JsonInventoryDetailsArray=json_decode($JsonInventoryDetailsArrayString, true);
		
		$seller_name = $app->request()->post("seller_name");
		$seller_phone = $app->request()->post("seller_phone");
		$password = $app->request()->post("password");
		$shop_name = $app->request()->post("shop_name");
		$shop_address = $app->request()->post("shop_address");
		$shop_city = $app->request()->post("shop_city");
		$shop_type = $app->request()->post("shop_type");
		$shop_open_time = $app->request()->post("shop_open_time");
		$shop_close_time = $app->request()->post("shop_close_time");
		$delivery_time = $app->request()->post("delivery_time");
		$min_delivery_amt = $app->request()->post("min_delivery_amt");
		
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}    
    
    $response = array();
    $db = new DbHandler();
    $response=$db->PostCompleteRegistrationDetails($JsonServiceAreasArray, $JsonInventoryDetailsArray,$seller_name, $seller_phone, $password, $shop_name, $shop_address, $shop_city, $shop_type, $shop_open_time, $shop_close_time, $delivery_time, $min_delivery_amt);
    echoRespnse(200, $response);
	//echoDummySuccessResponse();
});


$app->post('/postCompleteRegistrationDetails_v2', function() use ($app) {

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$JsonServiceAreasArrayString=$data['JsonServiceAreasArray'];
		$JsonServiceAreasArray=json_decode($JsonServiceAreasArrayString, true);
		
		$seller_name = $data['seller_name'];
		$seller_phone = $data['seller_phone'];
		$password = $data['password'];
		$shop_name = $data['shop_name'];
		$shop_address = $data['shop_address'];
		$shop_city = $data['shop_city'];
		$shop_area = $data['shop_area'];
		$shop_type = $data['shop_type'];
		$shop_open_time = $data['shop_open_time'];
		$shop_close_time = $data['shop_close_time'];
		$delivery_time = $data['delivery_time'];
		$min_delivery_amt = $data['min_delivery_amt'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$JsonServiceAreasArrayString=$app->request()->post("JsonServiceAreasArray");
		$JsonServiceAreasArray=json_decode($JsonServiceAreasArrayString, true);
		
		$seller_name = $app->request()->post("seller_name");
		$seller_phone = $app->request()->post("seller_phone");
		$password = $app->request()->post("password");
		$shop_name = $app->request()->post("shop_name");
		$shop_address = $app->request()->post("shop_address");
		$shop_city = $app->request()->post("shop_city");
		$shop_area = $app->request()->post("shop_area");
		$shop_type = $app->request()->post("shop_type");
		$shop_open_time = $app->request()->post("shop_open_time");
		$shop_close_time = $app->request()->post("shop_close_time");
		$delivery_time = $app->request()->post("delivery_time");
		$min_delivery_amt = $app->request()->post("min_delivery_amt");
		
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}    
    
    $response = array();
    $db = new DbHandler();
    $response=$db->PostCompleteRegistrationDetails_v2($JsonServiceAreasArray, $seller_name, $seller_phone, $password,
     $shop_name, $shop_address, $shop_city, $shop_area, $shop_type, $shop_open_time, $shop_close_time,
      $delivery_time, $min_delivery_amt);
    echoRespnse(200, $response);

});

$app->post('/postCompleteRegistrationDetails_v3', function() use ($app) {

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$JsonServiceAreasArrayString=arrayValue($data,"JsonServiceAreasArray");
		$JsonServiceAreasArray=json_decode($JsonServiceAreasArrayString, true);
		
		$seller_name = arrayValue($data,"seller_name");
		$seller_phone = arrayValue($data,"seller_phone");
		$seller_phone_validated = arrayValue($data,'seller_phone_validated');
		$password = arrayValue($data,"password");
		$shop_name = arrayValue($data,"shop_name");
		$shop_address = arrayValue($data,"shop_address");
		$shop_city = arrayValue($data,"shop_city");
		$shop_area = arrayValue($data,"shop_area");
		$shop_type = arrayValue($data,"shop_type");
		$shop_open_time = arrayValue($data,"shop_open_time");
		$shop_close_time = arrayValue($data,"shop_close_time");
		$delivery_time = arrayValue($data,"delivery_time");
		$min_delivery_amt = arrayValue($data,"min_delivery_amt");
		$shop_open_bits = arrayValue($data,'shop_open_bits');
		$inventory_by_us = arrayValue($data,'inventory_by_us');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$JsonServiceAreasArrayString=$app->request()->post("JsonServiceAreasArray");
		$JsonServiceAreasArray=json_decode($JsonServiceAreasArrayString, true);
		
		$seller_name = $app->request()->post("seller_name");
		$seller_phone = $app->request()->post("seller_phone");
		$seller_phone_validated = $app->request()->post('seller_phone_validated');
		$password = $app->request()->post("password");
		$shop_name = $app->request()->post("shop_name");
		$shop_address = $app->request()->post("shop_address");
		$shop_city = $app->request()->post("shop_city");
		$shop_area = $app->request()->post("shop_area");
		$shop_type = $app->request()->post("shop_type");
		$shop_open_time = $app->request()->post("shop_open_time");
		$shop_close_time = $app->request()->post("shop_close_time");
		$delivery_time = $app->request()->post("delivery_time");
		$min_delivery_amt = $app->request()->post("min_delivery_amt");
		$shop_open_bits = $app->request()->post('shop_open_bits');
		$inventory_by_us = $app->request()->post('inventory_by_us');
		
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}    
    
    $response = array();
    $db = new DbHandler();
    $response=$db->PostCompleteRegistrationDetails_v3($JsonServiceAreasArray, $seller_name, $seller_phone,
    $seller_phone_validated, $password, $shop_name, $shop_address, $shop_city, $shop_area, $shop_type,
    $shop_open_time, $shop_close_time, $delivery_time, $min_delivery_amt, $shop_open_bits, $inventory_by_us);
    echoRespnse(200, $response);

});

$app->post('/postCompleteRegistrationDetails_v4', function() use ($app) {

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$JsonServiceAreasArrayString=arrayValue($data,"JsonServiceAreasArray");
		$JsonServiceAreasArray=json_decode($JsonServiceAreasArrayString, true);
		
		$seller_name = arrayValue($data,"seller_name");
		$seller_phone = arrayValue($data,"seller_phone");
		$seller_phone_validated = arrayValue($data,'seller_phone_validated');
		$password = arrayValue($data,"password");
		$shop_name = arrayValue($data,"shop_name");
		$shop_address = arrayValue($data,"shop_address");
		$shop_city = arrayValue($data,"shop_city");
		$shop_area = arrayValue($data,"shop_area");
		$shop_type = arrayValue($data,"shop_type");
		$shop_open_time = arrayValue($data,"shop_open_time");
		$shop_close_time = arrayValue($data,"shop_close_time");
		$delivery_time = arrayValue($data,"delivery_time");
		$min_delivery_amt = arrayValue($data,"min_delivery_amt");
		$shop_open_bits = arrayValue($data,'shop_open_bits');
		$inventory_by_us = arrayValue($data,'inventory_by_us');
		$logistics_services = arrayValue($data,'logistics_services');
		$sodexo_coupons = arrayValue($data,'sodexo_coupons');
		$midnight_delivery = arrayValue($data,'midnight_delivery');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$JsonServiceAreasArrayString=$app->request()->post("JsonServiceAreasArray");
		$JsonServiceAreasArray=json_decode($JsonServiceAreasArrayString, true);
		
		$seller_name = $app->request()->post("seller_name");
		$seller_phone = $app->request()->post("seller_phone");
		$seller_phone_validated = $app->request()->post('seller_phone_validated');
		$password = $app->request()->post("password");
		$shop_name = $app->request()->post("shop_name");
		$shop_address = $app->request()->post("shop_address");
		$shop_city = $app->request()->post("shop_city");
		$shop_area = $app->request()->post("shop_area");
		$shop_type = $app->request()->post("shop_type");
		$shop_open_time = $app->request()->post("shop_open_time");
		$shop_close_time = $app->request()->post("shop_close_time");
		$delivery_time = $app->request()->post("delivery_time");
		$min_delivery_amt = $app->request()->post("min_delivery_amt");
		$shop_open_bits = $app->request()->post('shop_open_bits');
		$inventory_by_us = $app->request()->post('inventory_by_us');
		$logistics_services = $app->request()->post('logistics_services');
		$sodexo_coupons = $app->request()->post('sodexo_coupons');
		$midnight_delivery = $app->request()->post('midnight_delivery');
		
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}    
    
    $response = array();
    $db = new DbHandler();
    $response=$db->PostCompleteRegistrationDetails_v4($JsonServiceAreasArray, $seller_name, $seller_phone,
    $seller_phone_validated, $password, $shop_name, $shop_address, $shop_city, $shop_area, $shop_type,
    $shop_open_time, $shop_close_time, $delivery_time, $min_delivery_amt, $shop_open_bits, $inventory_by_us
    , $logistics_services, $sodexo_coupons, $midnight_delivery);
    
    echoRespnse(200, $response);

});

$app->post('/postCompleteRegistrationDetails_v5', function() use ($app) {

	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$JsonServiceAreasArrayString=arrayValue($data,"JsonServiceAreasArray");
		$JsonServiceAreasArray=json_decode($JsonServiceAreasArrayString, true);
		
		$seller_name = arrayValue($data,"seller_name");
		$seller_phone = arrayValue($data,"seller_phone");
		$seller_phone_validated = arrayValue($data,'seller_phone_validated');
		$password = arrayValue($data,"password");
		$shop_name = arrayValue($data,"shop_name");
		$shop_address = arrayValue($data,"shop_address");
		$shop_city = arrayValue($data,"shop_city");
		$shop_area = arrayValue($data,"shop_area");
		$shop_type = arrayValue($data,"shop_type");
		$shop_open_time = arrayValue($data,"shop_open_time");
		$shop_close_time = arrayValue($data,"shop_close_time");
		$delivery_time = arrayValue($data,"delivery_time");
		$min_delivery_amt = arrayValue($data,"min_delivery_amt");
		$shop_open_bits = arrayValue($data,'shop_open_bits');
		$inventory_by_us = arrayValue($data,'inventory_by_us');
		$logistics_services = arrayValue($data,'logistics_services');
		$sodexo_coupons = arrayValue($data,'sodexo_coupons');
		$midnight_delivery = arrayValue($data,'midnight_delivery');
		$referral_code = arrayValue($data,'referral_code');
        $verification_source = arrayValue($data,'verification_source');
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$JsonServiceAreasArrayString=$app->request()->post("JsonServiceAreasArray");
		$JsonServiceAreasArray=json_decode($JsonServiceAreasArrayString, true);
		
		$seller_name = $app->request()->post("seller_name");
		$seller_phone = $app->request()->post("seller_phone");
		$seller_phone_validated = $app->request()->post('seller_phone_validated');
		$password = $app->request()->post("password");
		$shop_name = $app->request()->post("shop_name");
		$shop_address = $app->request()->post("shop_address");
		$shop_city = $app->request()->post("shop_city");
		$shop_area = $app->request()->post("shop_area");
		$shop_type = $app->request()->post("shop_type");
		$shop_open_time = $app->request()->post("shop_open_time");
		$shop_close_time = $app->request()->post("shop_close_time");
		$delivery_time = $app->request()->post("delivery_time");
		$min_delivery_amt = $app->request()->post("min_delivery_amt");
		$shop_open_bits = $app->request()->post('shop_open_bits');
		$inventory_by_us = $app->request()->post('inventory_by_us');
		$logistics_services = $app->request()->post('logistics_services');
		$sodexo_coupons = $app->request()->post('sodexo_coupons');
		$midnight_delivery = $app->request()->post('midnight_delivery');
		$referral_code = $app->request()->post('referral_code');
        $verification_source = $app->request()->post('verification_source');
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}    
    
    $response = array();
    $db = new DbHandler();
    $response=$db->PostCompleteRegistrationDetails_v5($JsonServiceAreasArray, $seller_name, $seller_phone,
    $seller_phone_validated, $password, $shop_name, $shop_address, $shop_city, $shop_area, $shop_type,
    $shop_open_time, $shop_close_time, $delivery_time, $min_delivery_amt, $shop_open_bits, $inventory_by_us
    , $logistics_services, $sodexo_coupons, $midnight_delivery, $referral_code, $verification_source);
    
    echoRespnse(200, $response);

});

$app->post('/commentOnSellerRegister', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$seller_phone = $data['seller_phone'];
		$comment_text = $data['comment_text'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$seller_phone = $app->request()->post("seller_phone");
		$comment_text = $app->request()->post("comment_text");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->commentOnSellerRegister($seller_phone, $comment_text);
	echoRespnse(200, $response); 
});

$app->get('/getServiceTypesforCityForBuyers/:city_code', function($city_code) use ($app){

	$user_code = $app->request()->get("user_code");
	if($user_code == null)
		$user_code = 0;
	
	$response = array();
	$db = new DbHandler();
	//$response["reply"] = "recieved";
	$response = $db->getServiceTypesforCityforBuyers($city_code, $user_code);
	echoRespnse(200, $response);
});

$app->get('/getServiceTypesforCityforBuyers_v2/:city_code', function($city_code) use ($app){

    $user_code = $app->request()->get("user_code");
    if($user_code == null)
        $user_code = 0;
    
    $response = array();
    $db = new DbHandler();
    //$response["reply"] = "recieved";
    $response = $db->getServiceTypesforCityforBuyers_v2($city_code, $user_code);
    echoRespnse(200, $response);
});

$app->post('/createOfferLazylads',function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$offer_tag = $data['offer_tag'];
		$offer_desc = $data['offer_desc'];
		$offer_launch_time = $data['offer_launch_time'];
		$offer_validity = $data['offer_validity'];
		$offer_img_flag = $data['offer_img_flag'];
		$offer_img_add = $data['offer_img_add'];
		$shall_show = $data['shall_show'];
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$offer_tag = $app->request()->post("offer_tag");
		$offer_desc = $app->request()->post("offer_desc");
		$offer_launch_time = $app->request()->post("offer_launch_time");
		$offer_validity = $app->request()->post("offer_validity");
		$offer_img_flag = $app->request()->post("offer_img_flag");
		$offer_img_add = $app->request()->post("offer_img_add");
		$shall_show = $app->request()->post("shall_show");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 
	
	$response = array();
	$db = new DbHandler();
	//$response["reply"] = "recieved";
	$response = $db->createOfferLazylads($offer_tag, $offer_desc, $offer_launch_time, 
	$offer_validity, $offer_img_flag, $offer_img_add, $shall_show);
	echoRespnse(200, $response);   
});

$app->get('/offersListLimited/:offer_code', function($offer_code) use ($app){
	
	$response = array();
	$db = new DbHandler();
	$response = $db->offersListLimited($offer_code);
	echoRespnse(200, $response);
});

$app->get('/activeOffersListLatest', function() use ($app){
	
	$response = array();
	$db = new DbHandler();
	$response = $db->activeOffersListLatestOnly();
	echoRespnse(200, $response);
});

//temporary
$app->get('/pushServiceProviderOrderNotification/:serv_prov_id', function($serv_prov_id) use ($app){
	
	$response = array();
	$db = new DbHandler();
	$response = $db->pushServiceProviderOrderNotification($serv_prov_id);
	echoRespnse(200, $response);
});

$app->post('/configDetails',function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$version_code = arrayValue($data,"lazylad_version_code");
		
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$version_code = $app->request()->post("lazylad_version_code");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 
	
	$response = array();
	$response['error'] = 1;
	$response['number_verification'] = array('shall_number_verify' => 1);
	
	echoRespnse(200, $response);
});

$app->post('/configDetails_v2',function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data,"user_code");
		$lazylad_version_code = arrayValue($data,"lazylad_version_code");
		$user_current_city = arrayValue($data,"city_code_selected");
		$user_current_area = arrayValue($data,"area_code_selected");
		$imei_number = arrayValue($data,"imei_number");
		$mac_address = arrayValue($data,"mac_address");
		$os_type = arrayValue($data,"os_type");
		$model_name = arrayValue($data,"model_name");
		$phone_manufacturer = arrayValue($data,"phone_manufacturer");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
		$lazylad_version_code = $app->request()->post("lazylad_version_code");
		$user_current_city = $app->request()->post("city_code_selected");
		$user_current_area = $app->request()->post("area_code_selected");
		$imei_number = $app->request()->post("imei_number");
		$mac_address = $app->request()->post("mac_address");
		$os_type = $app->request()->post("os_type");
		$model_name = $app->request()->post("model_name");
		$phone_manufacturer = $app->request()->post("phone_manufacturer");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	} 
	
	$response = array();
	$db = new DbHandler();
	$response = $db->configDetails_v2($user_code, $lazylad_version_code, $user_current_city, $user_current_area, $imei_number,
		$mac_address, $os_type, $model_name, $phone_manufacturer);
	echoRespnse(200, $response);
});

$app->post('/userProfileDetails', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data, "user_code");
		$user_email = arrayValue($data, "user_email");
		$user_first_name = arrayValue($data, "user_first_name");
		$user_last_name = arrayValue($data, "user_last_name");
		
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
		$user_email = $app->request()->post("user_email");
		$user_first_name = $app->request()->post("user_first_name");
		$user_last_name = $app->request()->post("user_last_name");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->updateUserProfileDetails($user_code, $user_email, $user_first_name, $user_last_name);
	echoRespnse(200, $response);
});


$app->post('/userProfileDetailsv2', function() use ($app){
    $req = $app->request;
    $mtype = $req->getMediaType();
    if($mtype == "application/json"){
        //For application/json
        $data = json_decode($app->request->getBody(),true);
        
        $user_code = arrayValue($data, "user_code");
        $user_email = arrayValue($data, "user_email");
        $user_first_name = arrayValue($data, "user_first_name");
        $user_last_name = arrayValue($data, "user_last_name");
        $referralCode = arrayValue($data, "referralCode");
        
    }
    elseif($mtype == "application/x-www-form-urlencoded"){
        
        $user_code = $app->request()->post("user_code");
        $user_email = $app->request()->post("user_email");
        $user_first_name = $app->request()->post("user_first_name");
        $user_last_name = $app->request()->post("user_last_name");
        $referralCode = $app->request()->post("referralCode");
    }
    else{
        echoRespnse(400, "Bad Request");
        return;
    }
    
    $response = array();
    $db = new DbHandler();
    $response = $db->updateUserProfileDetailsv2($user_code, $user_email, $user_first_name, $user_last_name, $referralCode);
    echoRespnse(200, $response);
});

$app->post('/getUserProfileDetails', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data, "user_code");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->getUserProfileDetails($user_code);
	echoRespnse(200, $response);
});

$app->post('/getOrderDetailsUser', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);

		$user_code = arrayValue($data, "user_code");
		$order_code = arrayValue($data, "order_code");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){

		$user_code = $app->request()->post("user_code");
		$order_code = $app->request()->post("order_code");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	$response = array();
	$db = new DbHandler();
	$response = $db->getOrderDetailsUser($user_code, $order_code);
	echoRespnse(200, $response);
});

$app->get('/aboutUsCategoriesforSeller', function() use ($app){
	
	$response = array();
	$db = new DbHandler();
	$response = $db->aboutUsCategoriesforSeller();
	echoRespnse(200, $response);
});

$app->post('/aboutUsInfoforId', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$info_id = arrayValue($data, "info_id");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$info_id = $app->request()->post("info_id");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->aboutUsInfoforId($info_id);
	echoRespnse(200, $response);
});

$app->post('/lazyPointsinProfile', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data, "user_code");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->lazyPointsinProfile($user_code);
	echoRespnse(200, $response);
});

$app->post('/getAllCSCoupons', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data, "user_code");
		$city_code = arrayValue($data, "city_code");
		$area_code = arrayValue($data, "area_code");
		$previous_coupon_id = arrayValue($data, "previous_coupon_id");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
		$city_code = $app->request()->post("city_code");
		$area_code = $app->request()->post("area_code");
		$previous_coupon_id = $app->request()->post("previous_coupon_id");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->getAllCSCoupons($user_code, $city_code, $area_code, $previous_coupon_id);
	echoRespnse(200, $response);
});

$app->post('/getBoughtCouponsForCustomer', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data, "user_code");
		$previous_bought_id = arrayValue($data, "previous_bought_id");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
		$previous_bought_id = $app->request()->post("previous_bought_id");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->getBoughtCouponsForCustomer($user_code, $previous_bought_id);
	echoRespnse(200, $response);
});

$app->post('/getDetailsCSCoupon', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data, "user_code");
		$coupon_id = arrayValue($data, "coupon_id");
		$city_code = arrayValue($data, "city_code");
		$area_code = arrayValue($data, "area_code");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
		$coupon_id = $app->request()->post("coupon_id");
		$city_code = $app->request()->post("city_code");
		$area_code = $app->request()->post("area_code");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->getDetailsCSCoupon($user_code, $coupon_id, $city_code, $area_code);
	echoRespnse(200, $response);
});

$app->post('/getDetailsBoughtCoupon', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data, "user_code");
		$bought_id = arrayValue($data, "bought_id");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
		$bought_id = $app->request()->post("bought_id");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->getDetailsBoughtCoupon($user_code, $bought_id);
	echoRespnse(200, $response);
});

$app->post('/buyCSCoupon', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data, "user_code");
		$coupon_id = arrayValue($data, "coupon_id");
		$city_code = arrayValue($data, "city_code");
		$area_code = arrayValue($data, "area_code");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
		$coupon_id = $app->request()->post("coupon_id");
		$city_code = $app->request()->post("city_code");
		$area_code = $app->request()->post("area_code");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->buyCSCoupon($user_code, $coupon_id, $city_code, $area_code);
	echoRespnse(200, $response);
});

$app->post('/redeemBoughtCoupon', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data, "user_code");
		$bought_id = arrayValue($data, "bought_id");
		$city_code = arrayValue($data, "city_code");
		$area_code = arrayValue($data, "area_code");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
		$bought_id = $app->request()->post("bought_id");
		$city_code = $app->request()->post("city_code");
		$area_code = $app->request()->post("area_code");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->redeemBoughtCoupon($user_code, $bought_id, $city_code, $area_code);
	echoRespnse(200, $response);
});

$app->post('/reportProblemBoughtCoupon', function() use ($app){
	$req = $app->request;
	$mtype = $req->getMediaType();
	if($mtype == "application/json"){
		//For application/json
		$data = json_decode($app->request->getBody(),true);
		
		$user_code = arrayValue($data, "user_code");
		$bought_id = arrayValue($data, "bought_id");
		$report_stmt = arrayValue($data, "report_stmt");
	}
	elseif($mtype == "application/x-www-form-urlencoded"){
		
		$user_code = $app->request()->post("user_code");
		$bought_id = $app->request()->post("bought_id");
		$report_stmt = $app->request()->post("report_stmt");
	}
	else{
		echoRespnse(400, "Bad Request");
		return;
	}
	
	$response = array();
	$db = new DbHandler();
	$response = $db->reportProblemBoughtCoupon($user_code, $bought_id, $report_stmt);
	echoRespnse(200, $response);
});

$app->get('/getTime', function(){
	$datetime = date("D M d, Y G:i");
	echoRespnse(200, "$datetime");
	
});

$app->post('/pushNotificationsThroughGCM', function() use ($app) {
    $req = $app->request;
    $mtype = $req->getMediaType();
    if($mtype == "application/json"){
        //For application/json
        $data = json_decode($app->request->getBody(),true);
        $pass = arrayValue($data, "pass");
        $reg_ids_json = arrayValue($data, "reg_ids");
        $reg_ids = json_decode($reg_ids_json, true);
        $message_json = arrayValue($data, "message");
        $message = json_decode($message_json, true);

    }
    elseif($mtype == "application/x-www-form-urlencoded"){
        $pass = $app->request()->post("pass");
        $reg_ids_json = $app->request()->post("reg_ids");
        $reg_ids = json_decode($reg_ids_json, true);
        $message_json = $app->request()->post("message");
        $message = json_decode($message_json, true);
    }
    else{
        echoRespnse(400, "Bad Request");
        return;
    }
    $response = array();
    $db = new DbHandler();
    $response=$db->pushNotificationsThroughGCM($pass, $reg_ids, $message);
    echoRespnse(200, $response);
});


/* coupon system 
lazyPointsinProfile($user_code)
getAllCSCoupons($user_code, $city_code, $area_code, $previous_coupon_id)
getBoughtCouponsForCustomer($user_code, $previous_bought_id)
getDetailsCSCoupon($user_code, $coupon_id, $city_code, $area_code)
getDetailsBoughtCoupon($user_code, $bought_id)
buyCSCoupon($user_code, $coupon_id, $city_code, $area_code)
redeemBoughtCoupon($user_code, $bought_id, $city_code, $area_code)
reportProblemBoughtCoupon($user_code, $bought_id, $report_stmt)*/


$app->run();


?>





