<?php

define('DUMMY_VALIDATION_PIN', '973150');

function verifypinHackcheck($params){
	$result = array();
	$pin  = $params['pin'];
	if(DUMMY_VALIDATION_PIN == $pin){
		$response = array('validated' => true);
		$status = "200";
		
		$result["bypass_hack"] = 1;
		$result["status"] = $status;
		$result["response"] = $response;
	}
	else
		$result["bypass_hack"] = 0;
		
	return $result;
}

?>