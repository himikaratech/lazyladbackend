<?php
require '.././libs/Slim/Slim.php';
require_once './CheckMobiRest.php';

require_once './bypassNumberHack.php';

\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();
$app->checkmobi_key = 'ACFAFEA4-FB40-4528-B309-B79FFB6D6FB9';

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->get('/v1/ping', function() use ($app){

//      $authorization = $app->request->headers("Authorization");
        $authorization = $app->checkmobi_key;

        $api = new \CheckMobiApi\CheckMobiRest($authorization);

        $result = $api->GetCountriesList();
        $status = 200;
        $response = $api;
        echoResponse($status, $response);
});

$app->get('/v1/validation/status/{id}', function($id) use ($app){

// 	$authorization = $app->request->headers("Authorization");
	$authorization = $app->checkmobi_key;
	
	$api = new \CheckMobiApi\CheckMobiRest($authorization);

	$result = $api->ValidationStatus(array("id" => $id));
	$status = $result["status"];
	$response = $result["response"];

	echoResponse($status, $response);
});

 $app->get('/v1/ping', function(){

//      $authorization = $app->request->headers("Authorization");
        //$authorization = $app->checkmobi_key;

        //$api = new \CheckMobiApi\CheckMobiRest($authorization);

        //$result = $api->GetCountriesList();
        $status = 200;
        $response = "pong";
        echoResponse($status, $response);
});


$app->get('/v1/countries', function() use ($app){

// 	$authorization = $app->request->headers("Authorization");
	$authorization = $app->checkmobi_key;
	
	$api = new \CheckMobiApi\CheckMobiRest($authorization);
	
	$result = $api->GetCountriesList();
	$status = $result["status"];
	$response = $result["response"];
	echoResponse($status, $response);
});

$app->post('/v1/checknumber', function() use ($app){

// 	$authorization = $app->request->headers("Authorization");
	$authorization = $app->checkmobi_key;
	
	$params = json_decode($app->request->getBody(),true);
	
	$api = new \CheckMobiApi\CheckMobiRest($authorization);
	
	$result = $api->CheckNumber($params);
	$status = $result["status"];
	$response = $result["response"];
	echoResponse($status, $response);

});

$app->post('/v1/validation/request', function() use ($app){

// 	$authorization = $app->request->headers("Authorization");
	$authorization = $app->checkmobi_key;
	
	$params = json_decode($app->request->getBody(),true);
	
	$api = new \CheckMobiApi\CheckMobiRest($authorization);
	$result = $api->RequestValidation($params);
	$status = $result["status"];
	$response = $result["response"];

	echoResponse($status, $response);
});

$app->post('/v1/validation/verify', function() use ($app){

// 	$authorization = $app->request->headers("Authorization");
	$authorization = $app->checkmobi_key;
	
	$params = json_decode($app->request->getBody(),true);
	
	$hackckeck = verifypinHackcheck($params);
	if(1 == $hackckeck['bypass_hack']){
		$status = $hackckeck["status"];
		$response = $hackckeck["response"];
	}
	else{
		$api = new \CheckMobiApi\CheckMobiRest($authorization);
	
		$result = $api->VerifyPin($params);
		$status = $result["status"];
		$response = $result["response"];
	}
	echoResponse($status, $response);
});

$app->post('/v1/sms/send', function() use ($app){

// 	$authorization = $app->request->headers("Authorization");
	$authorization = $app->checkmobi_key;
	
	$params = json_decode($app->request->getBody(),true);
	
	$api = new \CheckMobiApi\CheckMobiRest($authorization);
	
	$result = $api->SendSMS($params);
	$status = $result["status"];
	$response = $result["response"];
	echoResponse($status, $response);
});

$app->get('/v1/sms/{id}', function($id) use ($app){

// 	$authorization = $app->request->headers("Authorization");
	$authorization = $app->checkmobi_key;

	$api = new \CheckMobiApi\CheckMobiRest($authorization);
	
	$result = $api->GetSMSDetails(array("id" => $id));
	$status = $result["status"];
	$response = $result["response"];
	echoResponse($status, $response);
});

$app->post('/v1/call', function() use ($app){

// 	$authorization = $app->request->headers("Authorization");
	$authorization = $app->checkmobi_key;
	
	$params = json_decode($app->request->getBody(),true);
	
	$api = new \CheckMobiApi\CheckMobiRest($authorization);
	
	$result = $api->SendSMS($params);
	$status = $result["status"];
	$response = $result["response"];
	echoResponse($status, $response);
});

$app->get('/v1/call/{id}', function($id) use ($app){

// 	$authorization = $app->request->headers("Authorization");
	$authorization = $app->checkmobi_key;

	$api = new \CheckMobiApi\CheckMobiRest($authorization);
	
	$result = $api->GetCallDetails(array("id" => $id));
	$status = $result["status"];
	$response = $result["response"];
	echoResponse($status, $response);
});

$app->run();

?>
