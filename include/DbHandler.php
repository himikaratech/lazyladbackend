                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               <?php
/**
 * Created by PhpStorm.
 * User: Saurabh
 * Date: 01/02/15
 * Time: 10:43 PM
 */

require_once '../LazyladRest/lazyladRest.php';
define("Lazylad_Auth", "1234567890");

require_once '../newserver_import/acc_management_api.php';
require_once '../utilities/helper_functions.php';

class DbHandler {
    private $conn;

    function __construct() {
    	$conn = null;
    	
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }
    
    function __destruct() {
       if($this->conn)
       		$this->conn->close();
       	$this->conn = null;
   	}
   	
   	private function getNetworkApi($url = "http://localhost"){
   		$authorization = Lazylad_Auth;
   		if($url)
   			$api = new \LazyladRest\LazyladRest($authorization, $url);
   		else
   			$api = new \LazyladRest\LazyladRest($authorization);
   		return $api;
   	}

    public function createServiceProvider($name, $email, $password, $latitude, $longitude, $area, $city, $number, $serviceType){
        require_once 'PassHash.php';
        $response = array();

        $sp_code="SP1";

        $stmt_city_name=$this->conn->prepare("select city_code from city_details where city_name=? and country_code=? ");
        $stmt_city_name->bind_param("ss", $city, 1);
        if($stmt_city_name->execute()){
            $city_code=$stmt_city_name->get_result()->fetch_assoc();
            $stmt_city_name->close();
        }
        else{
            $city_code=0;
        }

        $stmt_area_name=$this->conn->prepare("select area_code from area_details where area_name=? and city_code=? ");
        $stmt_area_name->bind_param("ss", $area, $city_code);
        if($stmt_area_name->execute()){
            $area_code=$stmt_area_name->get_result()->fetch_assoc();
            $stmt_area_name->close();
        }
        else{
            $area_code=0;
        }


        if (!$this->isServiceProviderExists($email)){
            $password_hash = PassHash::hash($password);
            $api_key = $this->generateApiKey();
            $stmt = $this->conn->prepare("INSERT INTO service_providers(sp_code, sp_name,
                        sp_city_code, 	sp_area_code, sp_latitute, sp_longitude, sp_email, sp_password, sp_number, sp_service_type_code, sp_api_key) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            $stmt->bind_param("ssssddsssss", $sp_code, $name, $city_code, $area_code, $latitude, $longitude, $email, $password_hash, $number, $serviceType, $api_key);

            $result = $stmt->execute();

            $stmt->close();
            if ($result) {
                // User successfully inserted
                return USER_CREATED_SUCCESSFULLY;
            } else {
                // Failed to create user
                return USER_CREATE_FAILED;
            }
        }
        else {
            // User with same email already existed in the db
            return USER_ALREADY_EXISTED;
        }
        return $response;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isServiceProviderExists($email) {
        $stmt = $this->conn->prepare("SELECT id from service_providers WHERE sp_email = ?");
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKeyServiceProvider($api_key) {
        $stmt = $this->conn->prepare("SELECT id from service_providers WHERE sp_api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyByIdServiceProvider($sp_code) {
        $stmt = $this->conn->prepare("SELECT sp_api_key FROM service_providers WHERE sp_code = ?");
        $stmt->bind_param("i", $sp_code);
        if ($stmt->execute()) {
            $api_key = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getServiceProviderId($api_key) {
        $stmt = $this->conn->prepare("SELECT sp_code FROM service_providers WHERE sp_api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }
    }

    private function isServiceProviderValid($servProvCode) {
        $stmt = $this->conn->prepare("SELECT id from service_providers WHERE sp_code = ?");
        $stmt->bind_param("s", $servProvCode);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }
    
    private function getCityDetails($city_name){
    	$query = " select id, city_code, city_name, country_code, country_name from city_details where city_name = ? ";
    	$stmt = $this->conn->prepare($query);
    	$stmt->bind_param('s',$city_name);
    	$result = $stmt->execute();
    	if($result){
        	$stmt->bind_result($id, $citycode, $cityname, $countrycode, $countryname);
        	if($stmt->fetch()){
        		$tmp = array();
            	$tmp["id"] = $id;
            	$tmp["city_code"] = $citycode;
            	$tmp["city_name"] = $cityname;
            	$tmp["country_code"] = $countrycode;
            	$tmp["country_name"] = $countryname;
            	
            	return $tmp;
        	}
        }
        return null;
    }

    public function getCities(){
        $stmt = $this->conn->prepare("SELECT id, city_code, city_name, country_code, country_name, city_alias_name  FROM city_details");;
        $stmt->execute();
        $stmt->bind_result($id, $citycode, $cityname, $countrycode, $countryname, $cityaliasname);

        $response = array();
        $response["error"] = 1;
        $response["cities"] = array();

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["city_code"] = $citycode;
            $tmp["city_name"] = $cityname;
            $tmp["country_code"] = $countrycode;
            $tmp["country_name"] = $countryname;
            $tmp["city_alias_name"] = $cityaliasname;
            array_push($response["cities"], $tmp);
        }
        return $response;
    }
    
    public function getCitiesFinal(){
        $response=array();
        $stmt="SELECT cd.id, cd.city_code, cd.city_name, cd.city_image_flag, cd.city_image_add,
        cd.city_alias_name, cd.show_city_flag, (
            (
                SELECT COUNT( * ) 
                FROM cs_coupon_city
                WHERE city_code = cd.id
            ) > 0
        ) AS coupon_exist
        FROM city_details cd
        WHERE launched_for_buyer = TRUE  ";
        $result=mysqli_query($this->conn, $stmt);
        $response["error"] = 1;
        if($result){
            $response["error"] = 1;
            $response["cities"] = array();
            while ($row = mysqli_fetch_array($result)){
                $product = array();
                $product["id"] = $row["id"];
                $product["city_code"] = $row["city_code"];
                $product["city_name"] = $row["city_name"];
                $product["city_image_flag"] = $row["city_image_flag"];
                $product["city_image_add"] = $row["city_image_add"];
                $product["city_alias_name"] = $row["city_alias_name"];
                $product["show_city_flag"] = $row["show_city_flag"];
                $product["coupon_exists_in_city"] = $row["coupon_exist"];
                array_push($response["cities"], $product);
            }
        }else{
            $response["error"] = 0;
        }
        return $response;
    }
    
    public function getCitiesforSeller(){
        $response=array();
        $stmt="select id, city_code, city_name from city_details where launched_for_seller = TRUE";
        $result=mysqli_query($this->conn, $stmt);
        $response["error"] = 1;
        if($result){
            $response["error"] = 1;
            $response["cities"] = array();
            while ($row = mysqli_fetch_array($result)){
                $product = array();
                $product["id"] = $row["id"];
                $product["city_code"] = $row["city_code"];
                $product["city_name"] = $row["city_name"];
                array_push($response["cities"], $product);
            }
        }else{
            $response["error"] = 0;
        }
        return $response;
    }

    public function getAreas($cityId){

        $stmt = $this->conn->prepare("SELECT id, area_code, area_name, city_code FROM area_details where city_code=? and area_launched_for_buyer = TRUE");
        $stmt->bind_param("s", $cityId);
        $stmt->execute();
        $stmt->bind_result($id, $areacode, $areaname, $citycode);

        $response = array();
        $response["error"] = 1;
        $response["areas"] = array();

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["area_code"] = $areacode;
            $tmp["area_name"] = $areaname;
            $tmp["city_code"] = $citycode;
            array_push($response["areas"], $tmp);
        }
        return $response;
        //$areas = $stmt->get_result();
        //$stmt->close();
        //return $areas;

    }
    
    public function getAreasInCity($cityId){

        $stmt = $this->conn->prepare("SELECT id, area_code, area_name, city_code FROM area_details 
        	where city_code=? and area_launched_for_buyer = TRUE");
        $stmt->bind_param("s", $cityId);
        $stmt->execute();
        $stmt->bind_result($id, $areacode, $areaname, $citycode);

        $response = array();
        $response["error"] = 1;
        $response["areas"] = array();

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["area_code"] = $areacode;
            $tmp["area_name"] = $areaname;
            $tmp["city_code"] = $citycode;
            array_push($response["areas"], $tmp);
        }
        return $response;

    }
    
    public function getAreasInCitySeller($cityId){

        $stmt = $this->conn->prepare("SELECT id, area_code, area_name, city_code FROM area_details 
        	where city_code=? and area_launched_for_seller = TRUE");
        $stmt->bind_param("s", $cityId);
        $stmt->execute();
        $stmt->bind_result($id, $areacode, $areaname, $citycode);

        $response = array();
        $response["error"] = 1;
        $response["areas"] = array();

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["area_code"] = $areacode;
            $tmp["area_name"] = $areaname;
            $tmp["city_code"] = $citycode;
            array_push($response["areas"], $tmp);
        }
        return $response;

    }
    
    public function getAreasInCityforSeller($city_code,$seller_area_code){

		//seller_area_code unused for now
		
        $response = array();
        $response["error"] = 1;

		$response["city_image_add"] = "";
		$stmt = $this->conn->prepare("select city_name, city_image_add from city_details where city_code = ?");
		$stmt->bind_param("s", $city_code);
        $stmt->execute();
        $stmt->bind_result($city_name, $city_image_add);
		if($stmt->fetch()){
			$response["city_name"] = $city_name;
			$response["city_image_add"] = $city_image_add;
		}
		$stmt->close();

        $stmt = $this->conn->prepare("SELECT id, area_code, area_name, city_code FROM area_details where city_code=?");
        $stmt->bind_param("s", $city_code);
        $stmt->execute();
        $stmt->bind_result($id, $areacode, $areaname, $citycode);

        $response["areas"] = array();

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["area_code"] = $areacode;
            $tmp["area_name"] = $areaname;
            $tmp["city_code"] = $citycode;
            array_push($response["areas"], $tmp);
        }
        return $response;

    }
    
    public function getServiceCategory($st_code){

        $stmt = $this->conn->prepare("SELECT id, st_code, sc_code, sc_name FROM service_type_categories where st_code=? order by score DESC");
        $stmt->bind_param("s", $st_code);
        $stmt->execute();
        $stmt->bind_result($id, $stcode, $sccode, $scname);

        $response = array();
        $response["error"] = 1;
        $response["service_categories"] = array();

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["st_code"] = $stcode;
            $tmp["sc_code"] = $sccode;
            $tmp["sc_name"] = $scname;
            array_push($response["service_categories"], $tmp);
        }
        return $response;

    }
    
    // works only for coupon type 1 or 2
    public function checkCouponCodeValidity($coupon_name){
        $stmt = "select * from coupons_by_lazylad where coupon_name='".$coupon_name."' and coupon_active = 1";
            $result=mysqli_query($this->conn, $stmt);
            if(mysqli_num_rows($result)==1){
                $row = mysqli_fetch_array($result);
                if(
                	($row["coupon_type_id"] == 1 
                	|| $row["coupon_type_id"] == 2)
                 && $row["coupons_applied"] < $row["number_of_coupons"] )
                {
					$discount=$row["discount_amount"];
					$response["error"] = 1;
					$response["coupon_amount"] = $discount;
                }
                else
                	$response["error"] = 0;

            }else{
                $response["error"] = 0;
                $response["message"]="Coupon can't be applied";
            }
        return $response;
    }
    
    // works only for coupon type 1 or 2
    public function checkCouponCodeValidityFinal($coupon_name){
    	$response = array();
    	$response["error"] = 1;
        $stmt = "select * from coupons_by_lazylad where coupon_name='".$coupon_name."' and coupon_active = 1";
            $result=mysqli_query($this->conn, $stmt);
            if(mysqli_num_rows($result)==1){
                $row = mysqli_fetch_array($result);
                if(
                	($row["coupon_type_id"] == 1 
                	|| $row["coupon_type_id"] == 2)
                 && $row["coupons_applied"] < $row["number_of_coupons"] )
                {
					$discount=$row["discount_amount"];
					$response["error"] = 1;
					$response["coupon_type_id"] = $row["coupon_type_id"];
					$response["coupon_amount"] = $discount;
					$response["minimum_order_amount"] = $row["minimum_order_amount"];
                }
                else
                	$response["error"] = 0;

            }else{
                $response["error"] = 0;
                $response["message"]="Coupon can't be applied";
            }
        return $response;
    }
    
    public function checkCouponCodeValidityFinal_v2($coupon_name, $user_code, $total_amount){
    	$response = array();
    	$response["error"] = 1;
        $stmt = "select * from coupons_by_lazylad where coupon_name='".$coupon_name."' and coupon_active = 1";
            $result=mysqli_query($this->conn, $stmt);
            if(mysqli_num_rows($result)==1){
                $coupon_details = mysqli_fetch_array($result);
                if($coupon_details["coupons_applied"] < $coupon_details["number_of_coupons"]){
					$discount=$coupon_details["discount_amount"];
					$response["error"] = 1;
					$response["coupon_type_id"] = $coupon_details["coupon_type_id"];
					
					if($coupon_details["coupon_type_id"] == 1 
					|| $coupon_details["coupon_type_id"] == 2){
						$response["coupon_amount"] = $discount;
						$response["minimum_order_amount"] = $coupon_details["minimum_order_amount"];
					}
					else if($coupon_details["coupon_type_id"] == 3){
						if(0 != $coupon_details["minimum_order_amount"] 
						&& $total_amount < $coupon_details["minimum_order_amount"]){
							$response["error"] = 0;
						}
						else{
							// $stmt = "select exists( select 1 from order_details where user_code = $user_code 
// 							and coupon_code = lower('".$coupon_name."') ) as coupon_used ";
							$stmt = " select count(*)  as num_coupon_used from order_details where user_code = $user_code 
							and coupon_code = lower('".$coupon_name."')  ";
							$result = mysqli_query($this->conn, $stmt);
							if( $result){
								$row = mysqli_fetch_array($result);
						
								if(0 != $coupon_details["num_coupons_per_user"] && 
									$coupon_details["num_coupons_per_user"] <= $row['num_coupon_used'])
								{
									$response["error"] = 0;
								}
								else
								{
									$max_discount_amount = $coupon_details['max_discount_amount'];
									if($coupon_details['apply_type'] == 'FLAT'){
										if(0 != $discount){
											if($total_amount < $discount)
												$final_discount = $total_amount;
											else
												$final_discount = $discount;
										}
										else{ 
											if(0 == $max_discount_amount || $total_amount < $max_discount_amount)
												$final_discount = $total_amount;
											else
												$final_discount = $max_discount_amount;
										}
										$response["coupon_amount"] = $final_discount;
									}
									else if($coupon_details['apply_type'] == 'Percentage'){
										if(0 == $discount )
											$percentage_amount = $total_amount;
										else
											$percentage_amount = (($discount * $total_amount)/100);
											
										if(0 == $max_discount_amount || $percentage_amount < $max_discount_amount)
											$final_discount = $percentage_amount;
										else
											$final_discount = $max_discount_amount;
										$response["coupon_amount"] = $final_discount;
									}
									else{
										$response["error"] = 0;
									}
								}
							}
							else {
								$response["error"] = 0;
							}
						}
					}
					else{
						// haven't yet implemented for 
						//this coupon type so won't work for now
						$response["error"] = 0;
					}
                }
                else
                	$response["error"] = 0;

            }else{
                $response["error"] = 0;
                $response["message"]="Coupon can't be applied";
            }
        return $response;
    }
    
    private function getOrderStringFromOrderCode($order_code){
    	
    	$order_string = null;
    	$stmt = "select os.order_string as order_string from order_strings os, order_basic_details obd 
    			where os.o_id = od.id and obd.order_code = $order_code";
    	$result = mysqli_query($this->conn, $stmt);
    	if($result){
    		$row = mysqli_fetch_array($result);
    		$order_string = $row['order_string'];
    	}
    	mysqli_free_result($result);
    	
    	return $order_string;
    }
    
    private function getOrderCodeFromOrderString($order_string){
    	
    	$order_code = null;
    	$stmt = "select obd.order_code as order_code from order_strings os, order_basic_details obd 
    			where os.o_id = obd.id and os.order_string = '$order_string'";
    	$result = mysqli_query($this->conn, $stmt);
    	if($result){
    		$row = mysqli_fetch_array($result);
    		$order_code = $row['order_code'];
    	}
    	mysqli_free_result($result);
    	
    	return $order_code;
    }

    private function updateItemConversion($order_code){

        $stmt = "select item_code, sp_code from order_item_served_by_sp where order_code = '$order_code'";
        $result = mysqli_query($this->conn, $stmt);

        if($result){
            while($row = $result->fetch_assoc()) {

                $item_code = $row["item_code"];
                $sp_code = $row["sp_code"];

                $stmt = "select * from ims_item_analytics where item_code = '$item_code'";
                $result_ia = mysqli_query($this->conn, $stmt);

                if(count($result_ia->fetch_assoc())>0){
                    $stmt = "update ims_item_analytics set conversions = conversions + 1 where item_code = '$item_code'";
                    mysqli_query($this->conn, $stmt);
                }
                else{
                    $stmt = "insert into ims_item_analytics (item_code, conversions) values('$item_code', 1)";
                    mysqli_query($this->conn, $stmt);
                }

                $stmt = "select cs_coupon_id from cs_item_coupons_mapping where item_code = '$item_code'";
                $result_icm = mysqli_query($this->conn, $stmt);
                $res_array = mysqli_fetch_assoc($result_icm);

                if(count($res_array)>0){
                    $stmt = "update cs_item_coupons_mapping set conversions = conversions + 1 where item_code = '$item_code'";
                    mysqli_query($this->conn, $stmt);
                }

                $stmt = "update ims_sp_item_analytics set conversions = conversions + 1 where item_code = '$item_code' and sp_code = '$sp_code'";
                mysqli_query($this->conn, $stmt);

                if(mysqli_affected_rows($this->conn)==0){
                    $stmt = "insert into ims_sp_item_analytics (item_code, sp_code, conversions) values('$item_code','$sp_code',1)";
                    mysqli_query($this->conn, $stmt);
                }
            }
        }
    }
    
    public function handleOrderCancelled($order_code){
    	$query = "select user_code from order_basic_details where order_code = ?";
    	$stmt = $this->conn->prepare($query);
    	$stmt->bind_param("s",$order_code);
    	$result = $stmt->execute();
    	if($result){
    		$stmt->bind_result($user_code);
    		if($stmt->fetch()){
    			$transaction_params = array(
					"user_code" => $user_code,
					"owner_type" => "0",
					"urid" => $order_code,
					"urid_type" => "ORDER",
					"t_type" => ORDER_CANCEL_CUSTOMER
					);
				$net_api = $this->getNetworkApi();
    	
				$response = $net_api->makeTransaction($transaction_params);
				if(null != $response && true == $response['success'])
					return true;
			}
    	}
    	return false;
    }
    
    public function cancelUserCurrentOrder($order_string){
        $order_code = $this->getOrderCodeFromOrderString($order_string);
        
        require_once dirname(__FILE__) . '/GCMCUSTOMER.php';
        $response = array();
        $status="Cancelled";
        $stmt_update="update order_in_progress set status='".$status."' where order_code='".$order_code."'";
        $result_update=mysqli_query($this->conn, $stmt_update);
        if($result_update){
            $stmt_user="select * from user_details where user_code = (select user_code from order_basic_details where order_code='".$order_code."')";
            $result=mysqli_query($this->conn, $stmt_user);
            $row_reg_id = mysqli_fetch_array($result);
            $reg_id=array($row_reg_id["reg_id"]);

            $gcm=new GCMCUSTOMER();
            $gcm->send_notification($reg_id, array("price"=>"Order Cancelled"));

            $response["error"]=1;
        }else{
            $response["error"]=0;
            //TODO if update not done than error handling
        }
        
        $this->handleOrderCancelled($order_code);

        return $response;
    }
    
    public function cancelUsersPreviousOrderForLazyLads($order_string){
    	$order_code = $this->getOrderCodeFromOrderString($order_string);
    
        require_once dirname(__FILE__) . '/GCMCUSTOMER.php';
        $response = array();
        $status="Cancelled";
        $stmt_update="update order_in_progress set status='".$status."' where order_code='".$order_code."'";
        $result_update=mysqli_query($this->conn, $stmt_update);
        if($result_update){
            $stmt_user="select * from user_details where user_code = (select user_code from order_basic_details where order_code='".$order_code."')";
            $result=mysqli_query($this->conn, $stmt_user);
            $row_reg_id = mysqli_fetch_array($result);
            $reg_id=array($row_reg_id["reg_id"]);

            $gcm=new GCMCUSTOMER();
            $gcm->send_notification($reg_id, array("price"=>"Order Cancelled"));

            $response["error"]=1;
        }else{
            $response["error"]=0;
            //TODO if update not done than error handling
        }

        return $response;
    }
    
    public function getServiceCategoriesOfferedBySP($sp_code){
        $response = array();
        $response["error"] = 1;
        if($this->isServiceProviderValid($sp_code)){    
            $stmt = $this->conn->prepare("SELECT stc.id, stc.st_code, stc.sc_code, stc.sc_name FROM service_type_categories stc, service_providers sp
             where stc.st_code=sp.sp_service_type_code and sp.sp_code=?
              and stc.sc_code in (SELECT sc_code from items_offered_by_service_provider where
               sp_code=sp.sp_code group by sc_code) order by stc.score DESC");
            $stmt->bind_param("s", $sp_code);
            $stmt->execute();
            $stmt->bind_result($id, $stcode, $sccode, $scname);
            
            $response["service_categories"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["st_code"] = $stcode;
                $tmp["sc_code"] = $sccode;
                $tmp["sc_name"] = $scname;
                array_push($response["service_categories"], $tmp);                
            }
        }
        
        return $response;
    }

    public function getItemsManagSystem($serv_prov_id){

        if($this->isServiceProviderValid($serv_prov_id)){
            $stmt = $this->conn->prepare("SELECT * FROM items_offered_by_service_provider where sp_code=?");
            $stmt->bind_param("s", $serv_prov_id);
            $stmt->execute();
            $stmt->bind_result($id, $itemcode, $spcode, $itemtypecode, $itemname, $itemunit, $itemquant, $itemcost, $itemimgflag, $itemimgadd
                , $itemshortdesc, $itemdesc, $itemstatus);

            $response = array();
            $response["error"] = false;
            $response["items"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["item_code"] = $itemcode;
                $tmp["sp_code"] = $spcode;
                $tmp["item_type_code"] = $itemtypecode;
                $tmp["item_name"] = $itemname;
                $tmp["item_unit"] = $itemunit;
                $tmp["item_quantity"] = $itemquant;
                $tmp["item_cost"] = $itemcost;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_desc"] = $itemdesc;
                $tmp["item_status"] = $itemstatus;
                array_push($response["items"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }

    public function getCurrentOrders($serv_prov_id, $reg_id){
        if($this->isServiceProviderValid($serv_prov_id)){
            $response = array();
            $response["error"] = 1;
            $response["current_orders_service_providers"] = array();
            $stmt = $this->conn->prepare("SELECT os.order_string, obd.expected_del_time,
             uad.user_address, ud.user_code, obd.order_time, cod.post_total_amount, uad.user_name, 
             uad.user_phone_number FROM order_in_progress oin, order_basic_details obd, 
             user_address_details uad, user_details ud, order_strings os, confirmed_order_details cod 
             where os.o_id = obd.id and oin.sp_code=? and oin.status=? and obd.order_code=oin.order_code 
             and uad.user_code=obd.del_add_user_code and uad.user_address_code=obd.del_add_code and 
             obd.user_code = ud.user_code and cod.order_code=oin.order_code");
            $status="Pending";
            $stmt->bind_param("ss", $serv_prov_id, $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber);



            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["current_order_code"] = $ordercode;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["user_code"] = $usercode;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["expected_del_time"] = $expecteddeltime;
                $tmp["sp_code"]=$serv_prov_id;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                array_push($response["current_orders_service_providers"], $tmp);
            }
            //$this->pushServiceProviderOrderNotification($serv_prov_id, $reg_id);
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
    public function getCurrentOrdersForLazyLads(){
            $response = array();
            $response["error"] = 1;
            $response["current_orders_service_providers"] = array();
            $stmt = $this->conn->prepare("SELECT os.order_string, obd.expected_del_time,
             uad.user_address, ud.user_code, obd.order_time, obd.total_amount, uad.user_name, 
             uad.user_phone_number, sps.sp_name FROM order_in_progress oin, order_basic_details obd,
             user_address_details uad, service_providers sps, user_details ud, order_strings os 
             where os.o_id = obd.id and oin.status=? and obd.order_code=oin.order_code and 
             uad.user_code=obd.del_add_user_code and uad.user_address_code=obd.del_add_code 
             and obd.user_code = ud.user_code and obd.order_code>=2960 and sps.sp_code=oin.sp_code");
            $status="Pending";
            $stmt->bind_param("s", $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber, $serv_prov_name);



            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["current_order_code"] = $ordercode;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["user_code"] = $usercode;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["expected_del_time"] = $expecteddeltime;
                $tmp["sp_name"]=$serv_prov_name;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                array_push($response["current_orders_service_providers"], $tmp);
            }
            //$this->pushServiceProviderOrderNotification($serv_prov_id, $reg_id);
            return $response;
    }
    
    const ORDER_FILTER_CITIES = 'cities';
    public function getCurrentOrdersForLazyLadsLimited($before_order_string, $order_filter){
    		$before_order_code = $this->getOrderCodeFromOrderString($before_order_string);
    		
    		$order_count_to_send = 25;    


            $response = array();
            $response["error"] = 1;
            $response["current_orders_service_providers"] = array();
            
            $query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , uad.user_address,
                ud.user_code, ud.phone_number, obd.order_time, cod.post_total_amount, oin.status, uad.user_name, 
                uad.user_phone_number, sps.sp_name, obd.coupon_code, lod.logistics_services_used, 
                lod.rider_assigned, lod.rider_accepted FROM order_in_progress oin, order_basic_details 
                obd, user_address_details uad, service_providers sps, user_details ud, order_strings os, 
                confirmed_order_details cod, logistics_order_details lod
                WHERE os.o_id = obd.id
                AND oin.status =  ?
                AND obd.order_code = oin.order_code
                AND uad.user_code = obd.del_add_user_code
                AND uad.user_address_code = obd.del_add_code
                AND obd.user_code = ud.user_code
                AND sps.sp_code = oin.sp_code
                AND lod.order_code = oin.order_code
                AND cod.order_code = oin.order_code";
            
            if($order_filter && isset($order_filter[self::ORDER_FILTER_CITIES]) &&
            	count($order_filter[self::ORDER_FILTER_CITIES]) ){
            	$order_city_condition = " and uad.user_city in (select city_name from city_details where city_code 
            	in (".implode(',',$order_filter[self::ORDER_FILTER_CITIES]).")) ";
            	/*$order_city_condition = "";	*/
            }
            else
            	$order_city_condition = "";

            $whether_now_or_not = " and UNIX_TIMESTAMP(NOW()) > (UNIX_TIMESTAMP(obd.expected_del_time) - 5400
			 - REPLACE(  sps.sp_delivery_time ,  ' mins',  '' )*60) ";
			
			if(intval($before_order_code) == 0)
				$order_condition = "";
			else 
				$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
			$sort_condition = "order by CAST(obd.order_code as UNSIGNED) desc ";
			$limit_condition = " limit $order_count_to_send ";
			
			$final_query = $query.$order_city_condition.$whether_now_or_not.$order_condition.$sort_condition.$limit_condition;

            $stmt = $this->conn->prepare($final_query);
            $status="Pending";
            $stmt->bind_param("s", $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $phonenumber, $orderdate, $ordertotamount,
             $order_status, $username, $userphonenumber, $serv_prov_name, $coupon_code, $rider_requested, $rider_assigned, $rider_accepted);



            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["current_order_code"] = $ordercode;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["user_code"] = $usercode;
                $tmp["phone_number"] = $phonenumber;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["expected_del_time"] = $expecteddeltime;
                $tmp["sp_name"]=$serv_prov_name;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                $tmp["coupon_code"] = $coupon_code;
                $tmp["rider_requested"] = $rider_requested;
                $tmp["rider_assigned"] = $rider_assigned;
                $tmp["rider_accepted"] = $rider_accepted;
                
                array_push($response["current_orders_service_providers"], $tmp);
            }
            //$this->pushServiceProviderOrderNotification($serv_prov_id, $reg_id);
            return $response;
    }

    public function getPendingOrders($serv_prov_id){
        if($this->isServiceProviderValid($serv_prov_id)){
            $response = array();
            $response["error"] = 1;
            $response["pending_orders_service_providers"] = array();
            $stmt = $this->conn->prepare("SELECT os.order_string, obd.expected_del_time, uad.user_address, 
            ud.user_code, obd.order_time, cod.post_total_amount, uad.user_name, uad.user_phone_number
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, 
            user_details ud, order_strings os, confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.sp_code =12
            AND oin.status =  'Pending'
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND obd.user_code = ud.user_code
            AND cod.order_code = oin.order_code");
            $status="Confirmed";
            $stmt->bind_param("ss", $serv_prov_id, $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber);



            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["pending_order_code"] = $ordercode;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["user_code"] = $usercode;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["expected_del_time"] = $expecteddeltime;
                $tmp["sp_code"]=$serv_prov_id;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                array_push($response["pending_orders_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
    public function getPendingOrdersForLazyLads(){
            $response = array();
            $response["error"] = 1;
            $response["pending_orders_service_providers"] = array();
            $stmt = $this->conn->prepare("SELECT os.order_string, obd.expected_del_time, uad.user_address,
             ud.user_code, obd.order_time, cod.post_total_amount, uad.user_name, uad.user_phone_number, sps.sp_name
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, service_providers sps, 
            user_details ud, order_strings os, confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.status =  ?
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND obd.user_code = ud.user_code
            AND sps.sp_code = oin.sp_code
            AND cod.order_code = obd.order_code");
            $status="Confirmed";
            $stmt->bind_param("s", $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber, $serv_prov_name);



            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["pending_order_code"] = $ordercode;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["user_code"] = $usercode;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["expected_del_time"] = $expecteddeltime;
                $tmp["sp_name"]=$serv_prov_name;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                array_push($response["pending_orders_service_providers"], $tmp);
            }
            return $response;
    }
    
    public function getPendingOrdersForLazyLadsLimited($before_order_string, $order_filter){
            $before_order_code = $this->getOrderCodeFromOrderString($before_order_string);
            
            $order_count_to_send = 25;
            
            $response = array();
            $response["error"] = 1;
            $response["pending_orders_service_providers"] = array();
            
            $query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , uad.user_address, 
            ud.user_code, ud.phone_number, obd.order_time, cod.post_total_amount, oin.status, uad.user_name, 
            uad.user_phone_number, sps.sp_name, obd.coupon_code, lod.logistics_services_used, 
            lod.rider_assigned, lod.rider_accepted
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, 
            user_details ud, service_providers sps, order_strings os, logistics_order_details lod,
             confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.status = ?
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND obd.user_code = ud.user_code
            AND sps.sp_code = oin.sp_code
            AND lod.order_code = oin.order_code
            AND cod.order_code = oin.order_code";
           
           	if($order_filter && isset($order_filter[self::ORDER_FILTER_CITIES]) &&
            	count($order_filter[self::ORDER_FILTER_CITIES]) ){
            	/*$order_city_condition = " and uad.user_city in (select city_name from city_details where city_code 
            	in (".implode(',',$order_filter[self::ORDER_FILTER_CITIES]).")) ";	*/
            	$order_city_condition = "";
            }
            else
            	$order_city_condition = "";
            
            $whether_now_or_not = " and UNIX_TIMESTAMP(NOW()) > (UNIX_TIMESTAMP(obd.expected_del_time) -  5400
			 - REPLACE(  sps.sp_delivery_time ,  ' mins',  '' )*60) ";
			
			if(intval($before_order_code) == 0)
				$order_condition = "";
			else 
				$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
			$sort_condition = " order by CAST(obd.order_code as UNSIGNED) desc ";
			$limit_condition = " limit $order_count_to_send ";
			
			$final_query = $query.$order_city_condition.$whether_now_or_not.$order_condition.$sort_condition.$limit_condition;
            
            $stmt = $this->conn->prepare($final_query);
            
            $status="Confirmed";
            $stmt->bind_param("s", $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $phonenumber, $orderdate, $ordertotamount,
             $order_status, $username, $userphonenumber, $serv_prov_name, $coupon_code, $rider_requested, $rider_assigned, $rider_accepted);



            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["pending_order_code"] = $ordercode;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["user_code"] = $usercode;
                $tmp["phone_number"] = $phonenumber;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["expected_del_time"] = $expecteddeltime;
                $tmp["sp_name"]=$serv_prov_name;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                $tmp["coupon_code"] = $coupon_code;
                $tmp["rider_requested"] = $rider_requested;
                $tmp["rider_assigned"] = $rider_assigned;
                $tmp["rider_accepted"] = $rider_accepted;
                
                array_push($response["pending_orders_service_providers"], $tmp);
            }
            return $response;
    }

    public function pushCustomerNotification($offer_tag){
        require_once dirname(__FILE__) . '/GCMCUSTOMER.php';
        
        $stmt = "select offer_desc from offer_details where LOWER(offer_tag)=LOWER('$offer_tag')";
        $result=mysqli_query($this->conn, $stmt);
        $row = mysqli_fetch_array($result);
        if($row){
        	$offer_desc = $row["offer_desc"];
        	
        	$message=array("offer" => $offer_desc, "price" => $offer_desc);
			$gcm=new GCMCUSTOMER();
		
			$stmt="select reg_id from user_details";
			$result=mysqli_query($this->conn, $stmt);
		
			$response=array(); $i = 0;
			while($row = mysqli_fetch_array($result)){
				$response[] = $row["reg_id"];
				$i = $i +1;
			
				if($i == 1000){
					$gcm->send_notification($response, $message);
					$i = 0; $response = array();
				}
			}
		
			if($i != 0)
				$gcm->send_notification($response, $message);
			
			$response = "Success";
        }
        else
        	$response = "Problem with Offer Tag";
        	
        
        return $response;
        
    }

    public function pushServiceProviderOrderNotification($servProvId){
        require_once dirname(__FILE__) . '/GCM.php';
        $stmt = "SELECT * FROM service_providers where sp_code='".$servProvId."'";
        $result=mysqli_query($this->conn, $stmt);

        $row_reg_id = mysqli_fetch_array($result);
        $reg_id=array();
        $reg_id[] = $row_reg_id["reg_id"];
        // $temp_reg = "APA91bGU4awZpmHxYV7_mMKI2B2KPc8gzvU8Uqg7N9DxlqWwTniKDeBWgb46AK7QkDC8C8ktxyDptreUv-GviX30gZnWKR4ruzd4YUxL_wqUVxDiNq5zPUyIMjPgO0S2RCdJGwJKWFu906uf0ETyuG8AWbPilJVvaJelV8CYy4UwRatA_D_Cxy6AsjQch-uQ3S2bwrYMB9FM";
//         $reg_id=array($temp_reg);
        
        $message=array("price" => "New Order");
        $gcm=new GCM();
        $gcm->send_notification($reg_id, $message);
        
        $response = array();
        $response["servProv"] = $servProvId;
        $response["reg_id"] = $reg_id;
        $response["message"] = $message;
        return $response;
    }

    public function ConfirmCurrentOrder($current_order_string, $user_code){
        $current_order_code = $this->getOrderCodeFromOrderString($current_order_string);
        
        require_once dirname(__FILE__) . '/GCMCUSTOMER.php';
        $response = array();
        $status="Confirmed";
        $stmt_update="update order_in_progress set status='".$status."' where order_code='".$current_order_code."' and status = 'Pending' ";
        $result_update=mysqli_query($this->conn, $stmt_update);
        if($result_update && 0 != mysqli_affected_rows($this->conn)){
        
        	$query = "insert into order_item_served_by_sp(order_code, user_code, sp_code, item_code, item_cost) 
			select oid.order_code, oid.user_code, oip.sp_code, oid.item_code, oid.item_cost 
			from order_item_details oid, order_in_progress oip where oid.order_code = oip.order_code and
			 oid.order_code = '".$current_order_code."' ";
			
			$stmt_insert_order_items_served_by_sp = $query;
			
			$result_insert_order_items_served_by_sp=mysqli_query($this->conn, $stmt_insert_order_items_served_by_sp);
			if($result_insert_order_items_served_by_sp){
				$response["error"]=1;
				$response["message"]="Insert Successfull";
			}else{
				$response["error"]=0;
				$response["message"]="Insert Unsuccessfull";
			}
        
            $stmt_user="select * from user_details where user_code='".$user_code."'";
            $result=mysqli_query($this->conn, $stmt_user);
            $row_reg_id = mysqli_fetch_array($result);
            $reg_id=array($row_reg_id["reg_id"]);

            $gcm=new GCMCUSTOMER();
            $gcm->send_notification($reg_id, array("price"=>"Order Confirmed"));

        }else{
            $response["error"]=0;
            //TODO if update not done than error handling
        }

        return $response;
    }
    
    public function ConfirmCurrentOrderItemDetails($confirmOrderItems, $current_order_string, $user_code, $serv_prov_code){
        $current_order_code = $this->getOrderCodeFromOrderString($current_order_string);
        
        $response = array();
        
        if(0 == count($confirmOrderItems)){
        	$response["error"]=0;
            $response["message"]="Update Unsuccessful, Atleast one item should be confirmed";
            return $response;
        }
        
        require_once dirname(__FILE__) . '/GCMCUSTOMER.php';
        
        $status="Confirmed";
        $stmt_update="update order_in_progress set status='".$status."' where order_code='".$current_order_code."' and status = 'Pending' ";
        $result_update=mysqli_query($this->conn, $stmt_update);
        if($result_update && 0 != mysqli_affected_rows($this->conn)){
            foreach($confirmOrderItems as $obj){	
                $itemcode=$obj['item_code'];
                
                $query = "insert into order_item_served_by_sp(order_code, user_code, sp_code, item_code, item_cost) 
				select order_code, '".$user_code."', '".$serv_prov_code."', item_code, item_cost 
                from order_item_details where order_code = '".$current_order_code."' and item_code = '".$itemcode."' ";
                
                $stmt_insert_order_items_served_by_sp = $query;
                
                $result_insert_order_items_served_by_sp=mysqli_query($this->conn, $stmt_insert_order_items_served_by_sp);
                if($result_insert_order_items_served_by_sp){
                    $response["error"]=1;
                    $response["message"]="Insert Successful";
                }else{
                    $response["error"]=0;
                    $response["message"]="Insert Unsuccessful";
                }
            }
            
            $query = "(select sum(item_cost * item_quantity) as order_extra_cost from order_item_details where order_code = '".$current_order_code."' and item_code not in 
            (select item_code from order_item_served_by_sp where order_code = '".$current_order_code."' and 
            	sp_code = '".$serv_prov_code."'))";
            $result = mysqli_query($this->conn,$query);
            $row = mysqli_fetch_assoc($result);
			$order_extra_cost = $row["order_extra_cost"];
			
            
            $query = "update items_offered_by_service_provider set item_status = 3 where sp_code = '".$serv_prov_code."' 
            and item_code in 
            (select item_code from order_item_details where order_code = '".$current_order_code."' and item_code not in 
            (select item_code from order_item_served_by_sp where order_code = '".$current_order_code."' and 
            	sp_code = '".$serv_prov_code."'))";
                
            $result_update_out_of_stock = mysqli_query($this->conn, $query);

            $query = " update confirmed_order_details set post_total_amount = post_total_amount - $order_extra_cost, post_total_amount = IF(post_total_amount < $order_extra_cost * (1 + (select tax_percentage from order_basic_details where order_code = '".$current_order_code."') / 100), 0, post_total_amount - $order_extra_cost * (1 + (select tax_percentage from order_basic_details where order_code = '".$current_order_code."') / 100) ) where order_code = '".$current_order_code."' ";
            mysqli_query($this->conn,$query);
            
            $stmt_user="select * from user_details where user_code='".$user_code."'";
            $result=mysqli_query($this->conn, $stmt_user);
            $row_reg_id = mysqli_fetch_array($result);
            $reg_id=array($row_reg_id["reg_id"]);

            $gcm=new GCMCUSTOMER();
            $gcm->send_notification($reg_id, array("price"=>"Order Confirmed, Packing your items. Will be dispached soon."));
        }else{
            $response["error"]=0;
            $response["message"]="Update Unsuccessful";
            //TODO if update not done than error handling
        }
        return $response;
    }
    
    public function ConfirmCurrentOrderItemDetails_v2($confirmOrderItems, $current_order_string, $user_code, $serv_prov_code){
        //With Updated Item Price from seller 
        
        $current_order_code = $this->getOrderCodeFromOrderString($current_order_string);
        
        $response = array();
        
        if(0 == count($confirmOrderItems)){
        	$response["error"]=0;
            $response["message"]="Update Unsuccessful, Atleast one item should be confirmed";
            return $response;
        }
        
        require_once dirname(__FILE__) . '/GCMCUSTOMER.php';
        
        $status="Confirmed";
        $stmt_update="update order_in_progress set status='".$status."' where order_code='".$current_order_code."' and status = 'Pending' ";
        $result_update=mysqli_query($this->conn, $stmt_update);
        if($result_update && 0 != mysqli_affected_rows($this->conn)){
            foreach($confirmOrderItems as $obj){
                $itemcode = $obj['item_code'];
                $itemcost = $obj['item_cost'];
                $priceflag = $obj['price_flag'];
                
                $query = "insert into order_item_served_by_sp(order_code, user_code, sp_code, item_code, item_cost) values (
                '".$current_order_code."', '".$user_code."', '".$serv_prov_code."', '".$itemcode."', '".$itemcost."')";
                $stmt_insert_order_items_served_by_sp = $query;
                
                $result_insert_order_items_served_by_sp=mysqli_query($this->conn, $stmt_insert_order_items_served_by_sp);
                if($result_insert_order_items_served_by_sp){
                    $response["error"]=1;
                    $response["message"]="Insert Successful";
                }else{
                    $response["error"]=0;
                    $response["message"]="Insert Unsuccessful";
                }
                
                if($priceflag == 1){
                	$query = "update items_offered_by_service_provider set item_cost = '".$itemcost."' where 
                	sp_code = '".$serv_prov_code."' and item_code = '".$itemcode."' ";
                 	mysqli_query($this->conn, $query);
                }
                
            }
            
            
            $query = "(select sum(item_cost * item_quantity) as order_extra_cost from order_item_details where order_code = '".$current_order_code."' and item_code not in 
            (select item_code from order_item_served_by_sp where order_code = '".$current_order_code."' and 
            	sp_code = '".$serv_prov_code."'))";
            $result = mysqli_query($this->conn,$query);
            $row = mysqli_fetch_assoc($result);
			$order_extra_cost = $row["order_extra_cost"];
            
            $query = "update items_offered_by_service_provider set item_status = 3 where sp_code = '".$serv_prov_code."' 
            and item_code in 
            (select item_code from order_item_details where order_code = '".$current_order_code."' and item_code not in 
            (select item_code from order_item_served_by_sp where order_code = '".$current_order_code."' and 
            	sp_code = '".$serv_prov_code."'))";
                
            $result_update_out_of_stock = mysqli_query($this->conn, $query);

            $query = " update confirmed_order_details set post_total_amount = post_total_amount - $order_extra_cost, post_total_amount = IF(post_total_amount < $order_extra_cost * (1 + (select tax_percentage from order_basic_details where order_code = '".$current_order_code."') / 100), 0, post_total_amount - $order_extra_cost * (1 + (select tax_percentage from order_basic_details where order_code = '".$current_order_code."') / 100) ) where order_code = '".$current_order_code."' ";
            mysqli_query($this->conn,$query);
            
            
            $stmt_user="select * from user_details where user_code='".$user_code."'";
            $result=mysqli_query($this->conn, $stmt_user);
            $row_reg_id = mysqli_fetch_array($result);
            $reg_id=array($row_reg_id["reg_id"]);

            $gcm=new GCMCUSTOMER();
            $gcm->send_notification($reg_id, array("price"=>"Order Confirmed, Packing your items. Will be dispached soon."));
        }else{
            $response["error"]=0;
            $response["message"]="Update Unsuccessful";
            //TODO if update not done than error handling
        }
        return $response;
    }
	
	public function ConfirmCurrentOrderItemDetailsForLazyLads($confirmOrderItems, $current_order_string, $user_code){
        $current_order_code = $this->getOrderCodeFromOrderString($current_order_string);
        
        $response = array();
        
        if(0 == count($confirmOrderItems)){
        	$response["error"]=0;
            $response["message"]="Update Unsuccessful, Atleast one item should be confirmed";
            return $response;
        }
        
        require_once dirname(__FILE__) . '/GCMCUSTOMER.php';

        $status="Confirmed";
        $stmt_update="update order_in_progress set status='".$status."' where order_code='".$current_order_code."' and status = 'Pending' ";
        $result_update=mysqli_query($this->conn, $stmt_update);
        if($result_update && 0 != mysqli_affected_rows($this->conn)){
			
			$stmt_user="select sp_code from order_in_progress where order_code='".$current_order_code."'";
            $result=mysqli_query($this->conn, $stmt_user);
            $res_array = mysqli_fetch_array($result);
            $serv_prov_code = $res_array["sp_code"];
			
            foreach($confirmOrderItems as $obj){
                $itemcode=$obj['item_code'];
                
                $query = "insert into order_item_served_by_sp(order_code, user_code, sp_code, item_code, item_cost) 
				select order_code, '".$user_code."', '".$serv_prov_code."', item_code, item_cost 
                from order_item_details where order_code = '".$current_order_code."' and item_code = '".$itemcode."' ";
                
                $stmt_insert_order_items_served_by_sp = $query;
                $result_insert_order_items_served_by_sp=mysqli_query($this->conn, $stmt_insert_order_items_served_by_sp);
                if($result_insert_order_items_served_by_sp){
                    $response["error"]=1;
                    $response["message"]="Insert Successful";
                }else{
                    $response["error"]=0;
                    $response["message"]="Insert Unsuccessful";
                }
            }
            
            $query = "(select SUM(item_cost * item_quantity) as order_extra_cost from order_item_details where order_code = '".$current_order_code."' and item_code not in 
            (select item_code from order_item_served_by_sp where order_code = '".$current_order_code."' and 
            	sp_code = '".$serv_prov_code."'))";
            $result = mysqli_query($this->conn,$query);
            $row = mysqli_fetch_assoc($result);
			$order_extra_cost = $row["order_extra_cost"];

            $query = " update confirmed_order_details set post_total_amount = post_total_amount - $order_extra_cost, post_total_amount = IF(post_total_amount < $order_extra_cost * (1 + (select tax_percentage from order_basic_details where order_code = '".$current_order_code."') / 100), 0, post_total_amount - $order_extra_cost * (1 + (select tax_percentage from order_basic_details where order_code = '".$current_order_code."') / 100) ) where order_code = '".$current_order_code."' ";
            mysqli_query($this->conn,$query);

            $stmt_user="select * from user_details where user_code='".$user_code."'";
            $result=mysqli_query($this->conn, $stmt_user);
            $row_reg_id = mysqli_fetch_array($result);
            $reg_id=array($row_reg_id["reg_id"]);

            $gcm=new GCMCUSTOMER();
            $gcm->send_notification($reg_id, array("price"=>"Order Confirmed, Packing your items. Will be dispached soon."));
        }else{
            $response["error"]=0;
            $response["message"]="Update Unsuccessful";
            //TODO if update not done than error handling
        }
        return $response;
    }

    public function ConfirmPendingOrder($pending_order_string, $user_code){
    	$pending_order_code = $this->getOrderCodeFromOrderString($pending_order_string);
    	
    	require_once dirname(__FILE__) . '/GCMCUSTOMER.php';
        
        $response = array();
        $status="Delivered";
        $stmt_update="update order_in_progress set status='".$status."' where order_code='".$pending_order_code."' and status = 'Confirmed' ";
        $result_update=mysqli_query($this->conn, $stmt_update);

        if($result_update && 0 != mysqli_affected_rows($this->conn)){
            $stmt_user="select * from user_details where user_code='".$user_code."'";

            $result=mysqli_query($this->conn, $stmt_user);
            $row_reg_id = mysqli_fetch_array($result);
            $reg_id=array($row_reg_id["reg_id"]);

            $gcm=new GCMCUSTOMER();
            $gcm->send_notification($reg_id, array("price"=>"Congrats your order has been dispached. Will be delivered in sometime at your doors."));

            $this->updateItemConversion($pending_order_code);
            $response["error"]=1;
        }else{
            $response["error"]=0;
            //TODO if update not done than error handling
        }
        
        $delivered_by = 1; // 1 for seller
        
        $this->handleOrderDelivered($pending_order_code, $delivered_by);

        return $response;
    }
    
    public function handleOrderDelivered($order_code, $delivered_by){
    	$settlement_succeed = true;
    	
    	$query = "SELECT obd.user_code, oip.sp_code
        FROM order_basic_details obd, order_in_progress oip
        WHERE oip.order_code = obd.order_code
        AND obd.order_code = ?";
    	$stmt = $this->conn->prepare($query);
    	$stmt->bind_param("s",$order_code);
    	$result = $stmt->execute();
    	if($result){
    		$stmt->bind_result($user_code, $seller_code);
    		if($stmt->fetch()){
    			$transaction_params = array(
					"user_code" => $user_code,
					"owner_type" => "0",
					"urid" => $order_code,
					"urid_type" => "ORDER",
					"t_type" => ORDER_FINISH_CUSTOMER
					);
				$net_api = $this->getNetworkApi();
    	
				$response = $net_api->makeTransaction($transaction_params);
				if(null == $response || false == $response['success'])
					$settlement_succeed = false;
					
				if(1 == $delivered_by){  //1 for seller
					$transaction_params = array(
						"user_code" => $seller_code,
						"owner_type" => "1",
						"urid" => $order_code,
						"urid_type" => "ORDER",
						"t_type" => ORDER_FINISH_SELLER
						);
					$net_api = $this->getNetworkApi();
		
					$response = $net_api->makeTransaction($transaction_params);
					if(null == $response || false == $response['success'])
						$settlement_succeed = false;
				}
			}
    	}
    	return $settlement_succeed;
    }
    
    public function ConfirmPendingOrderItemDetailsForLazyLads($pending_order_string, $user_code){
    	$pending_order_code = $this->getOrderCodeFromOrderString($pending_order_string);
    	
    	require_once dirname(__FILE__) . '/GCMCUSTOMER.php';
        $response = array();
        $status="Delivered";
        $stmt_update="update order_in_progress set status='".$status."' where order_code='".$pending_order_code."' and status = 'Confirmed' ";
        $result_update=mysqli_query($this->conn, $stmt_update);
        if($result_update && 0 != mysqli_affected_rows($this->conn)){
            $stmt_user="select * from user_details where user_code='".$user_code."'";
            $result=mysqli_query($this->conn, $stmt_user);
            $row_reg_id = mysqli_fetch_array($result);
            $reg_id=array($row_reg_id["reg_id"]);

            $gcm=new GCMCUSTOMER();
            $gcm->send_notification($reg_id, array("price"=>"Congrats your order has been dispached. Will be delivered in sometime at your doors."));

            $response["error"]=1;
        }else{
            $response["error"]=0;
            //TODO if update not done than error handling
        }
        
        $delivered_by = 0; // 0 for lazylad
        
        $this->handleOrderDelivered($pending_order_code, $delivered_by);

        return $response;
    }

    public function getCurrentOrdersDetails($serv_prov_id, $current_order_string){
        $current_order_code = $this->getOrderCodeFromOrderString($current_order_string);
        
        if($this->isServiceProviderValid($serv_prov_id)){
            $response = array();
            $response["error"] = 1;
            $response["current_order_details_service_providers"] = array();

            $stmt = $this->conn->prepare("SELECT id.item_code, id.item_name, id.item_desc, id.item_img_flag,
            id.item_img_add, id.item_short_desc, oid.item_cost, oid.item_quantity, id.item_quantity, id.item_unit from order_item_details oid,
            items_offered_by_service_provider iobsp, item_details id where oid.order_code=? and iobsp.sp_code=? 
            and oid.item_code=iobsp.item_code and id.item_code = iobsp.item_code");

            $stmt->bind_param("ss", $current_order_code, $serv_prov_id);

            $stmt->execute();

            $stmt->bind_result($itemcode, $itemname, $itemdesc, $itemimgflag, $itemimgadd, $itemshortdesc, $itemcost, $itemquantity, $itemquant, $itemunit);

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["item_code"] = $itemcode;
                $tmp["item_name"] = $itemname;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_quantity"]=$itemquantity;
                $tmp["item_cost"]=$itemcost;
                $tmp["item_desc"]=$itemquant." ".$itemunit;
                array_push($response["current_order_details_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
  //   public function getCurrentOrdersDetailsForLazyLads($current_order_string){
		// $current_order_code = $this->getOrderCodeFromOrderString($current_order_string);
		
		// $response = array();
		// $response["error"] = 1;
		// $response["current_order_details_service_providers"] = array();

	 //    $stmt_sp="SELECT sp_code from order_in_progress where order_code='".$current_order_code."'";
	 //    $result=mysqli_query($this->conn, $stmt_sp);

	 //    if(mysqli_num_rows($result)==1){
	 //        $row_serv_prov_code_present = mysqli_fetch_array($result);
		// 	$sp_code=$row_serv_prov_code_present["sp_code"];
		// 	$stmt = $this->conn->prepare("SELECT id.item_code, id.item_name, id.item_desc, id.item_img_flag,
		// 	id.item_img_add, id.item_short_desc, oid.item_cost, oid.item_quantity, id.item_quantity, id.item_unit from order_item_details oid,
		// 	items_offered_by_service_provider iobsp ,item_details id where oid.order_code=? and iobsp.sp_code=? and 
  //           oid.item_code=iobsp.item_code and id.item_code=iobsp.item_code");

		// 	$stmt->bind_param("ss", $current_order_code, $sp_code);

		// 	$stmt->execute();

		// 	$stmt->bind_result($itemcode, $itemname, $itemdesc, $itemimgflag, $itemimgadd, $itemshortdesc, $itemcost, $itemquantity, $itemquant, $itemunit);

		// 	// looping through result and preparing tasks array
		// 	while ($stmt->fetch()) {
		// 		$tmp = array();
		// 		$tmp["item_code"] = $itemcode;
		// 		$tmp["item_name"] = $itemname;
  //               echo $itemname.' '.$itemcode;
		// 		$tmp["item_img_flag"] = $itemimgflag;
		// 		$tmp["item_img_address"] = $itemimgadd;
		// 		$tmp["item_short_desc"] = $itemshortdesc;
		// 		$tmp["item_quantity"]=$itemquantity;
		// 		$tmp["item_cost"]=$itemcost;
		// 		$tmp["item_desc"]=$itemquant." ".$itemunit;
		// 		array_push($response["current_order_details_service_providers"], $tmp);
		// 	}

	 //    }
  //           return $response;
  //   }

    public function getCurrentOrdersDetailsForLazyLads($current_order_string){
        $current_order_code = $this->getOrderCodeFromOrderString($current_order_string);

        $response = array();
        $response["error"] = 1;
        $response["current_order_details_service_providers"] = array();

        $stmt_sp="SELECT sp_code from order_in_progress where order_code='".$current_order_code."'";
        $result=mysqli_query($this->conn, $stmt_sp);

        if(mysqli_num_rows($result)==1){
            $row_serv_prov_code_present = mysqli_fetch_array($result);
            $sp_code=$row_serv_prov_code_present["sp_code"];
            $stmt = $this->conn->prepare("SELECT iobsp.item_code, id.item_name, id.item_desc, id.item_img_flag,
            id.item_img_add, id.item_short_desc, oid.item_cost, oid.item_quantity, id.item_quantity, id.item_unit
            from order_item_details oid,
            items_offered_by_service_provider iobsp, item_details id where oid.order_code=? and iobsp.sp_code=? 
            and oid.item_code=iobsp.item_code and id.item_code=iobsp.item_code");

            $stmt->bind_param("ss", $current_order_code, $sp_code);

            $stmt->execute();

            $stmt->bind_result($itemcode, $itemname, $itemdesc, $itemimgflag, $itemimgadd, $itemshortdesc, $itemcost, $itemquantity, $itemquant, $itemunit);

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["item_code"] = $itemcode;
                $tmp["item_name"] = $itemname;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_quantity"]=$itemquantity;
                $tmp["item_cost"]=$itemcost;
                $tmp["item_desc"]=$itemquant.' '.$itemunit;
                array_push($response["current_order_details_service_providers"], $tmp);
            }

        }
            return $response;
    }
    
    public function getPendingOrdersDetails($serv_prov_id, $pending_order_string){
        $pending_order_code = $this->getOrderCodeFromOrderString($pending_order_string);
        
        if($this->isServiceProviderValid($serv_prov_id)){
            $response = array();
            $response["error"] = 1;
            $response["pending_order_details_service_providers"] = array();

            $stmt = $this->conn->prepare("SELECT iobsp.item_code, id.item_name, id.item_desc, id.item_img_flag,
            id.item_img_add, id.item_short_desc, oisbsp.item_cost, oid.item_quantity, id.item_quantity, id.item_unit from order_item_details oid, order_item_served_by_sp oisbsp, 
            items_offered_by_service_provider iobsp, item_details id where oisbsp.order_code=? and iobsp.sp_code=? and oisbsp.item_code=iobsp.item_code 
            and oid.order_code=oisbsp.order_code and oid.item_code=oisbsp.item_code and id.item_code = iobsp.item_code");

            $stmt->bind_param("ss", $pending_order_code, $serv_prov_id);

            $stmt->execute();

            $stmt->bind_result($itemcode, $itemname, $itemdesc, $itemimgflag, $itemimgadd, $itemshortdesc, $itemcost, $itemquantity, $itemquant, $itemunit);

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["item_code"] = $itemcode;
                $tmp["item_name"] = $itemname;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_quantity"]=$itemquantity;
                $tmp["item_cost"]=$itemcost;
                $tmp["item_desc"]=$itemquant.' '.$itemunit;
                array_push($response["pending_order_details_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
    public function getPendingOrdersDetailsForLazyLads($pending_order_string){
    	$pending_order_code = $this->getOrderCodeFromOrderString($pending_order_string);
    	
		$response = array();
		$response["error"] = 1;
		$response["pending_order_details_service_providers"] = array();
	    $stmt_sp="SELECT sp_code from order_in_progress where order_code='".$pending_order_code."'";
	    $result=mysqli_query($this->conn, $stmt_sp);

	    if(mysqli_num_rows($result)==1){
	        $row_serv_prov_code_present = mysqli_fetch_array($result);
			$sp_code=$row_serv_prov_code_present["sp_code"];
            	
			$query = "SELECT iobsp.item_code, id.item_name, id.item_desc, id.item_img_flag,
			id.item_img_add, id.item_short_desc, oisbsp.item_cost, oid.item_quantity, id.item_quantity, id.item_unit from order_item_details oid,
			 order_item_served_by_sp oisbsp, items_offered_by_service_provider iobsp , item_details id
			 where oisbsp.order_code=? and iobsp.sp_code=? and oisbsp.item_code=iobsp.item_code 
			 and oid.order_code=oisbsp.order_code and oid.item_code=oisbsp.item_code and id.item_code = iobsp.item_code";

			$stmt = $this->conn->prepare($query);
			$stmt->bind_param("ss", $pending_order_code, $sp_code);

			$stmt->execute();

			$stmt->bind_result($itemcode, $itemname, $itemdesc, $itemimgflag, $itemimgadd, $itemshortdesc, $itemcost, $itemquantity, $itemquant, $itemunit);

			// looping through result and preparing tasks array
			while ($stmt->fetch()) {
				$tmp = array();
				$tmp["item_code"] = $itemcode;
				$tmp["item_name"] = $itemname;
				$tmp["item_img_flag"] = $itemimgflag;
				$tmp["item_img_address"] = $itemimgadd;
				$tmp["item_short_desc"] = $itemshortdesc;
				$tmp["item_quantity"]=$itemquantity;
				$tmp["item_cost"]=$itemcost;
				$tmp["item_desc"]=$itemquant.' '.$itemunit;;
				array_push($response["pending_order_details_service_providers"], $tmp);
			}

	    }
        return $response;
    }
    
    public function getDeliveredOrdersDetails($serv_prov_id, $delivered_order_string){
        $delivered_order_code = $this->getOrderCodeFromOrderString($delivered_order_string);
        
        if($this->isServiceProviderValid($serv_prov_id)){
            $response = array();
            $response["error"] = 1;
            $response["delivered_order_details_service_providers"] = array();

            $stmt = $this->conn->prepare("SELECT iobsp.item_code, id.item_name, id.item_desc, id.item_img_flag,
            id.item_img_add, id.item_short_desc, oisbsp.item_cost, oid.item_quantity from order_item_details oid, order_item_served_by_sp oisbsp, 
            items_offered_by_service_provider iobsp , item_details id where oisbsp.order_code=? and iobsp.sp_code=? 
            and oisbsp.item_code=iobsp.item_code and oid.order_code=oisbsp.order_code and oid.item_code=oisbsp.item_code and id.item_code = iobsp.item_code");

            $stmt->bind_param("ss", $delivered_order_code, $serv_prov_id);

            $stmt->execute();

            $stmt->bind_result($itemcode, $itemname, $itemdesc, $itemimgflag, $itemimgadd, $itemshortdesc, $itemcost, $itemquantity);

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["item_code"] = $itemcode;
                $tmp["item_name"] = $itemname;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_quantity"]=$itemquantity;
                $tmp["item_cost"]=$itemcost;
                $tmp["item_desc"]=$itemdesc;
                array_push($response["delivered_order_details_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
    public function getDeliveredOrdersDetailsForLazyLads($delivered_order_code){
        $delivered_order_code = $this->getOrderCodeFromOrderString($delivered_order_string);
            
		$response = array();
		$response["error"] = 1;
		$response["delivered_order_details_service_providers"] = array();

	    
	    $stmt_sp="SELECT sp_code from order_in_progress where order_code='".$delivered_order_code."'";
	    $result=mysqli_query($this->conn, $stmt_sp);

	    if(mysqli_num_rows($result)==1){
	        $row_serv_prov_code_present = mysqli_fetch_array($result);
			$sp_code=$row_serv_prov_code_present["sp_code"];
            	
			$query = "SELECT iobsp.item_code, id.item_name, id.item_desc, id.item_img_flag,
			id.item_img_add, id.item_short_desc, oisbsp.item_cost, oid.item_quantity from order_item_details oid,
			 order_item_served_by_sp oisbsp, items_offered_by_service_provider iobsp , item_details id 
			 where oisbsp.order_code=? and iobsp.sp_code=? and oisbsp.item_code=iobsp.item_code 
			 and oid.order_code=oisbsp.order_code and oid.item_code=oisbsp.item_code and id.item_code = iobsp.item_code";

			$stmt = $this->conn->prepare($query);
			$stmt->bind_param("ss", $delivered_order_code, $sp_code);

			$stmt->execute();

			$stmt->bind_result($itemcode, $itemname, $itemdesc, $itemimgflag, $itemimgadd, $itemshortdesc, $itemcost, $itemquantity);

			// looping through result and preparing tasks array
			while ($stmt->fetch()) {
				$tmp = array();
				$tmp["item_code"] = $itemcode;
				$tmp["item_name"] = $itemname;
				$tmp["item_img_flag"] = $itemimgflag;
				$tmp["item_img_address"] = $itemimgadd;
				$tmp["item_short_desc"] = $itemshortdesc;
				$tmp["item_quantity"]=$itemquantity;
				$tmp["item_cost"]=$itemcost;
				$tmp["item_desc"]=$itemdesc;
				array_push($response["delivered_order_details_service_providers"], $tmp);
			}

	    }
        return $response;
    }
    
    public function getUserPreviousOrdersDetails($order_string, $user_code){
    	$order_code = $this->getOrderCodeFromOrderString($order_string);
    	
		$response = array();
		$response["error"] = 1;
		$response["previous_orders_details"] = array();

		$stmt = $this->conn->prepare("SELECT iobsp.item_code, id.item_name, id.item_desc, id.item_img_flag,
		id.item_img_add, id.item_short_desc, oid.item_cost, oid.item_offer_price, oid.item_offer_flag, oid.item_quantity from order_item_details oid, order_in_progress oip,
		items_offered_by_service_provider iobsp , item_details id where oip.order_code=? 
		and oip.user_code in 
		(select ud1.user_code from user_details ud1, user_details ud2 where ud1.phone_number = ud2.phone_number 
		and ud2.user_code = ?) 
		and oid.order_code=? and iobsp.sp_code=oip.sp_code and oid.item_code=iobsp.item_code and id.item_code=iobsp.item_code");

		$stmt->bind_param("sss", $order_code, $user_code, $order_code);

		$stmt->execute();

		$stmt->bind_result($itemcode, $itemname, $itemdesc, $itemimgflag, $itemimgadd, $itemshortdesc, $itemcost, $item_offer_price, $item_offer_flag, $itemquantity);

		// looping through result and preparing tasks array
		while ($stmt->fetch()) {
			$tmp = array();
			$tmp["item_code"] = $itemcode;
			$tmp["item_name"] = $itemname;
			$tmp["item_img_flag"] = $itemimgflag;
			$tmp["item_img_address"] = $itemimgadd;
			$tmp["item_short_desc"] = $itemshortdesc;
			$tmp["item_quantity"]=$itemquantity;
            if ($item_offer_flag == 1){
                $tmp["item_cost"]=$item_offer_price;
            }
            else
                $tmp["item_cost"]=$itemcost;
			$tmp["item_description"]=$itemdesc;
			array_push($response["previous_orders_details"], $tmp);
		}
		return $response;
    }
    
    public function getConfirmedPreviousOrdersDetails($order_string, $user_code){
        $order_code = $this->getOrderCodeFromOrderString($order_string);
            
		$response = array();
		$response["error"] = 1;
		$response["previous_orders_confirmed_details"] = array();

		$stmt = $this->conn->prepare("SELECT iobsp.item_code, id.item_name, id.item_desc, id.item_img_flag,
		id.item_img_add, id.item_short_desc, oid.item_cost, oid.item_quantity from order_item_served_by_sp oisbsp,
		 items_offered_by_service_provider iobsp, order_item_details oid , item_details id where oisbsp.order_code=? 
		 and oisbsp.user_code in 
		(select ud1.user_code from user_details ud1, user_details ud2 where ud1.phone_number = ud2.phone_number 
		and ud2.user_code = ?)
		  and iobsp.item_code=oisbsp.item_code and iobsp.sp_code=oisbsp.sp_code and oid.order_code=oisbsp.order_code
		   and oid.user_code=oisbsp.user_code and oid.item_code=oisbsp.item_code and id.item_code = oisbsp.item_code");

		$stmt->bind_param("ss", $order_code, $user_code);

		$stmt->execute();

		$stmt->bind_result($itemcode, $itemname, $itemdesc, $itemimgflag, $itemimgadd, $itemshortdesc, $itemcost, $itemquantity);

		// looping through result and preparing tasks array
		while ($stmt->fetch()) {
			$tmp = array();
			$tmp["item_code"] = $itemcode;
			$tmp["item_name"] = $itemname;
			$tmp["item_img_flag"] = $itemimgflag;
			$tmp["item_img_address"] = $itemimgadd;
			$tmp["item_short_desc"] = $itemshortdesc;
			$tmp["item_quantity"]=$itemquantity;
			$tmp["item_cost"]=$itemcost;
			$tmp["item_description"]=$itemdesc;
			array_push($response["previous_orders_confirmed_details"], $tmp);
		}
		return $response;
    }

    public function getDeliveredOrders($serv_prov_id){
        if($this->isServiceProviderValid($serv_prov_id)){
            $response = array();
            $response["error"] = 1;
            $response["delivered_orders_service_providers"] = array();
            $stmt = $this->conn->prepare("SELECT os.order_string, obd.expected_del_time, uad.user_address, 
            ud.user_code, obd.order_time, cod.post_total_amount, uad.user_name, uad.user_phone_number
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, user_details ud, 
            order_strings os, confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.sp_code = ?
            AND oin.status =  ?
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND obd.user_code = ud.user_code
            AND cod.order_code = oin.order_code");

            $status="Delivered";

            $stmt->bind_param("ss", $serv_prov_id, $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $deliverytime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber);

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["delivered_order_code"] = $ordercode;
                $tmp["delivery_time"] = $deliverytime;
                //$tmp["total_time_taken"] = $tottimetaken;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["sp_code"]=$serv_prov_id;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                array_push($response["delivered_orders_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
    public function getDeliveredOrdersForLazyLads(){
            $response = array();
            $response["error"] = 1;
            $response["delivered_orders_service_providers"] = array();
            $stmt = $this->conn->prepare("SELECT os.order_string, obd.expected_del_time, uad.user_address, 
            ud.user_code, obd.order_time, cod.post_total_amount, uad.user_name, uad.user_phone_number, 
            sps.sp_name
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, 
            service_providers sps, user_details ud, order_strings os, confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.status = ?
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND obd.user_code = ud.user_code
            AND sps.sp_code = oin.sp_code
            AND cod.order_code = obd.order_code");

            $status="Delivered";

            $stmt->bind_param("s", $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $deliverytime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber, $serv_prov_name);

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["delivered_order_code"] = $ordercode;
                $tmp["delivery_time"] = $deliverytime;
                //$tmp["total_time_taken"] = $tottimetaken;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["sp_name"]=$serv_prov_name;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                array_push($response["delivered_orders_service_providers"], $tmp);
            }
            return $response;
    }
    
    public function getDeliveredOrdersForLazyLadsLimited($before_order_string, $order_filter){
    		$before_order_code = $this->getOrderCodeFromOrderString($before_order_string);
    		
    		$order_count_to_send = 30;
    
            $response = array();
            $response["error"] = 1;
            $response["delivered_orders_service_providers"] = array();
            
            $query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , uad.user_address, 
            ud.user_code, ud.phone_number, obd.order_time, cod.post_total_amount, oin.status, uad.user_name,
             uad.user_phone_number, sps.sp_name, obd.coupon_code
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, service_providers sps,
            user_details ud, order_strings os, confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.status =  'Delivered'
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND obd.user_code = ud.user_code
            AND sps.sp_code = oin.sp_code
            AND cod.order_code = obd.order_code";
            
            if($order_filter && isset($order_filter[self::ORDER_FILTER_CITIES]) &&
            	count($order_filter[self::ORDER_FILTER_CITIES]) ){
				$order_city_condition = " and uad.user_city in (select city_name from city_details where city_code 
            	in (".implode(',',$order_filter[self::ORDER_FILTER_CITIES]).")) ";
            	/*$order_city_condition = "";	*/
            }
            else
            	$order_city_condition = "";
            
            if(intval($before_order_code) == 0)
				$order_condition = "";
			else 
				$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
			$sort_condition = " order by CAST(obd.order_code as UNSIGNED) desc ";
			$limit_condition = " limit $order_count_to_send ";
			
			$final_query = $query.$order_city_condition.$order_condition.$sort_condition.$limit_condition;
            
            $stmt = $this->conn->prepare($final_query);

            $status="Delivered";

            $stmt->bind_param("s", $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $deliverytime, $deladdress, $usercode, $phonenumber, $orderdate, $ordertotamount, 
            $order_status, $username, $userphonenumber, $serv_prov_name, $coupon_code);

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["delivered_order_code"] = $ordercode;
                $tmp["delivery_time"] = $deliverytime;
                //$tmp["total_time_taken"] = $tottimetaken;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["sp_name"]=$serv_prov_name;
                $tmp["user_code"]=$usercode;
                $tmp["phone_number"]=$phonenumber;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                $tmp["coupon_code"] = $coupon_code;
                array_push($response["delivered_orders_service_providers"], $tmp);
            }
            return $response;    
    }


    public function getServiceTypes(){
        $response = array();
        $response["error"] = 1;   // 1 for false
        $response["service_types"] = array();
        $stmt = $this->conn->prepare("SELECT id, st_code, st_name FROM service_types where is_service = 0");

        $stmt->execute();

        $stmt->bind_result($id, $stcode, $stname);

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["st_code"] = $stcode;
            $tmp["st_name"] = $stname;
            array_push($response["service_types"], $tmp);
        }
        return $response;
    }


    public function getServiceTypes_v2(){
        $response = array();
        $response["error"] = 1;   // 1 for false
        $response["service_types"] = array();
        $stmt = $this->conn->prepare("SELECT id, st_code, st_name FROM service_types ");

        $stmt->execute();

        $stmt->bind_result($id, $stcode, $stname);

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["st_code"] = $stcode;
            $tmp["st_name"] = $stname;
            array_push($response["service_types"], $tmp);
        }
        return $response;
    }


    public function getServiceProvidersForServiceType($st_code, $area_name){
        $response = array();
        $response["error"] = 1;   // 1 for false
        $response["service_providers"] = array();

        $area_code=$this->getAreaCode($area_name);

        $stmt = $this->conn->prepare("SELECT sp.sp_code, sp.sp_name, sp.sp_number, spdd.sp_delivery_time, spdd.sp_min_order FROM service_providers sp,
                 service_providers_delivery_details spdd where sp.sp_service_type_code=? and spdd.area_code=? and sp.sp_code=spdd.sp_code and sp.shop_approval_status = 'SHOP_APPROVED'");

        $stmt->bind_param("ss", $st_code, $area_code);

        $stmt->execute();

        $stmt->bind_result($spcode, $spname, $spnumber, $spdeltime, $spminorder);

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["sp_code"] = $spcode;
            $tmp["sp_name"] = $spname;
            $tmp["sp_number"] = $spnumber;
            $tmp["sp_del_time"] = $spdeltime;
            $tmp["sp_min_order"] = $spminorder;
            array_push($response["service_providers"], $tmp);
        }
        return $response;
    }
    
    public function getServiceProvidersForServiceTypeGSON($st_code, $area_code){
        $response = array();
        $response["error"] = 1;   // 1 for false
        $response["service_providers"] = array();
        
        //$area_code=$this->getAreaCode($area_name);

        $stmt = $this->conn->prepare("SELECT sp.sp_code, sp.sp_name, sp.sp_number, sp.sp_service_type_code,
         sp.shop_open_time, sp.shop_close_time, spdd.sp_delivery_time, spdd.sp_min_order, sp.images_available,
          sp.tax_flag_1, sp.tax_display_1, sp.tax_number_1, sp.tax_flag_2, sp.tax_display_2, sp.tax_number_2,
           sp.tax_flag_3, sp.tax_display_3, sp.tax_number_3 FROM service_providers sp,
          service_providers_delivery_details spdd where sp.sp_service_type_code=? and spdd.area_code=? and 
          sp.sp_code=spdd.sp_code and sp.shop_approval_status = 'SHOP_APPROVED' order by spdd.score desc");

        $stmt->bind_param("ss", $st_code, $area_code);

        $stmt->execute();

        $stmt->bind_result($spcode, $spname,  $spnumber, $sptype, $shop_open_time, $shop_close_time, $spdeltime, $spminorder, $images_available, $tax_flag_1, $tax_display_1, $tax_number_1, $tax_flag_2, $tax_display_2, $tax_number_2, $tax_flag_3, $tax_display_3, $tax_number_3);

        // looping through result and preparing tasks array
        $i=1;
        while ($stmt->fetch()) {
            $tmp = array();
            
            $tmp["id"] = $i;
            $tmp["sp_code"] = $spcode;
            $tmp["sp_name"] = $spname;
            $tmp["sp_number"] = $spnumber;
            $tmp["sp_type"] = $sptype;
            $tmp["shop_open_time"] = $shop_open_time;
            $tmp["shop_close_time"] = $shop_close_time;
            $tmp["sp_del_time"] = $spdeltime;
            $tmp["sp_min_order"] = $spminorder;
            $tmp["images_available"] = $images_available;
            $tmp["seller_taxes"] = array(
                "tax_flag_1" => $tax_flag_1,
                "tax_display_1" => $tax_display_1,
                "tax_number_1" => $tax_number_1,
                "tax_flag_2" => $tax_flag_2,
                "tax_display_2" => $tax_display_2,
                "tax_number_2" => $tax_number_2,
                "tax_flag_3" => $tax_flag_3,
                "tax_display_3" => $tax_display_3,
                "tax_number_3" => $tax_number_3,
            );
            array_push($response["service_providers"], $tmp);
            $i++;
        }
        return $response;
    }

    public function getAreaCode($areaname){
        $stmt = $this->conn->prepare("SELECT area_code FROM area_details
                                        where area_name=? and city_code=?");

        $citycode="1"; //only gurgaon
        $stmt->bind_param("ss", $areaname, $citycode);

        $stmt->execute();

        $stmt->bind_result($areacode);
        
        $area_code = "";
        while ($stmt->fetch()) {
            $area_code=$areacode;
        }
        return $area_code;
    }

    public function getItemsServiceProviderSpecific($serv_prov_code, $serv_type_code){
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt = $this->conn->prepare("SELECT * FROM items_offered_by_service_provider where sp_code=?");
            $stmt->bind_param("s", $serv_prov_code);
            $stmt->execute();
            $stmt->bind_result($id, $itemcode, $spcode, $itemtypecode, $itemname, $itemunit, $itemquant, $itemcost, $itemimgflag, $itemimgadd
                , $itemshortdesc, $itemdesc, $itemstatus, $itemservcat);

            $response = array();
            $response["error"] = 1;
            $response["items_service_providers"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["item_code"] = $itemcode;
                $tmp["sp_code"] = $spcode;
                $tmp["item_type_code"] = $itemtypecode;
                $tmp["item_name"] = $itemname;
                $tmp["item_unit"] = $itemunit;
                $tmp["item_quantity"] = $itemquant;
                $tmp["item_cost"] = $itemcost;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_desc"] = $itemdesc;
                $tmp["item_status"] = $itemstatus;
                array_push($response["items_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
    public function getItemsServiceProviderCategoryWise($serv_prov_code, $serv_type_code, $serv_category_code){
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt = $this->conn->prepare("SELECT * FROM items_offered_by_service_provider where sp_code=? and sc_code=?");
            
            $stmt->bind_param("ss", $serv_prov_code, $serv_category_code);
            $stmt->execute();
            $stmt->bind_result($id, $itemcode, $spcode, $itemtypecode, $itemname, $itemunit, $itemquant, $itemcost, $itemimgflag, $itemimgadd
                , $itemshortdesc, $itemdesc, $itemstatus, $itemservcode);

            $response = array();
            $response["error"] = 1;
            $response["items_service_providers"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["item_code"] = $itemcode;
                $tmp["sp_code"] = $spcode;
                $tmp["item_type_code"] = $itemtypecode;
                $tmp["item_name"] = $itemname;
                $tmp["item_unit"] = $itemunit;
                $tmp["item_quantity"] = $itemquant;
                $tmp["item_cost"] = $itemcost;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_desc"] = $itemdesc;
                $tmp["item_status"] = $itemstatus;
                array_push($response["items_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
    public function getItemsServiceProviderCategoryWiseFinal($serv_prov_code, $serv_type_code, $serv_category_code){
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt = $this->conn->prepare("SELECT iosp.`id` , iosp.`item_code` , iosp.sp_code, iosp.item_type_code, 
            	iosp.item_name, iosp.item_unit, iosp.item_quantity, iosp.item_cost, iosp.item_img_flag, iosp.item_img_add,
            	 iosp.item_short_desc, iosp.item_desc, iosp.item_status, iosp.sc_code 
            	 FROM items_offered_by_service_provider where sp_code=? and sc_code=?");
            
            $stmt->bind_param("ss", $serv_prov_code, $serv_category_code);
            $stmt->execute();
            $stmt->bind_result($id, $itemcode, $spcode, $itemtypecode, $itemname, $itemunit, $itemquant, $itemcost, $itemimgflag, $itemimgadd
                , $itemshortdesc, $itemdesc, $itemstatus, $itemservcode);

            $response = array();
            $response["error"] = 1;
            $response["items_service_providers"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["item_code"] = $itemcode;
                $tmp["sp_code"] = $spcode;
                $tmp["item_type_code"] = $itemtypecode;
                $tmp["item_name"] = $itemname;
                $tmp["item_unit"] = $itemunit;
                $tmp["item_quantity"] = $itemquant;
                $tmp["item_cost"] = $itemcost;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_desc"] = $itemdesc;
                $tmp["item_status"] = $itemstatus;
                $tmp["item_selected"] = 0;
                $tmp["item_quantity_selected"] = 0;
                $tmp["item_service_type"] = $serv_type_code;
                $tmp["item_service_category"] = $serv_category_code;
                array_push($response["items_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
    public function getItemsServiceProviderCategoryWiseFinalWithMRP($serv_prov_code, $serv_type_code, $serv_category_code){
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt = $this->conn->prepare("SELECT iosp.`id` , iosp.`item_code` , iosp.sp_code, id.item_type_code, id.item_name, id.item_unit, id.item_quantity, iosp.item_cost, id.item_img_flag, id.item_img_add, id.item_short_desc, id.item_desc, iosp.item_status, id.sc_code, id.item_cost
FROM items_offered_by_service_provider iosp, item_details id
WHERE iosp.sp_code = ? and iosp.sc_code=? AND id.item_code = iosp.item_code AND iosp.item_status = 1");
            
            $stmt->bind_param("ss", $serv_prov_code, $serv_category_code);
            $stmt->execute();
            $stmt->bind_result($id, $itemcode, $spcode, $itemtypecode, $itemname, $itemunit, $itemquant, $itemcost, $itemimgflag, $itemimgadd
                , $itemshortdesc, $itemdesc, $itemstatus, $itemservcode, $itemmrp);

            $response = array();
            $response["error"] = 1;
            $response["items_service_providers"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["item_code"] = $itemcode;
                $tmp["sp_code"] = $spcode;
                $tmp["item_type_code"] = $itemtypecode;
                $tmp["item_name"] = $itemname;
                $tmp["item_unit"] = $itemunit;
                $tmp["item_quantity"] = $itemquant;
                $tmp["item_cost"] = $itemcost;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_desc"] = $itemquant.' '.$itemunit;
                $tmp["item_status"] = $itemstatus;
                $tmp["item_selected"] = 0;
                $tmp["item_quantity_selected"] = 0;
                $tmp["item_service_type"] = $serv_type_code;
                $tmp["item_service_category"] = $serv_category_code;
                $tmp["item_mrp"] = $itemmrp;
                array_push($response["items_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    

    public function getItemsServiceProviderCategoryWiseFinalWithMRPLimited($serv_prov_code, $serv_type_code, $serv_category_code, 
     $previous_item_code){
     	$item_count_to_send = 30;
     
        if($this->isServiceProviderValid($serv_prov_code)){
        
        	$query = "SELECT iosp.id , iosp.item_code , iosp.sp_code, id.item_type_code,
             id.item_name, id.item_unit, id.item_quantity, iosp.item_cost, id.item_img_flag, id.item_img_add,
              id.item_short_desc, CONCAT(id.item_quantity, ' ', id.item_unit), iosp.item_status, id.sc_code,
               id.item_cost, iosp.sp_bfr_offer_price, iosp.b_in_offer, iosp.offer_special_price, iosp.in_cart,
                iosp.max_special_quantity, iosp.min_special_amount, iosp.in_offer
			FROM items_offered_by_service_provider iosp, item_details id
			WHERE iosp.sp_code = ? AND id.item_code = iosp.item_code and id.sc_code=? AND iosp.item_status = 1 ";
            
            if(intval($previous_item_code) == 0)
					$item_condition = "";
				else 
					$item_condition = " and CAST(iosp.item_code as UNSIGNED) > $previous_item_code ";
			$sort_condition = " order by CAST(iosp.item_code as UNSIGNED) ";
			$limit_condition = " limit $item_count_to_send ";
				
			$final_query = $query.$item_condition.$sort_condition.$limit_condition;
				
            $stmt = $this->conn->prepare($final_query);			
            
            $stmt->bind_param("ss", $serv_prov_code, $serv_category_code);
            $stmt->execute();
            $stmt->bind_result($id, $itemcode, $spcode, $itemtypecode, $itemname, $itemunit, $itemquant, $itemcost, $itemimgflag, $itemimgadd
                , $itemshortdesc, $itemdesc, $itemstatus, $itemservcode, $itemmrp, $itemofferprice, $iteminoffer,
                $offerspecialprice, $incart, $maxspecialqty, $mimspecialamount, $inoffer);

            $response = array();
            $response["error"] = 1;
            $response["items_service_providers"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["item_code"] = $itemcode;
                $tmp["sp_code"] = $spcode;
                $tmp["item_type_code"] = $itemtypecode;
                $tmp["item_name"] = $itemname;
                $tmp["item_unit"] = $itemunit;
                $tmp["item_quantity"] = $itemquant;
                $tmp["item_cost"] = $itemcost;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_desc"] = $itemdesc;
                $tmp["item_status"] = $itemstatus;
                $tmp["item_selected"] = 0;
                $tmp["item_quantity_selected"] = 0;
                $tmp["item_service_type"] = $serv_type_code;
                $tmp["item_service_category"] = $serv_category_code;
                $tmp["item_mrp"] = $itemmrp;
                if($itemofferprice == 0)
                    $tmp["item_offer_price"] = $itemcost;
                else
                    $tmp["item_offer_price"] = $itemofferprice;
                $tmp["item_in_offer"] = $iteminoffer;
                $tmp["offer_special_price"] = $offerspecialprice;
                $tmp["in_cart"] = $incart;
                $tmp["max_special_qty"] = $maxspecialqty;
                $tmp["min_special_amount"] = $mimspecialamount;
                $tmp["in_special_offer"] = $inoffer;
                array_push($response["items_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }


    public function getInSpecialOfferItemsServiceProviderCategoryWiseFinalWithMRPLimited($serv_prov_code, $serv_type_code, $previous_item_code){
        $item_count_to_send = 30;

        if($this->isServiceProviderValid($serv_prov_code)){

            $query = "SELECT iosp.id , iosp.item_code , iosp.sp_code, id.item_type_code,
             id.item_name, id.item_unit, id.item_quantity, iosp.item_cost, id.item_img_flag, id.item_img_add,
              id.item_short_desc, CONCAT(id.item_quantity, ' ', id.item_unit), iosp.item_status, id.sc_code,
               id.item_cost, iosp.sp_bfr_offer_price, iosp.b_in_offer, iosp.offer_special_price, iosp.in_cart,
                iosp.max_special_quantity, iosp.min_special_amount, iosp.in_offer, iosp.item_special_offer_image
			FROM items_offered_by_service_provider iosp, item_details id
			WHERE iosp.sp_code = ? AND id.item_code = iosp.item_code and iosp.in_offer = 1 AND iosp.item_status = 1 ";

            if(intval($previous_item_code) == 0)
                $item_condition = "";
            else
                $item_condition = " and CAST(iosp.item_code as UNSIGNED) > $previous_item_code ";
            $sort_condition = " order by CAST(iosp.item_code as UNSIGNED) ";
            $limit_condition = " limit $item_count_to_send ";

            $final_query = $query.$item_condition.$sort_condition.$limit_condition;

            $stmt = $this->conn->prepare($final_query);

            $stmt->bind_param("s", $serv_prov_code);
            $stmt->execute();
            $stmt->bind_result($id, $itemcode, $spcode, $itemtypecode, $itemname, $itemunit, $itemquant, $itemcost, $itemimgflag, $itemimgadd
                , $itemshortdesc, $itemdesc, $itemstatus, $itemservcode, $itemmrp, $itemofferprice, $iteminoffer,
                $offerspecialprice, $incart, $maxspecialqty, $mimspecialamount, $inoffer, $itemspofferimage);

            $response = array();
            $response["error"] = 1;
            $response["items_service_providers"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["item_code"] = $itemcode;
                $tmp["sp_code"] = $spcode;
                $tmp["item_type_code"] = $itemtypecode;
                $tmp["item_name"] = $itemname;
                $tmp["item_unit"] = $itemunit;
                $tmp["item_quantity"] = $itemquant;
                $tmp["item_cost"] = $itemcost;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_desc"] = $itemdesc;
                $tmp["item_status"] = $itemstatus;
                $tmp["item_selected"] = 0;
                $tmp["item_quantity_selected"] = 0;
                $tmp["item_service_type"] = $serv_type_code;
                $tmp["item_service_category"] = 0;
                $tmp["item_mrp"] = $itemmrp;
                if($itemofferprice == 0)
                    $tmp["item_offer_price"] = $itemcost;
                else
                    $tmp["item_offer_price"] = $itemofferprice;
                $tmp["item_in_offer"] = $iteminoffer;
                $tmp["offer_special_price"] = $offerspecialprice;
                $tmp["in_cart"] = $incart;
                $tmp["max_special_qty"] = $maxspecialqty;
                $tmp["min_special_amount"] = $mimspecialamount;
                $tmp["in_special_offer"] = $inoffer;
                $tmp["item_special_offer_image"] = $itemspofferimage;
                array_push($response["items_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
    public function searchItemsSPCategoryWiseLimited($serv_prov_code, $serv_type_code, $serv_category_code,
    	$search_string, $previous_item_code){
    	$item_count_to_send = 30;
     
        if($this->isServiceProviderValid($serv_prov_code)){
        
        	$query = "SELECT iosp.id , iosp.item_code , iosp.sp_code, id.item_type_code,
             id.item_name, id.item_unit, id.item_quantity, iosp.item_cost, id.item_img_flag, id.item_img_add,
              id.item_short_desc, id.item_desc, iosp.item_status, id.sc_code, id.item_cost, iosp.sp_bfr_offer_price, iosp.b_in_offer,
              iosp.offer_special_price, iosp.in_cart, iosp.max_special_quantity, iosp.min_special_amount, iosp.in_offer
			FROM items_offered_by_service_provider iosp, item_details id
			WHERE iosp.sp_code = ? AND id.item_code = iosp.item_code AND iosp.item_status = 1 ";
            
            $search_condition = " and LOWER(iosp.item_name) like LOWER('%$search_string%') ";
            
            if(intval($previous_item_code) == 0)
					$item_condition = "";
				else 
					$item_condition = " and CAST(iosp.item_code as UNSIGNED) > $previous_item_code ";
			$sort_condition = " order by CAST(iosp.item_code as UNSIGNED) ";
			$limit_condition = " limit $item_count_to_send ";
				
			$final_query = $query.$search_condition.$item_condition.$sort_condition.$limit_condition;
				
            $stmt = $this->conn->prepare($final_query);			
            
            $stmt->bind_param("s", $serv_prov_code);
            $stmt->execute();
            $stmt->bind_result($id, $itemcode, $spcode, $itemtypecode, $itemname, $itemunit, $itemquant, $itemcost, $itemimgflag, $itemimgadd
                , $itemshortdesc, $itemdesc, $itemstatus, $itemservcode, $itemmrp, $itemofferprice, $iteminoffer,
                $offerspecialprice, $incart, $maxspecialqty, $mimspecialamount, $inoffer);

            $response = array();
            $response["error"] = 1;
            $response["items_service_providers"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["item_code"] = $itemcode;
                $tmp["sp_code"] = $spcode;
                $tmp["item_type_code"] = $itemtypecode;
                $tmp["item_name"] = $itemname;
                $tmp["item_unit"] = $itemunit;
                $tmp["item_quantity"] = $itemquant;
                $tmp["item_cost"] = $itemcost;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_desc"] = $itemquant." ".$itemunit;
                $tmp["item_status"] = $itemstatus;
                $tmp["item_selected"] = 0;
                $tmp["item_quantity_selected"] = 0;
                $tmp["item_service_type"] = $serv_type_code;
                $tmp["item_service_category"] = $serv_category_code;
                $tmp["item_mrp"] = $itemmrp;
                if($itemofferprice == 0)
                    $tmp["item_offer_price"] = $itemcost;
                else
                    $tmp["item_offer_price"] = $itemofferprice;
                $tmp["item_in_offer"] = $iteminoffer;
                $tmp["offer_special_price"] = $offerspecialprice;
                $tmp["in_cart"] = $incart;
                $tmp["max_special_qty"] = $maxspecialqty;
                $tmp["min_special_amount"] = $mimspecialamount;
                $tmp["in_special_offer"] = $inoffer;
                array_push($response["items_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    

    public function getItemsServiceProviderCategoryWiseAll($serv_prov_code, $serv_type_code){
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt = $this->conn->prepare("SELECT * FROM items_offered_by_service_provider where sp_code=?");
            
            $stmt->bind_param("s", $serv_prov_code);
            $stmt->execute();
            $stmt->bind_result($id, $itemcode, $spcode, $itemtypecode, $itemname, $itemunit, $itemquant, $itemcost, $itemimgflag, $itemimgadd
                , $itemshortdesc, $itemdesc, $itemstatus, $itemservcode);

            $response = array();
            $response["error"] = 1;
            $response["items_service_providers"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["item_code"] = $itemcode;
                $tmp["sp_code"] = $spcode;
                $tmp["item_type_code"] = $itemtypecode;
                $tmp["item_name"] = $itemname;
                $tmp["item_unit"] = $itemunit;
                $tmp["item_quantity"] = $itemquant;
                $tmp["item_cost"] = $itemcost;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_desc"] = $itemdesc;
                $tmp["item_status"] = $itemstatus;
                $tmp["item_selected"] = 0;
                $tmp["item_quantity_selected"] = 0;
                $tmp["item_service_type"] = $serv_type_code;
                $tmp["item_service_category"] = $itemservcode;
                array_push($response["items_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }
    
    public function getItemsServiceProviderCategoryWiseBatchWise($serv_prov_code, $serv_type_code, $serv_category_code, $number_of_items){
        if($this->isServiceProviderValid($serv_prov_code)){
        	if(strcmp($number_of_items, "few")){
        		$stmt = $this->conn->prepare("SELECT * FROM items_offered_by_service_provider where sp_code=? and sc_code=? limit 20");
        	}else{
        		$stmt = $this->conn->prepare("SELECT * FROM items_offered_by_service_provider where sp_code=? and sc_code=? LIMIT 18446744073709551615 offset 20");
        	}
            
            $stmt->bind_param("ss", $serv_prov_code, $serv_category_code);
            $stmt->execute();
            $stmt->bind_result($id, $itemcode, $spcode, $itemtypecode, $itemname, $itemunit, $itemquant, $itemcost, $itemimgflag, $itemimgadd
                , $itemshortdesc, $itemdesc, $itemstatus, $itemservcode);

            $response = array();
            $response["error"] = 1;
            $response["items_service_providers"] = array();

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["id"] = $id;
                $tmp["item_code"] = $itemcode;
                $tmp["sp_code"] = $spcode;
                $tmp["item_type_code"] = $itemtypecode;
                $tmp["item_name"] = $itemname;
                $tmp["item_unit"] = $itemunit;
                $tmp["item_quantity"] = $itemquant;
                $tmp["item_cost"] = $itemcost;
                $tmp["item_img_flag"] = $itemimgflag;
                $tmp["item_img_address"] = $itemimgadd;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_desc"] = $itemdesc;
                $tmp["item_status"] = $itemstatus;
                array_push($response["items_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
    }

    public function getNonAvaliableItemsSrvProv($serv_prov_code){
        $response = array();
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt = "select * from service_providers where sp_code='".$serv_prov_code."'";
            $result=mysqli_query($this->conn, $stmt);
            if(mysqli_num_rows($result)==1){
                $row_serv_type = mysqli_fetch_array($result);
                $st_code=$row_serv_type["sp_service_type_code"];
                //TODO service type if non existent

                $stmt_items="select * from item_details where st_code='".$st_code."' and item_code not in (select item_code from items_offered_by_service_provider where sp_code='".$serv_prov_code."')";
                $result_items=mysqli_query($this->conn, $stmt_items);
                if($result_items){
                    $response["error"] = 1;
                    $response["items_not_avaliable"] = array();
                    while ($row = mysqli_fetch_array($result_items)){
                        $product = array();
                        $product["item_code"] = $row["item_code"];
                        $product["item_name"] = $row["item_name"];
                        $product["item_unit"] = $row["item_unit"];
                        $product["item_type_code"] = $row["item_type_code"];
                        $product["item_quantity"] = $row["item_quantity"];
                        $product["item_cost"] = $row["item_cost"];
                        $product["item_img_flag"] = $row["item_img_flag"];
                        $product["item_img_add"] = $row["item_img_add"];
                        $product["item_short_desc"] = $row["item_short_desc"];
//                         $product["item_desc"] = $row["item_quantity"]." ".$row["item_unit"];
						$product["item_desc"] = $row["item_desc"];
                        array_push($response["items_not_avaliable"], $product);
                    }
                    $response["error"] = 1;
                }else{
                    $response["error"] = 0;
                    $response["message"]="Items Not Avaliable";
                }
            }else{
                $response["error"] = 0;
                $response["message"]="Service Provider Not Valid";
            }

        }else{
            $response["error"] = 0;
            $response["message"]="Service Provider Not Available";
        }
        return $response;
    }
    
    public function getNonAvaliableItemsSrvProvGSON($serv_prov_code, $serv_prov_cat_type){
        $response = array();
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt = "select * from service_providers where sp_code='".$serv_prov_code."'";
            $result=mysqli_query($this->conn, $stmt);
            if(mysqli_num_rows($result)==1){
                $row_serv_type = mysqli_fetch_array($result);
                $st_code=$row_serv_type["sp_service_type_code"];
                $response["serv_type"]=$st_code;
                //TODO service type if non existent

                $stmt = "select * from service_type_categories where st_code='".$st_code."' and sc_code='".$serv_prov_cat_type."'";
                $result=mysqli_query($this->conn, $stmt);
                if(mysqli_num_rows($result)==1){
                    $row_serv_type = mysqli_fetch_array($result);
                    $sc_name=$row_serv_type["sc_name"];

                    $stmt_items="select * from item_details where st_code='".$st_code."' and item_desc='".$sc_name."' 
                    and item_code not in (select item_code from items_offered_by_service_provider 
                    where sp_code='".$serv_prov_code."') order by item_code ";
                    $result_items=mysqli_query($this->conn, $stmt_items);
                    if($result_items){
                        $response["error"] = 1;
                        $response["items_service_providers"] = array();
                        $i=0;
                        while ($row = mysqli_fetch_array($result_items)){
                            $product = array();
                            $product["item_id"]=$i+1;
                            $product["item_code"] = $row["item_code"];
                            $product["item_name"] = $row["item_name"];
                            $product["item_unit"] = $row["item_unit"];
                            $product["item_type_code"] = $row["item_type_code"];
                            $product["item_quantity"] = $row["item_quantity"];
                            $product["item_cost"] = $row["item_cost"];
                            $product["item_img_flag"] = $row["item_img_flag"];
                            $product["item_img_add"] = $row["item_img_add"];
                            $product["item_short_desc"] = $row["item_short_desc"];
//                             $product["item_desc"] = $row["item_quantity"]." ".$row["item_unit"];
							$product["item_desc"] = $row["item_desc"];
                            $product["item_status"]=0;
                            $i=$i+1;
                            array_push($response["items_service_providers"], $product);
                        }
                        $response["error"] = 1;
                    }else{
                        $response["error"] = 0;
                        $response["message"]="Items Not Avaliable";
                    }
                }
            }else{
                $response["error"] = 0;
                $response["message"]="Service Provider Not Valid";
            }

        }else{
            $response["error"] = 0;
            $response["message"]="Service Provider Not Available";
        }
        return $response;
    }
    
    public function getNonAvaliableItemsSrvProvGSONLimited($serv_prov_code, $serv_prov_cat_type, $previous_item_code){
    	$item_count_to_send = 10;
    
        $response = array();
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt = "select * from service_providers where sp_code='".$serv_prov_code."'";
            $result=mysqli_query($this->conn, $stmt);
            if(mysqli_num_rows($result)==1){
                $row_serv_type = mysqli_fetch_array($result);
                $st_code=$row_serv_type["sp_service_type_code"];
                $response["serv_type"]=$st_code;
                //TODO service type if non existent

                $stmt = "select * from service_type_categories where st_code='".$st_code."' and sc_code='".$serv_prov_cat_type."'";
                $result=mysqli_query($this->conn, $stmt);
                if(mysqli_num_rows($result)==1){
                    $row_serv_type = mysqli_fetch_array($result);
                    $sc_name=$row_serv_type["sc_name"];
		    
                    $query="select * from item_details where st_code='".$st_code."' and item_desc='".$sc_name."' 
                    and item_code not in (select item_code from items_offered_by_service_provider 
                    where sp_code='".$serv_prov_code."') ";
                    
                    if(null == $previous_item_code || intval($previous_item_code) == 0)
						$item_condition = "";
					else 
						$item_condition = " and CAST(item_code as UNSIGNED) > $previous_item_code ";
					$sort_condition = " order by CAST(item_code as UNSIGNED) ";
					$limit_condition = " limit $item_count_to_send ";
			
					$stmt_items = $query.$item_condition.$sort_condition.$limit_condition;
                    
                    $result_items=mysqli_query($this->conn, $stmt_items);
                    if($result_items){
                        $response["error"] = 1;
                        $response["items_service_providers"] = array();
                        $i=0;
                        while ($row = mysqli_fetch_array($result_items)){
                            $product = array();
                            $product["item_id"]=$i+1;
                            $product["item_code"] = $row["item_code"];
                            $product["item_name"] = $row["item_name"];
                            $product["item_unit"] = $row["item_unit"];
                            $product["item_type_code"] = $row["item_type_code"];
                            $product["item_quantity"] = $row["item_quantity"];
                            $product["item_cost"] = $row["item_cost"];
                            $product["item_img_flag"] = $row["item_img_flag"];
                            $product["item_img_add"] = $row["item_img_add"];
                            $product["item_short_desc"] = $row["item_short_desc"];
                            $product["item_desc"] = $row["item_quantity"]." ".$row["item_unit"];
//							$product["item_desc"] = $row["item_desc"];
                            $product["item_status"]=0;
                            $i=$i+1;
                            array_push($response["items_service_providers"], $product);
                        }
                        $response["error"] = 1;
                    }else{
                        $response["error"] = 0;
                        $response["message"]="Items Not Avaliable";
                    }
                }
            }else{
                $response["error"] = 0;
                $response["message"]="Service Provider Not Valid";
            }

        }else{
            $response["error"] = 0;
            $response["message"]="Service Provider Not Available";
        }
        return $response;
    }

    public function getAvaliableItemsSrvProv($serv_prov_code){
        $response = array();
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt_items="select * from items_offered_by_service_provider where sp_code='".$serv_prov_code."' and item_status=1";
            $result_items=mysqli_query($this->conn, $stmt_items);
            if($result_items){
                $response["items_avaliable"] = array();
                while ($row = mysqli_fetch_array($result_items)){
                    $product = array();
                    $product["item_id"] = $row["id"];
                    $product["item_code"] = $row["item_code"];
                    $product["item_name"] = $row["item_name"];
                    $product["item_unit"] = $row["item_unit"];
                    $product["item_type_code"] = $row["item_type_code"];
                    $product["item_quantity"] = $row["item_quantity"];
                    $product["item_cost"] = $row["item_cost"];
                    $product["item_img_flag"] = $row["item_img_flag"];
                    $product["item_img_add"] = $row["item_img_add"];
                    $product["item_short_desc"] = $row["item_short_desc"];
                    $product["item_desc"] = $row["item_desc"];
                    $product["item_status"] = $row["item_status"];
                    $product["sc_code"] = $row["sc_code"];
                    array_push($response["items_avaliable"], $product);
                }
                $response["error"] = 1;
            }else{
                $response["error"] = 0;
                $response["message"]="Items Not Avaliable";
            }
        }else{
            $response["error"] = 0;
            $response["message"]="Service Provider Not Available";
        }
        return $response;
    }
    
    public function getAvaliableItemsSrvProvGSON($serv_prov_code, $serv_prov_cat_type){
        $response = array();
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt_items="select iobsp.id,iobsp.item_code, id.item_name, id.item_unit, id.item_type_code ,
             id.item_quantity, iobsp.item_cost, id.item_img_flag, id.item_img_add, id.item_short_desc, id.item_desc 
             from items_offered_by_service_provider iobsp, item_details id where iobsp.sp_code='".$serv_prov_code."' 
             and id.item_code = iobsp.item_code and id.sc_code='".$serv_prov_cat_type."' and item_status=1 order 
             by iobsp.item_code ";
            $result_items=mysqli_query($this->conn, $stmt_items);
            if($result_items){
                $response["items_service_providers"] = array();
                while ($row = mysqli_fetch_array($result_items)){
                    $product = array();
                    $product["item_id"] = $row["id"];
                    $product["item_code"] = $row["item_code"];
                    $product["item_name"] = $row["item_name"];
                    $product["item_unit"] = $row["item_unit"];
                    $product["item_type_code"] = $row["item_type_code"];
                    $product["item_quantity"] = $row["item_quantity"];
                    $product["item_cost"] = $row["item_cost"];
                    $product["item_img_flag"] = $row["item_img_flag"];
                    $product["item_img_add"] = $row["item_img_add"];
                    $product["item_short_desc"] = $row["item_short_desc"];
                    $product["item_desc"] = $row["item_desc"];
                    $product["item_status"] = 0;
                    array_push($response["items_service_providers"], $product);
                }
                $response["error"] = 1;
            }else{
                $response["error"] = 0;
                $response["message"]="Items Not Avaliable";
            }
        }else{
            $response["error"] = 0;
            $response["message"]="Service Provider Not Available";
        }
        return $response;
    }
    
    public function getAvaliableItemsSrvProvGSONLimited($serv_prov_code, $serv_prov_cat_type, $previous_item_code){
    	$item_count_to_send = 10;
    
        $response = array();
        if($this->isServiceProviderValid($serv_prov_code)){
            $query="select iobsp.id, iobsp.item_code , id.item_name , id.item_unit , id.item_type_code , 
            id.item_quantity , iobsp.item_cost , id.item_img_flag , id.item_img_add , id.item_short_desc , 
            id.item_desc  from items_offered_by_service_provider iobsp, item_details id where iobsp.sp_code='".$serv_prov_code."' and
            id.sc_code='".$serv_prov_cat_type."' and id.item_code = iobsp.item_code and item_status=1 ";
            
			if(null == $previous_item_code || intval($previous_item_code) == 0)
				$item_condition = "";
			else 
				$item_condition = " and CAST(iobsp.item_code as UNSIGNED) > $previous_item_code ";
			$sort_condition = " order by CAST(iobsp.item_code as UNSIGNED) ";
			$limit_condition = " limit $item_count_to_send ";
			
			$stmt_items = $query.$item_condition.$sort_condition.$limit_condition;
            
            $result_items=mysqli_query($this->conn, $stmt_items);
            if($result_items){
                $response["items_service_providers"] = array();
                while ($row = mysqli_fetch_array($result_items)){
                    $product = array();
                    $product["item_id"] = $row["id"];
                    $product["item_code"] = $row["item_code"];
                    $product["item_name"] = $row["item_name"];
                    $product["item_unit"] = $row["item_unit"];
                    $product["item_type_code"] = $row["item_type_code"];
                    $product["item_quantity"] = $row["item_quantity"];
                    $product["item_cost"] = $row["item_cost"];
                    $product["item_img_flag"] = $row["item_img_flag"];
                    $product["item_img_add"] = $row["item_img_add"];
                    $product["item_short_desc"] = $row["item_short_desc"];
                    $product["item_desc"] = $row["item_quantity"]." ".$row["item_unit"];
                    $product["item_status"] = 0;
                    array_push($response["items_service_providers"], $product);
                }
                $response["error"] = 1;
            }else{
                $response["error"] = 0;
                $response["message"]="Items Not Avaliable";
            }
        }else{
            $response["error"] = 0;
            $response["message"]="Service Provider Not Available";
        }
        return $response;
    }
    
	public function getOutofStockItemsSrvProvGSON($serv_prov_code, $serv_prov_cat_type){
        $response = array();
        if($this->isServiceProviderValid($serv_prov_code)){
            $stmt_items="select iobsp.id , iobsp.item_code , id.item_name , id.item_unit , id.item_type_code , id.item_quantity , 
            iobsp.item_cost , id.item_img_flag , id.item_img_add , id.item_short_desc, id.item_desc from items_offered_by_service_provider iobsp, 
            item_details id where iobsp.sp_code='".$serv_prov_code."' and id.item_code=iobsp.item_code and id.sc_code='".$serv_prov_cat_type."' and iobsp.item_status=3 order by iobsp.item_code ";
            $result_items=mysqli_query($this->conn, $stmt_items);
            if($result_items){
                $response["items_service_providers"] = array();
                while ($row = mysqli_fetch_array($result_items)){
                    $product = array();
                    $product["item_id"] = $row["id"];
                    $product["item_code"] = $row["item_code"];
                    $product["item_name"] = $row["item_name"];
                    $product["item_unit"] = $row["item_unit"];
                    $product["item_type_code"] = $row["item_type_code"];
                    $product["item_quantity"] = $row["item_quantity"];
                    $product["item_cost"] = $row["item_cost"];
                    $product["item_img_flag"] = $row["item_img_flag"];
                    $product["item_img_add"] = $row["item_img_add"];
                    $product["item_short_desc"] = $row["item_short_desc"];
                    $product["item_desc"] = $row["item_desc"];
                    $product["item_status"] = 0;
                    array_push($response["items_service_providers"], $product);
                }
                $response["error"] = 1;
            }else{
                $response["error"] = 0;
                $response["message"]="Items Not Avaliable";
            }
        }else{
            $response["error"] = 0;
            $response["message"]="Service Provider Not Available";
        }
        return $response;
    }
    
    public function getOutofStockItemsSrvProvGSONLimited($serv_prov_code, $serv_prov_cat_type, $previous_item_code){
        $item_count_to_send = 10;
        
        $response = array();
        if($this->isServiceProviderValid($serv_prov_code)){
            $query="select iobsp.id , iobsp.item_code , id.item_name , id.item_unit , id.item_type_code , id.item_quantity , 
            iobsp.item_cost , id.item_img_flag , id.item_img_add , id.item_short_desc,  id.item_desc from items_offered_by_service_provider iobsp, item_details id where iobsp.sp_code='".$serv_prov_code."' and
            id.sc_code='".$serv_prov_cat_type."' and id.item_code = iobsp.item_code and item_status=3 ";
            
			if(null == $previous_item_code || intval($previous_item_code) == 0)
				$item_condition = "";
			else 
				$item_condition = " and CAST(iobsp.item_code as UNSIGNED) > $previous_item_code ";
			$sort_condition = " order by CAST(iobsp.item_code as UNSIGNED) ";
			$limit_condition = " limit $item_count_to_send ";
			
			$stmt_items = $query.$item_condition.$sort_condition.$limit_condition;
            
            $result_items=mysqli_query($this->conn, $stmt_items);
            if($result_items){
                $response["items_service_providers"] = array();
                while ($row = mysqli_fetch_array($result_items)){
                    $product = array();
                    $product["item_id"] = $row["id"];
                    $product["item_code"] = $row["item_code"];
                    $product["item_name"] = $row["item_name"];
                    $product["item_unit"] = $row["item_unit"];
                    $product["item_type_code"] = $row["item_type_code"];
                    $product["item_quantity"] = $row["item_quantity"];
                    $product["item_cost"] = $row["item_cost"];
                    $product["item_img_flag"] = $row["item_img_flag"];
                    $product["item_img_add"] = $row["item_img_add"];
                    $product["item_short_desc"] = $row["item_short_desc"];
                    $product["item_desc"] = $row["item_quantity"].' '.$row["item_unit"];
                    $product["item_status"] = 0;
                    array_push($response["items_service_providers"], $product);
                }
                $response["error"] = 1;
            }else{
                $response["error"] = 0;
                $response["message"]="Items Not Avaliable";
            }
        }else{
            $response["error"] = 0;
            $response["message"]="Service Provider Not Available";
        }
        return $response;
    }
    
    public function searchItemsSrvProvLimited($sp_code, $sc_code, $item_status,$search_string,$previous_item_code){
    	
    	$item_count_to_send = 10;
    	$item_status = intval($item_status);
    	
    	$response = array();
        if($this->isServiceProviderValid($sp_code)){
        switch($item_status){
        	case 1:
        	case 3:
        
				$query="select iobsp.id, iobsp.item_code, id.item_name, id.item_unit, id.item_type_code, id.item_quantity,
                  iobsp.item_cost, id.item_img_flag, id.item_img_add, id.item_short_desc, id.item_desc, iobsp.item_status,
                   id.sc_code   from items_offered_by_service_provider iobsp, item_details id where
                    id.item_code=iobsp.item_code and iobsp.sp_code='".$sp_code."' and iobsp.item_status=$item_status ";
			
				$search_condition = " and LOWER(id.item_name) like LOWER('%$search_string%') ";
			
				if(null == $previous_item_code || intval($previous_item_code) == 0)
						$item_condition = "";
					else 
						$item_condition = " and CAST(iobsp.item_code as UNSIGNED) > $previous_item_code ";
				$sort_condition = " order by CAST(iobsp.item_code as UNSIGNED) ";
				$limit_condition = " limit $item_count_to_send ";
		   
			
				$stmt_items = $query.$search_condition.$item_condition.$sort_condition.$limit_condition;
			
				$result_items=mysqli_query($this->conn, $stmt_items);
				if($result_items){
					$response["error"] = 1;
					$response["items_service_providers"] = array();
					while ($row = mysqli_fetch_array($result_items)){
						$product = array();
						$product["item_id"] = $row["id"];
						$product["item_code"] = $row["item_code"];
						$product["item_name"] = $row["item_name"];
						$product["item_unit"] = $row["item_unit"];
						$product["item_type_code"] = $row["item_type_code"];
						$product["item_quantity"] = $row["item_quantity"];
						$product["item_cost"] = $row["item_cost"];
						$product["item_img_flag"] = $row["item_img_flag"];
						$product["item_img_add"] = $row["item_img_add"];
						$product["item_short_desc"] = $row["item_short_desc"];
						$product["item_desc"] = $row["item_desc"];
						$product["item_status"] = 0;
						$product["item_sc_code"] = $row["sc_code"];
						array_push($response["items_service_providers"], $product);
					}
				
				}else{
					$response["error"] = 0;
					$response["message"]="Items Not Avaliable";
				}
				break;
            
        	case 2:
				$stmt = "select * from service_providers where sp_code='".$sp_code."'";
				$result=mysqli_query($this->conn, $stmt);
				if(mysqli_num_rows($result)==1){
					$row_serv_type = mysqli_fetch_array($result);
					$st_code=$row_serv_type["sp_service_type_code"];
					$response["serv_type"]=$st_code;
					//TODO service type if non existent
				
					$query="select * from item_details where st_code='".$st_code."' 
					and item_code not in (select item_code from items_offered_by_service_provider 
					where sp_code='".$sp_code."') ";
				
					$search_condition = " and LOWER(item_name) like LOWER('%$search_string%') ";
			
					if(null == $previous_item_code || intval($previous_item_code) == 0)
						$item_condition = "";
					else 
						$item_condition = " and CAST(item_code as UNSIGNED) > $previous_item_code ";
					$sort_condition = " order by CAST(item_code as UNSIGNED) ";
					$limit_condition = " limit $item_count_to_send ";
		   
					$stmt_items = $query.$search_condition.$item_condition.$sort_condition.$limit_condition;
				
					$result_items=mysqli_query($this->conn, $stmt_items);
					if($result_items){
						$response["error"] = 1;
						$response["items_service_providers"] = array();
						$i=0;
						while ($row = mysqli_fetch_array($result_items)){
							$product = array();
							$product["item_id"]=$i+1;
							$product["item_code"] = $row["item_code"];
							$product["item_name"] = $row["item_name"];
							$product["item_unit"] = $row["item_unit"];
							$product["item_type_code"] = $row["item_type_code"];
							$product["item_quantity"] = $row["item_quantity"];
							$product["item_cost"] = $row["item_cost"];
							$product["item_img_flag"] = $row["item_img_flag"];
							$product["item_img_add"] = $row["item_img_add"];
							$product["item_short_desc"] = $row["item_short_desc"];
							$product["item_desc"] = $row["item_desc"];
							$product["item_status"]=0;
							$i=$i+1;
							array_push($response["items_service_providers"], $product);
						}

					}else{
						$response["error"] = 0;
						$response["message"]="Items Not Avaliable";
					}
				
				}else{
					$response["error"] = 0;
					$response["message"]="Service Provider Not Valid";
				}
			
				break;
			
				default : 
					break;
        	}
        }else{
            $response["error"] = 0;
            $response["message"]="Service Provider Not Available";
        }
        return $response;
    }
    
    
    public function getUserPreviousOrders($user_code){
        $response = array();
        $response["error"] = 1;
        $response["previous_orders_details"] = array();
        $stmt = $this->conn->prepare("SELECT os.order_string, obd.order_time, cod.post_total_amount, 
        oin.status, sp.sp_name
        FROM order_in_progress oin, order_basic_details obd, confirmed_order_details cod,
        service_providers sp, order_strings os
        WHERE os.o_id = obd.id
        AND obd.user_code = ?
        AND obd.order_code = oin.order_code
        AND sp.sp_code = oin.sp_code
        AND cod.order_code = oin.order_code");
        $stmt->bind_param("s", $user_code);

        $stmt->execute();

        $stmt->bind_result($ordercode, $orderdate, $ordertotamount, $orderstatus, $spname);



        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["order_code"] = $ordercode;
            $tmp["order_date"] = $orderdate;
            $tmp["order_amount"] = $ordertotamount;
            $tmp["order_status"] = $orderstatus;
            $tmp["sp_name"] = $spname;
            array_push($response["previous_orders_details"], $tmp);
        }
        return $response;
    }
    
    public function getUserPreviousOrdersLimited($user_code, $previous_order_string){
		$previous_order_code = $this->getOrderCodeFromOrderString($previous_order_string);
		
		$order_count_to_send = 10;
		
        $response = array();
        $response["error"] = 1;
        $response["previous_orders_details"] = array();
        
        $query = "SELECT os.order_string, obd.order_time, cod.post_total_amount, oin.status, sp.sp_name
        FROM user_details ud, order_in_progress oin, order_basic_details obd, confirmed_order_details cod,
        service_providers sp, order_strings os
        WHERE os.o_id = obd.id
        AND obd.user_code = ud.user_code
        AND (
        ud.user_code = ?
        OR ud.phone_number = ( 
        SELECT phone_number
        FROM user_details
        WHERE user_code = ? )
        )
        AND obd.order_code = oin.order_code
        AND sp.sp_code = oin.sp_code 
        AND cod.order_code=oin.order_code" ;

		if(intval($previous_order_code) == 0)
			$order_condition = "";
		else 
			$order_condition = " and CAST(obd.order_code as UNSIGNED) < $previous_order_code ";
		$sort_condition = " order by CAST(obd.order_code as UNSIGNED) desc ";
		$limit_condition = " limit $order_count_to_send ";
		
		$final_query = $query.$order_condition.$sort_condition.$limit_condition;
             
        $stmt = $this->conn->prepare($final_query);
        $stmt->bind_param("ss", $user_code, $user_code);
        
        $result = $stmt->execute();
		if($result){
			$stmt->bind_result($ordercode, $orderdate, $ordertotamount, $orderstatus, $spname);

			// looping through result and preparing tasks array
			while ($stmt->fetch()) {
				$tmp = array();

				$tmp["order_code"] = $ordercode;
				$tmp["order_date"] = $orderdate;
				$tmp["order_amount"] = $ordertotamount;
				$tmp["order_status"] = $orderstatus;
				$tmp["sp_name"] = $spname;
				array_push($response["previous_orders_details"], $tmp);
			}
        }
        else
        	$response["error"] = 0;
        return $response;
    }

    public function postOrderDetailsToServer($orderDataArray, $userCode, $userAddress, $totalAmount, $expectedDeliveryDate, $spCode, $stCode){
        $stmt = "SELECT id, order_code FROM order_details ORDER BY id DESC LIMIT 1";
        $result=mysqli_query($this->conn, $stmt);
        $response = array();
        $response["error"] = 1;

        if(mysqli_num_rows($result)==0){
            $order_code='1';
        }
        else{
            $row_order_id_present = mysqli_fetch_array($result);
            $order_code=$row_order_id_present["order_code"];
            $order_code=intval($order_code)+1;
        }
        
	    $date = date("D M d, Y G:i");
		$stmt_order_details="insert into order_details(order_code, user_code, expected_del_time,
		del_address_code, del_address_user_code, order_date, order_tot_amount, order_orig_amount) values('".$order_code."', '".$userCode."', NOW(), '".$userAddress."',
		'".$userCode."', '".$date."', $totalAmount, $totalAmount)";
		$result_order_details=mysqli_query($this->conn, $stmt_order_details);
		foreach($orderDataArray as $obj){
			$itemcode=$obj['code'];
			$itemquantityselected=$obj['itemQuantitySelected'];
			
			$query = "insert into order_item_details(item_code, item_quantity, user_code, order_code, item_cost) 
				select '".$itemcode."', '".$itemquantityselected."', '".$userCode."', '".$order_code."', item_cost 
                from items_offered_by_service_provider where sp_code = '".$spCode."' and item_code = '".$itemcode."' limit 1";
			
			$stmt_insert = $query;
			$result_insert=mysqli_query($this->conn, $stmt_insert);
		}
		$stmt_order_in_progress="insert into order_in_progress(order_code, sp_code, status, max_no_of_items, user_code) values (
			'".$order_code."', '".$spCode."', 'Pending', 0, '".$userCode."')";
		$result_in_progress=mysqli_query($this->conn, $stmt_order_in_progress);

		$this->pushServiceProviderOrderNotification($spCode);

		$order_string = $this->getOrderStringFromOrderCode($order_code);
		$response["order_code"] = $order_string;
        
        return $response;
    }
    
    public function postOrderDetailsToServerFinal($orderDataArray, $userCode, $userAddress, $totalAmount, $expectedDeliveryDate, $spCode, $stCode, $delivery_amount, $discount_amount, $total_items_amount){
        $stmt = "SELECT id, order_code FROM order_details ORDER BY id DESC LIMIT 1";
        $result=mysqli_query($this->conn, $stmt);
        $response = array();
        $response["error"] = 1;

        if(mysqli_num_rows($result)==0){
            $order_code='1';
        }
        else{
            $row_order_id_present = mysqli_fetch_array($result);
            $order_code=$row_order_id_present["order_code"];
            $order_code=intval($order_code)+1;
        }
            
	    $date = date("D M d, Y G:i");
		$stmt_order_details="insert into order_details(order_code, user_code, expected_del_time,
		del_address_code, del_address_user_code, order_date, order_tot_amount, order_total_cost, order_delivery_cost, order_discount_cost, order_orig_amount )values('".$order_code."', '".$userCode."', NOW(), '".$userAddress."',
		'".$userCode."', '".$date."', $totalAmount, $total_items_amount, $delivery_amount, $discount_amount, $total_items_amount)";
		$result_order_details=mysqli_query($this->conn, $stmt_order_details);
		foreach($orderDataArray as $obj){
			$itemcode=$obj['code'];
			$itemquantityselected=$obj['itemQuantitySelected'];
			
			$query = "insert into order_item_details(item_code, item_quantity, user_code, order_code, item_cost) 
				select '".$itemcode."', '".$itemquantityselected."', '".$userCode."', '".$order_code."', item_cost 
                from items_offered_by_service_provider where sp_code = '".$spCode."' and item_code = '".$itemcode."' limit 1";
			
			$stmt_insert = $query;
			$result_insert=mysqli_query($this->conn, $stmt_insert);
		}
		$stmt_order_in_progress="insert into order_in_progress(order_code, sp_code, status, max_no_of_items, user_code) values (
			'".$order_code."', '".$spCode."', 'Pending', 0, '".$userCode."')";
		$result_in_progress=mysqli_query($this->conn, $stmt_order_in_progress);

		$this->pushServiceProviderOrderNotification($spCode);

// 		$response["order_code"] = $order_code;
		$response["order_code"] = 1; // 1 sent to hide the order code on iphone which requires it as an int

        return $response;
    }
    
    public function postOrderDetailsToServerFinal_v2($orderDataArray, $userCode, $userAddress, $totalAmount, $expectedDeliveryDate, $spCode, $stCode, $delivery_amount, $discount_amount, $total_items_amount){
        $stmt = "SELECT id, order_code FROM order_details ORDER BY id DESC LIMIT 1";
        $result=mysqli_query($this->conn, $stmt);
        $response = array();
        $response["error"] = 1;

        if(mysqli_num_rows($result)==0){
            $order_code='1';
        }
        else{
            $row_order_id_present = mysqli_fetch_array($result);
            $order_code=$row_order_id_present["order_code"];
            $order_code=intval($order_code)+1;
        }
            
	    $date = date("D M d, Y G:i");
		$stmt_order_details="insert into order_details(order_code, user_code, expected_del_time,
		del_address_code, del_address_user_code, order_date, order_tot_amount, order_total_cost, order_delivery_cost,
		 order_discount_cost,order_orig_amount)values('".$order_code."', '".$userCode."', FROM_UNIXTIME(".$expectedDeliveryDate."),
		  '".$userAddress."', '".$userCode."', '".$date."', $totalAmount, $total_items_amount, $delivery_amount, $discount_amount
		  , $total_items_amount)";
		  
		$result_order_details=mysqli_query($this->conn, $stmt_order_details);
		foreach($orderDataArray as $obj){
			$itemcode=$obj['code'];
			$itemquantityselected=$obj['itemQuantitySelected'];
						
			$query = "insert into order_item_details(item_code, item_quantity, user_code, order_code, item_cost) 
				select '".$itemcode."', '".$itemquantityselected."', '".$userCode."', '".$order_code."', item_cost 
                from items_offered_by_service_provider where sp_code = '".$spCode."' and item_code = '".$itemcode."' limit 1";
			
			$stmt_insert = $query;
			$result_insert=mysqli_query($this->conn, $stmt_insert);
		}
		$stmt_order_in_progress="insert into order_in_progress(order_code, sp_code, status, max_no_of_items, user_code) values (
			'".$order_code."', '".$spCode."', 'Pending', 0, '".$userCode."')";
		$result_in_progress=mysqli_query($this->conn, $stmt_order_in_progress);

		$this->pushServiceProviderOrderNotification($spCode);

		$order_string = $this->getOrderStringFromOrderCode($order_code);
		$response["order_code"] = $order_string;

        return $response;
    }
    
    public function handleOrderSuccessfulPlaced($order_code, $user_code, $amount, $spCode){
    	//Handle All Things For Order Placed Successful

    	//points to profile for order
//    	if(-1 != $this->CreateorGetProfileId($user_code)){
//			$query = "select REPLACE(  sp_delivery_time ,  ' mins',  '' ) as sp_delivery_time from service_providers
//				where sp_code = $spCode ";
//			$result = mysqli_query($this->conn, $query);
//			$row = mysqli_fetch_array($result);
//			$delivery_time = $row['sp_delivery_time'];
//
//			$points_to_credit = $total_items_amount * 3/4 + $delivery_time* 1/2;
//			$query = "update user_profile up, user_details ud set lazy_points_balance = lazy_points_balance + $points_to_credit
//				where ud.user_code = $user_code and up.phone_number = ud.phone_number ";
//			mysqli_query($this->conn, $query);
//    	}

		$city_code = 0;
		$query = " select uad.user_city as city_name from user_address_details uad, order_details od where 
		uad.user_code = od.del_address_user_code  and uad.user_address_code = od.del_address_code and 
		od.order_code = $order_code ";

		$result = mysqli_query($this->conn, $query);
		if($result && 0 != mysqli_num_rows($result)){
			$row = mysqli_fetch_array($result);
			$city_name = $row['city_name'];
			$city_details = $this->getCityDetails($city_name);
			if($city_details){
				$city_code = $city_details['city_code'];
			}
		}
		
		$logistic_order_sent = false;
//		if(0 != $city_code){
//			$query = " update order_details od join service_providers sp
//			on od.order_code = $order_code and sp.sp_code = $spCode
//			set od.logistics_services_used = sp.logistics_services ";
//			$result = mysqli_query($this->conn, $query);
//			if(1 || $result && 0 != mysqli_affected_rows($this->conn) ){
//
//				$api_params = array(
//					"order_code" => $this->getOrderStringFromOrderCode($order_code),
//					"city_code" => $city_code,
//				);
//
//				$net_api = $this->getNetworkApi();
//
//				$response = $net_api->logisticsCreateOrder($api_params);
//				if(null == $response || false == $response['success']) {
//					$logistic_order_sent = false;
//				}
//				else {
//					$logistic_order_sent = true;
//				}
//			}
//		}
        $logs_flag = 0;
        $logistic_query = "select logistics_services from service_providers where sp_code = $spCode";
        $result = mysqli_query($this->conn, $logistic_query);
        if($result && 0 != mysqli_num_rows($result)) {
            $row = mysqli_fetch_array($result);
            $logs_flag = $row['logistics_services'];
        }
        if ($logs_flag==1){
            $logs_update_query = "update order_details set logistcs_services_used = $logs_flag where order_code=$order_code";
            $result = mysqli_query($this->conn, $logs_update_query);
            $api_params = array(
                "order_code" => $this->getOrderStringFromOrderCode($order_code),
                "city_code" => $city_code,
            );

            $net_api = $this->getNetworkApi();

            $response = $net_api->logisticsCreateOrder($api_params);
            if(null == $response || false == $response['success']) {
                $logistic_order_sent = false;
            }
            else {
                $logistic_order_sent = true;
            }
        }

		$cashback_active = false;
		
		if($cashback_active){
			$cashback_success = true;
			$max_cashback = $amount;
			$cashback_amount = $amount;
			$response = null;
			$transaction_params = array(
				"t_type" => 'CASHBACK' ,
				"user_code" => $user_code,
				"owner_type" => 0,
				"urid" => $order_code,
				"urid_type" => "ORDER",
				"amount" => $cashback_amount,
			);
			$net_api = $this->getNetworkApi();

			$response = $net_api->makeTransaction($transaction_params);
			if(null == $response || false == $response['success']) {
				$cashback_success = false;
			}
			else {
				$cashback_success = true;
			}
        }
        
        return $logistic_order_sent;
    }
    
    public function postOrderDetailsToServerFinal_v3($orderDataArray, $userCode, $userAddress, $addressUserCode, $totalAmount,
     $expectedDeliveryDate, $spCode, $stCode, $delivery_amount, $discount_amount, $total_items_amount, $coupon_code){
        $stmt = "SELECT id, order_code FROM order_details ORDER BY id DESC LIMIT 1";
        $result=mysqli_query($this->conn, $stmt);
        $response = array();
        $response["error"] = 1;

        if(mysqli_num_rows($result)==0){
            $order_code='1';
        }
        else{
            $row_order_id_present = mysqli_fetch_array($result);
            $order_code=$row_order_id_present["order_code"];
            $order_code=intval($order_code)+1;
        }
            
	    $date = date("D M d, Y G:i");
		$stmt_order_details="insert into order_details(order_code, user_code, expected_del_time,
		del_address_code, del_address_user_code, order_date, order_tot_amount, order_total_cost, order_delivery_cost, order_discount_cost,
		 order_orig_amount, coupon_code)values('".$order_code."', '".$userCode."', FROM_UNIXTIME(".$expectedDeliveryDate."),
		  '".$userAddress."', '".$addressUserCode."', '".$date."', $totalAmount, $total_items_amount, $delivery_amount, $discount_amount
		  , $total_items_amount, lower('".$coupon_code."') )";
		  
		$result_order_details=mysqli_query($this->conn, $stmt_order_details);
		
		if($result_order_details && 0 != mysqli_affected_rows($this->conn)){
			$response["error"] = 1;
		}
		else
			$response["error"] = 0;
		
		foreach($orderDataArray as $obj){
			$itemcode=$obj['itemCode'];
			$itemquantityselected=$obj['itemQuantitySelected'];
			$itemcost=$obj['itemCost'];
						
			$query = "insert into order_item_details(item_code, item_quantity, user_code, order_code, item_cost) 
				values ('".$itemcode."', '".$itemquantityselected."', '".$userCode."', '".$order_code."', $itemcost )";
			
			$stmt_insert = $query;
			$result_insert=mysqli_query($this->conn, $stmt_insert);
		}
		$stmt_order_in_progress="insert into order_in_progress(order_code, sp_code, status, max_no_of_items, user_code) values (
			'".$order_code."', '".$spCode."', 'Pending', 0, '".$userCode."')";
		$result_in_progress=mysqli_query($this->conn, $stmt_order_in_progress);
		
		if($coupon_code != ""){
			$query = " update coupons_by_lazylad set coupons_applied = coupons_applied + 1 
						where coupon_name = '".$coupon_code."' ";
			mysqli_query($this->conn, $query);
		}
		
		$this->pushServiceProviderOrderNotification($spCode);
		
		if(1 == $response["error"]){
 			$this->handleOrderSuccessfulPlaced($order_code, $userCode, $total_items_amount, $spCode);
 		}

		$order_string = $this->getOrderStringFromOrderCode($order_code);
// 		$response["order_code"] = $order_string;
		$response["order_code"] = $order_code; // original order code in this release as app is parsing that as int

        return $response;
    }

    private function processNewOrderPayment($order_code, $user_code, $order_payment_details, $order_amount){
        if(null == $order_payment_details)
            return true;

        $payment_types = array("WALLET", "ONLINE", "COD");

        $wallet_usage_amount = $order_amount;

        $net_api = $this->getNetworkApi();
        $wallet_params = array(
            "user_code" => $user_code,
            "owner_type" => 0,
        );
        $response = $net_api->getWalletDetails($wallet_params);
        if($response && $response["success"]){
            $wallet = $response["wallet"];
            foreach ($wallet["wallet_usage"] as $usage_entry){
                if($usage_entry["urid_type"] == "Order"){
                    if(1 == $usage_entry["usage_type"]){
                        $wallet_usage_amount = $order_amount * ($usage_entry["usage_number"] / 100);
                    }
                    $wallet_usage_amount = min ($wallet_usage_amount, $usage_entry["usage_cap"] );

                    break;
                }
            }
        }

        $payment_amount = arrayValue($order_payment_details, $payment_types[0]);

        if(null != $payment_amount && $payment_amount >= $wallet_usage_amount){
            if(null != arrayValue($order_payment_details, $payment_types[1])
                && 0 !=  arrayValue($order_payment_details, $payment_types[1]))
            {
                $order_payment_details[$payment_types[1]] += $payment_amount-$wallet_usage_amount;
            }
            else{
                $order_payment_details[$payment_types[2]] += $payment_amount-$wallet_usage_amount;
            }

            $order_payment_details[$payment_types[0]] = $wallet_usage_amount;
        }

//    	foreach ($payment_types as $p_type){
//     		$payment_amount = arrayValue($order_payment_details, $p_type);
//     		if(null != $payment_amount){
//     			$query = "insert into order_payment_details (order_code, p_mode, amount, p_status)
//     			 values ($order_code, '$p_type', $payment_amount, 'PENDING')";
//     			$result = mysqli_query($this->conn, $query);
//     		}
//     	}

        $payment_failed = false;
        $transaction_params = array(
            "t_type" => null ,
            "user_code" => $user_code,
            "owner_type" => '0',
            "urid" => $order_code,
            "urid_type" => "ORDER",
            "p_mode" => 0,
            "amount" => 0,
            "accounting" => "0", //0 for Debit
        );
        $net_api = $this->getNetworkApi();

        foreach ($payment_types as $p_type){
            $payment_amount = arrayValue($order_payment_details, $p_type);
            if(null != $payment_amount && 0 != $payment_amount){

                if($payment_types[0] == $p_type){
                    $transaction_params['t_type'] = ORDER_PAY_WALLET_CUSTOMER;
                }
                if($payment_types[1] == $p_type){
                    $transaction_params['t_type'] = ORDER_PAY_ONLINE_CUSTOMER;
                }

                if($payment_types[0] == $p_type || $payment_types[1] == $p_type){
                    $transaction_params['p_mode'] = $p_type;
                    $transaction_params['amount'] = $payment_amount;

                    $response = $net_api->makeTransaction($transaction_params);
                    if(null == $response || false == $response['success']){
                        $payment_failed = true;
                        break;
                    }
                }
            }
        }
        return !$payment_failed;
    }
    
    private function processNewOrderPayment_V2($order_code, $user_code, $order_payment_details, $order_amount, $sp_code){
    	if(null == $order_payment_details)
    		return true; 
    	
    	$payment_types = array("WALLET", "ONLINE", "COD", "PROMOTIONAL", "OFFER");
    	
    	$wallet_usage_amount = $order_amount;
    	
    	$net_api = $this->getNetworkApi();
    	$wallet_params = array(
    		"user_code" => $user_code,
    		"owner_type" => 0,
    		);
    	$response = $net_api->getWalletDetails($wallet_params);
    	if($response && $response["success"]){
    		$wallet = $response["wallet"];
			foreach ($wallet["wallet_usage"] as $usage_entry){
				if($usage_entry["urid_type"] == "Order"){
					if(1 == $usage_entry["usage_type"]){
						$wallet_usage_amount = $order_amount * ($usage_entry["usage_number"] / 100);
					}
					$wallet_usage_amount = min ($wallet_usage_amount, $usage_entry["usage_cap"] );
					
					break;
				}
			}
    	}
        
		$payment_amount = arrayValue($order_payment_details, $payment_types[0]);
		
		if(null != $payment_amount && $payment_amount >= $wallet_usage_amount){
			if(null != arrayValue($order_payment_details, $payment_types[1]) 
				&& 0 !=  arrayValue($order_payment_details, $payment_types[1]))
			{
				$order_payment_details[$payment_types[1]] += $payment_amount-$wallet_usage_amount;
			}
			else{
				 $order_payment_details[$payment_types[2]] += $payment_amount-$wallet_usage_amount;
			}
			
			$order_payment_details[$payment_types[0]] = $wallet_usage_amount;
		}
    	
//    	foreach ($payment_types as $p_type){
//     		$payment_amount = arrayValue($order_payment_details, $p_type);
//     		if(null != $payment_amount){
//     			$query = "insert into order_payment_details (order_code, p_mode, amount, p_status)
//     			 values ($order_code, '$p_type', $payment_amount, 'PENDING')";
//     			$result = mysqli_query($this->conn, $query);
//     		}
//     	}

    	$payment_failed = false;
    	$transaction_params = array(
			"t_type" => null ,
			"user_code" => $user_code,
			"owner_type" => '0',
			"urid" => $order_code,
			"urid_type" => "ORDER",
			"p_mode" => 0,
			"amount" => 0,
			"accounting" => "0", //0 for Debit
			);
		$net_api = $this->getNetworkApi();

    	foreach ($payment_types as $p_type){
    		$payment_amount = arrayValue($order_payment_details, $p_type);
    		if(null != $payment_amount && 0 != $payment_amount){
    			
    			if($payment_types[0] == $p_type){
    				$transaction_params['t_type'] = ORDER_PAY_WALLET_CUSTOMER;
    			}
    			if($payment_types[1] == $p_type){
    				$transaction_params['t_type'] = ORDER_PAY_ONLINE_CUSTOMER;
    			}

                if($payment_types[3] == $p_type){
                    $query = "insert into order_item_offer_details (order_code, sp_code, offer_diff) values ('".$order_code."', '".$sp_code."',$payment_amount )";
                    mysqli_query($this->conn, $query);
                }

                if($payment_types[4] == $p_type){
                    $query = "insert into order_item_special_offer_details (order_code, sp_code, offer_diff) values ('".$order_code."', '".$sp_code."',$payment_amount )";
                    mysqli_query($this->conn, $query);
                }

    			if($payment_types[0] == $p_type || $payment_types[1] == $p_type ){
					$transaction_params['p_mode'] = $p_type;
					$transaction_params['amount'] = $payment_amount;
					
					$response = $net_api->makeTransaction($transaction_params);
					if(null == $response || false == $response['success']){
							$payment_failed = true;
							break;
					}
				}
    		}
    	}
    	return !$payment_failed;
    }

    public function postOrderDetailsToServerFinal_v4($orderDataArray, $userCode, $userAddress, $addressUserCode, $totalAmount,
                                                     $expectedDeliveryDate, $spCode, $delivery_amount, $discount_amount, $total_items_amount, $coupon_code,
                                                     $order_payment_details, $taxes){
        $stmt = "SELECT id, order_code FROM order_details ORDER BY id DESC LIMIT 1";
        $result=mysqli_query($this->conn, $stmt);
        $response = array();
        $response["error"] = 1;

        if(mysqli_num_rows($result)==0){
            $order_code='1';
        }
        else{
            $row_order_id_present = mysqli_fetch_array($result);
            $order_code=$row_order_id_present["order_code"];
            $order_code=intval($order_code)+1;
        }

        $date = date("D M d, Y G:i");
        $stmt_order_details="insert into order_details(order_code, user_code, expected_del_time,
		del_address_code, del_address_user_code, order_date, order_tot_amount, order_total_cost, order_delivery_cost, order_discount_cost,
		 order_orig_amount, coupon_code, taxes, tax_percentage)
		 ( select '".$order_code."', '".$userCode."', FROM_UNIXTIME(".$expectedDeliveryDate."),
		  '".$userAddress."', '".$addressUserCode."', '".$date."', $totalAmount, $total_items_amount, $delivery_amount, $discount_amount
		  , $total_items_amount, lower('".$coupon_code."'), $taxes,
		   (tax_flag_1 * tax_number_1 + tax_flag_2 * tax_number_2 + tax_flag_3 * tax_number_3) as total_tax_percentage
		   from service_providers where sp_code = $spCode)";

        $result_order_details=mysqli_query($this->conn, $stmt_order_details);

        if($result_order_details && 0 != mysqli_affected_rows($this->conn)){
            $response["error"] = 1;
        }
        else
            $response["error"] = 0;

        foreach($orderDataArray as $obj){
            $itemcode=$obj['itemCode'];
            $itemquantityselected=$obj['itemQuantitySelected'];
            $itemcost=$obj['itemCost'];

            $query = "insert into order_item_details(item_code, item_quantity, user_code, order_code, item_cost)
				values ('".$itemcode."', '".$itemquantityselected."', '".$userCode."', '".$order_code."', $itemcost )";

            $stmt_insert = $query;
            $result_insert=mysqli_query($this->conn, $stmt_insert);
        }

        $payment_success = $this->processNewOrderPayment($order_code, $userCode, $order_payment_details, $totalAmount);
        if($payment_success){
            $order_status = 'Pending';
            $response["payment_success"] = true;
        }
        else {
            $order_status = 'Payment_Failed';
            $response["payment_success"] = false;
        }

        $stmt_order_in_progress="insert into order_in_progress(order_code, sp_code, status, max_no_of_items, user_code) values (
			'".$order_code."', '".$spCode."', '$order_status', 0, '".$userCode."')";
        $result_in_progress=mysqli_query($this->conn, $stmt_order_in_progress);

        if($payment_success && $coupon_code != ""){
            $query = " update coupons_by_lazylad set coupons_applied = coupons_applied + 1
						where coupon_name = '".$coupon_code."' ";
            mysqli_query($this->conn, $query);
        }

        if($payment_success) $this->pushServiceProviderOrderNotification($spCode);

        if(1 == $response["error"]){
            $this->handleOrderSuccessfulPlaced($order_code, $userCode, $total_items_amount, $spCode);
        }

        $order_string = $this->getOrderStringFromOrderCode($order_code);
        $response["order_code"] = $order_string;

        return $response;
    }


    public function postOrderDetailsToServerFinal_v5($orderDataArray, $userCode, $userAddress, $addressUserCode, $totalAmount,
     $expectedDeliveryDate, $spCode, $delivery_amount, $discount_amount, $total_items_amount, $coupon_code,
      $order_payment_details, $taxes){
        $stmt = "SELECT id, order_code FROM order_details ORDER BY id DESC LIMIT 1";
        $result=mysqli_query($this->conn, $stmt);
        $response = array();
        $response["error"] = 1;

        if(mysqli_num_rows($result)==0){
            $order_code='1';
        }
        else{
            $row_order_id_present = mysqli_fetch_array($result);
            $order_code=$row_order_id_present["order_code"];
            $order_code=intval($order_code)+1;
        }
            
	    $date = date("D M d, Y G:i");
		$stmt_order_details="insert into order_details(order_code, user_code, expected_del_time,
		del_address_code, del_address_user_code, order_date, order_tot_amount, order_total_cost, order_delivery_cost, order_discount_cost,
		 order_orig_amount, coupon_code, taxes, tax_percentage)
		 ( select '".$order_code."', '".$userCode."', FROM_UNIXTIME(".$expectedDeliveryDate."),
		  '".$userAddress."', '".$addressUserCode."', '".$date."', $totalAmount, $total_items_amount, $delivery_amount, $discount_amount
		  , $total_items_amount, lower('".$coupon_code."'), $taxes,
		   (tax_flag_1 * tax_number_1 + tax_flag_2 * tax_number_2 + tax_flag_3 * tax_number_3) as total_tax_percentage
		   from service_providers where sp_code = $spCode)";
		
		$result_order_details=mysqli_query($this->conn, $stmt_order_details);
		
		if($result_order_details && 0 != mysqli_affected_rows($this->conn)){
			$response["error"] = 1;
		}
		else
			$response["error"] = 0;
		
		foreach($orderDataArray as $obj){
			$itemcode=$obj['itemCode'];
			$itemquantityselected=$obj['itemQuantitySelected'];
			$itemcost=$obj['itemCost'];
            $itemoffercost=$obj['itemOfferCost'];
            $itemofferflag=$obj['itemOfferFlag'];
						
			$query = "insert into order_item_details(item_code, item_quantity, user_code, order_code, item_offer_price, item_offer_flag, item_cost)
				values ('".$itemcode."', '".$itemquantityselected."', '".$userCode."', '".$order_code."', '".$itemoffercost."', '".$itemofferflag."', $itemcost )";
			
			$stmt_insert = $query;
			$result_insert=mysqli_query($this->conn, $stmt_insert);
		}
		
		$payment_success = $this->processNewOrderPayment_V2($order_code, $userCode, $order_payment_details, $totalAmount, $spCode);
		if($payment_success){
			$order_status = 'Pending';
			$response["payment_success"] = true;	
		}
		else {
			$order_status = 'Payment_Failed';
			$response["payment_success"] = false;
		}
		
		$stmt_order_in_progress="insert into order_in_progress(order_code, sp_code, status, max_no_of_items, user_code) values (
			'".$order_code."', '".$spCode."', '$order_status', 0, '".$userCode."')";
		$result_in_progress=mysqli_query($this->conn, $stmt_order_in_progress);
		
		if($payment_success && $coupon_code != ""){
			$query = " update coupons_by_lazylad set coupons_applied = coupons_applied + 1 
						where coupon_name = '".$coupon_code."' ";
			mysqli_query($this->conn, $query);
		}
		
		if($payment_success) $this->pushServiceProviderOrderNotification($spCode);
		
		if(1 == $response["error"]){
 			$this->handleOrderSuccessfulPlaced($order_code, $userCode, $total_items_amount, $spCode);
 		}

		$order_string = $this->getOrderStringFromOrderCode($order_code);
		$response["
        "] = $order_string;

        return $response;
    }

    public function post_order($orderDataArray, $userCode, $userAddress, $addressUserCode, $totalAmount,
     $expectedDeliveryDate, $spCode, $delivery_amount, $discount_amount, $total_items_amount, $coupon_code,
      $order_payment_details, $taxes){
        $order_params = array(
            "JsonDataArray" => $orderDataArray,
            "taxes" => $taxes,
            "sp_code" => $spCode,
            "user_code" => $userCode,
            "address_code" => $userAddress,
            "address_user_code" => $addressUserCode,
            "user_exp_del_time" => $expectedDeliveryDate,
            "tot_amount" => $totalAmount,
            "delivery_charges" => $delivery_amount,
            "discount_amount" => $discount_amount,
            "total_items_amount" => $total_items_amount,
            "couponCode" => $coupon_code,
            "order_payment_details" => $order_payment_details
        );

        $net_api = $this->getNetworkApi();

        $response = $net_api->post_order_final($order_params);
        return $response;

    }

    public function addNewAddressAndGetAddressCode($user_code, $customer_name, $customer_number, $customer_email, $customer_address){
        $usercode=intval($user_code);
        $response = array();

        if($usercode==0){
            /*
             *
             * First Time User
             * */
            $stmt="select id, user_code from user_details ORDER BY id DESC LIMIT 1";
            $result=mysqli_query($this->conn, $stmt);
            if(mysqli_num_rows($result)==0){
                /*
                 * First User
                 * */
                $userid=1;
                $stmt_insert_user_details="insert into user_details(user_code) values ('".$userid."')";
                $result_insert_user_details=mysqli_query($this->conn, $stmt_insert_user_details);
                if($result_insert_user_details){
                    $addressCode=1;
                    $stmt_insert_user_address="insert into user_address_details(user_code, user_address_code, user_address, user_name, user_email_id, user_phone_number) values 
                    ($userid, $addressCode, '".$customer_address."', '".$customer_name."', '".$customer_email."',
                    '".$customer_number."')";
                    $result_insert_user_address=mysqli_query($this->conn, $stmt_insert_user_address);
                    if($result_insert_user_address){
                        $response["error"] = 1;
                        $response["user_details"] = array();
                        $tmp = array();
                        $tmp["user_code"] = $userid;
                        $tmp["user_address_code"] = $addressCode;
                        array_push($response["user_details"], $tmp);
                        return $response;
                    }else{
                        $response["error"] = 0;
                        $response["user_details"] = array();
                        $tmp = array();
                        $tmp["message"] = "Error";
                        array_push($response["user_details"], $tmp);
                        //TODO error handling
                    }
                }else{
                    $response["error"] = 0;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["message"] = "Error";
                    array_push($response["user_details"], $tmp);
                    //TODO Error Handling
                }
            }else{
                /*
                 * Not First User
                 * */
                $row_user_code_fetch=mysqli_fetch_array($result);
                $userid=$row_user_code_fetch['user_code'];
                $userid=$userid+1;
                $stmt_insert_user_details="insert into user_details(user_code) values ('".$userid."')";
                $result_insert_user_details=mysqli_query($this->conn, $stmt_insert_user_details);
                if($result_insert_user_details){
                    $addressCode=1;
                    $stmt_insert_user_address="insert into user_address_details(user_code, user_address_code, user_address, user_name, user_email_id, user_phone_number) values
                    ($userid, $addressCode, '".$customer_address."', '".$customer_name."', '".$customer_email."',
                    '".$customer_number."')";
                    $result_insert_user_address=mysqli_query($this->conn, $stmt_insert_user_address);
                    if($result_insert_user_address){
                        $response["error"] = 1;
                        $response["user_details"] = array();
                        $tmp = array();
                        $tmp["user_code"] = $userid;
                        $tmp["user_address_code"] = $addressCode;
                        array_push($response["user_details"], $tmp);
                        return $response;
                    }else{
                        $response["error"] = 0;
                        $response["user_details"] = array();
                        $tmp = array();
                        $tmp["message"] = "Error";
                        array_push($response["user_details"], $tmp);
                        //TODO error handling
                    }
                }else{
                    $response["error"] = 0;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["message"] = "Error";
                    array_push($response["user_details"], $tmp);
                    //TODO Error Handling
                }
            }
        }else{
            /*
             * Not First Time user
             * */
            $stmt_select_user_address="select id, user_address_code from user_address_details where user_code=$usercode ORDER BY id DESC LIMIT 1";
            $result_select_user_address=mysqli_query($this->conn, $stmt_select_user_address);

            if(mysqli_num_rows($result_select_user_address)==0){
                $address_code=1;
                $stmt_insert_user_add="insert into user_address_details(user_code, user_address_code, user_address, user_name, user_email_id, user_phone_number) values
                ($usercode, $address_code, '".$customer_address."', '".$customer_name."', '".$customer_email."',
                    '".$customer_number."')";
                $result_insert_user_add=mysqli_query($this->conn, $stmt_insert_user_add);
                if($result_insert_user_add){
                    $response["error"] = 1;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["user_code"] = $usercode;
                    $tmp["user_address_code"] = $address_code;
                    array_push($response["user_details"], $tmp);
                    return $response;
                }else{
                    $response["error"] = 0;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["message"] = "Error";
                    array_push($response["user_details"], $tmp);
                    //TODO error handling
                }
            }else{
                $row_address_code_fetch=mysqli_fetch_array($result_select_user_address);
                $address_code=$row_address_code_fetch['user_address_code'];
                $address_code=$address_code+1;
                $stmt_insert_user_add="insert into user_address_details(user_code, user_address_code, user_address, user_name, user_email_id, user_phone_number) values
                ($usercode, $address_code, '".$customer_address."', '".$customer_name."', '".$customer_email."',
                    '".$customer_number."')";
                $result_insert_user_add=mysqli_query($this->conn, $stmt_insert_user_add);
                if($result_insert_user_add){
                    $response["error"] = 1;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["user_code"] = $usercode;
                    $tmp["user_address_code"] = $address_code;
                    array_push($response["user_details"], $tmp);
                    return $response;
                }else{
                    $response["error"] = 0;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["message"] = "Error";
                    array_push($response["user_details"], $tmp);
                    //TODO error handling
                }
            }
        }
    }

    public function signInServiceProvider($emailId, $password, $regId){
        $stmt="select sp_code from service_providers where sp_email='".$emailId."' and sp_password='".$password."'";
        $result=mysqli_query($this->conn, $stmt);
        $response = array();
        if(mysqli_num_rows($result)==1){
            $response["error"] = 1;
            $row_serv_prov_code_present = mysqli_fetch_array($result);
            $spcode=$row_serv_prov_code_present["sp_code"];
            $stmt_update="update service_providers set reg_id='".$regId."' where sp_code='".$spcode."'";
            $result_update=mysqli_query($this->conn, $stmt_update);
            if($result_update){
                $response["servProv_code"]=$spcode;
            }else{
                //TODO if update not done than error handling
            }
        }else{
            $response["error"] = 0;
            $response["message"] = "Such User Doesnot exists";
            //TODO
        }
        return $response;
    }
    
    public function signInServiceProviderAndGetType($emailId, $password, $regId){
    	
    	// After 1.0 of Seller App login is preferred with phone number as unique id but
    	// for backward compatibility it falls back to checking of email id
    	$seller_phone = $emailId;
        
        $stmt="select sp_code, sp_service_type_code from service_providers where sp_number='".$seller_phone."' and sp_password='".$password."'";
        $result=mysqli_query($this->conn, $stmt);
        $response = array();
        if(mysqli_num_rows($result)==1){
            $response["error"] = 1;
            $row_serv_prov_code_present = mysqli_fetch_array($result);
            $spcode=$row_serv_prov_code_present["sp_code"];
            $stcode=$row_serv_prov_code_present["sp_service_type_code"];
            $stmt_update="update service_providers set reg_id='".$regId."' where sp_code='".$spcode."'";
            $result_update=mysqli_query($this->conn, $stmt_update);
            if($result_update){
                $response["servProv_code"]=$spcode;
                $response["servProv_type"]=$stcode;
            }else{
                //TODO if update not done than error handling
            }
        }
        else {
			$stmt="select sp_code, sp_service_type_code from service_providers where sp_email='".$emailId."' and sp_password='".$password."'";
			$result=mysqli_query($this->conn, $stmt);
			$response = array();
			if(mysqli_num_rows($result)==1){
				$response["error"] = 1;
				$row_serv_prov_code_present = mysqli_fetch_array($result);
				$spcode=$row_serv_prov_code_present["sp_code"];
				$stcode=$row_serv_prov_code_present["sp_service_type_code"];
				$stmt_update="update service_providers set reg_id='".$regId."' where sp_code='".$spcode."'";
				$result_update=mysqli_query($this->conn, $stmt_update);
				if($result_update){
					$response["servProv_code"]=$spcode;
					$response["servProv_type"]=$stcode;
				}else{
					//TODO if update not done than error handling
				}
			}
			else{
				$response["error"] = 0;
				$response["message"] = "Such User Doesnot exists";
				//TODO
        	}
        }
        
        return $response;
    }
    
    public function AddNewUser($user_code, $reg_id){
        $usercode=intval($user_code);
        $response = array();
        if($usercode==0){
            /*
             *
             * First Time User
             * */
            $stmt="select id, user_code from user_details ORDER BY id DESC LIMIT 1";
            $result=mysqli_query($this->conn, $stmt);
            if(mysqli_num_rows($result)==0){
                /*
                 * First User
                 * */
                $userid=1;
                $stmt_insert_user_details="insert into user_details(user_code, reg_id, created) values ('".$userid."', '".$reg_id."', now())";
                $result_insert_user_details=mysqli_query($this->conn, $stmt_insert_user_details);
                if($result_insert_user_details){
                    $response["error"] = 1;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["user_code"]=$userid;
                    array_push($response["user_details"], $tmp);
                }else{
                    $response["error"] = 0;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["message"] = "Error";
                    array_push($response["user_details"], $tmp);
                    //TODO Error Handling
                }
            }else{
                /*
                 * Not First User
                 * */
                $row_user_code_fetch=mysqli_fetch_array($result);
                $userid=$row_user_code_fetch['user_code'];
                $userid=intval($userid)+1;
                $stmt_insert_user_details="insert into user_details(user_code, reg_id, created) values ('".$userid."', '".$reg_id."', now())";
                $result_insert_user_details=mysqli_query($this->conn, $stmt_insert_user_details);
                if($result_insert_user_details){
                    $response["error"] = 1;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["user_code"]=$userid;
                    array_push($response["user_details"], $tmp);
                }else{
                    $response["error"] = 0;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["message"] = "Error";
                    array_push($response["user_details"], $tmp);
                    //TODO Error Handling
                }
            }
        }
        else{
        	$response["error"] = 1;
        	
        	$query = "update user_details set reg_id = '".$reg_id."' where user_code = $user_code";
        	mysqli_query($this->conn, $query);
        }
        return $response;
    }
    
    private function CreateorGetProfileId($user_code){
    	$return_profile_id = -1;
    	
    	$query = " select phone_number from user_details where user_code = $user_code ";
		$result_phonenum = mysqli_query($this->conn, $query);
		if($result_phonenum && ($row = mysqli_fetch_array($result_phonenum)) && null != $row['phone_number']){
			$phoneNumber = $row['phone_number'];
    	
			$query = "select profile_id from user_profile where phone_number = ? ";
			$stmt = $this->conn->prepare($query);
			$stmt->bind_param("s", $phoneNumber);
			$result = $stmt->execute();
			if($result){
				$stmt->bind_result($profile_id);
				if($stmt->fetch()){
					$return_profile_id = $profile_id;
				}
				else{
					$stmt->close();
					
					$query = " insert into user_profile (phone_number) values (?) ";
					$stmt = $this->conn->prepare($query);
					$stmt->bind_param("s", $phoneNumber);
					$result = $stmt->execute();
					if($result && 0 != $stmt->affected_rows){
						$profile_id = $stmt->insert_id;
						$return_profile_id = $profile_id;
					}
					else $return_profile_id = -1;
				}
			}
			else $return_profile_id = -1;
			
			$stmt->close();
    		mysqli_free_result($result_phonenum);
    		
    	}
    	return $return_profile_id;

    }
    
    public function AddNumberWithUser($userCode, $phoneNumber, $referralCode){
    	
    	$response = array();
    	
    	$query = "update user_details set phone_number = ? where user_code = ? ";
    	$stmt = $this->conn->prepare($query);
    	$stmt->bind_param("ss", $phoneNumber, $userCode);
    	$result = $stmt->execute();
    	if($result)
    		$response["error"] = 1;
    	else
    		$response["error"] = 0;
    		
    	$stmt->close();
    		
    	$this->CreateorGetProfileId($userCode);
    	
    	$this->performReferralCustomer($referralCode, $userCode);
    		
    	return $response;
    }

    public function AddNumberWithUserv2($userCode, $phoneNumber, $verification_source){
        
        $response = array();
        
        $query = "update user_details set phone_number = ? , verification_source = ? where user_code = ? ";
        $stmt = $this->conn->prepare($query);
        $stmt->bind_param("sss", $phoneNumber, $verification_source, $userCode);
        $result = $stmt->execute();
        if($result)
            $response["error"] = 1;
        else
            $response["error"] = 0;
            
        $stmt->close();
            
        $this->CreateorGetProfileId($userCode);
            
        return $response;
    }

    public function ConfirmItemsServiceProvider($confirmItemDataArray, $serv_prov_code){
        $response = array();
        if($this->isServiceProviderValid($serv_prov_code)){
            foreach($confirmItemDataArray as $obj){
                $itemcode=$obj['item_code'];
                $itemname=$obj['item_name'];
                $itemimgflag=$obj['item_img_flag'];
                $itemimgadd=$obj['item_img_add'];
                $itemunit=$obj['item_unit'];
                $itemshortdesc=$obj['item_short_desc'];
                $itemdesc=$obj['item_desc'];
                $itemstatus=$obj['item_status'];
                $itemcost=$obj['item_cost'];
                $itemtypecode=$obj['item_type_code'];
                $itemquantity=$obj['item_qunatity'];
                $stmt="select item_code from items_offered_by_service_provider where item_code='".$itemcode."' and sp_code='".$serv_prov_code."'";
                $result=mysqli_query($this->conn, $stmt);
                if(mysqli_num_rows($result)==0){
                    $stmt_insert="insert into items_offered_by_service_provider(item_code, sp_code, item_cost, item_status) values ('".$itemcode."', '".$serv_prov_code."', '".$itemcost."', '".$itemstatus."')";
                    $result_insert=mysqli_query($this->conn, $stmt_insert);
                    if($result_insert){
                        $response["error"]=1;
                        $response["message"]="Inserted Successfully";
                    }else{
                        $response["error"]=0;
                        $response["message"]="Insert Unsuccessfully";
                    }
                }
                if(mysqli_num_rows($result)==1){
                    //TODO update the item details
                }
            }
        }else{
            $response["error"]=0;
            $response["message"]="Service Provider Not Available";
        }
        return $response;
    }
    
    public function ConfirmItemsServiceProviderWithCategoryGSON($confirmItemDataArray, $serv_prov_code, $serv_prov_cat){
    //     $response = array();
    //     $updatecount = 0;

    //     if($this->isServiceProviderValid($serv_prov_code)){
    //     	$response["error"]=1;
    //         foreach($confirmItemDataArray as $obj){
    //             $itemcode=$obj['item_code'];
    //             $itemname=$obj['item_name'];
    //             $itemimgflag=$obj['item_img_flag'];
    //             $itemimgadd=$obj['item_img_add'];
    //             $itemunit=$obj['item_unit'];
    //             $itemshortdesc=$obj['item_short_desc'];
    //             $itemdesc=$obj['item_desc'];
    //             $itemstatus=$obj['item_status'];
    //             $itemcost=$obj['item_cost'];
    //             $itemtypecode=$obj['item_type_code'];
    //             $itemquantity=$obj['item_qunatity'];
    //             $stmt="select item_code from items_offered_by_service_provider where item_code='".$itemcode."' and sp_code='".$serv_prov_code."' and sc_code='".$serv_prov_cat."'";
    //             $result=mysqli_query($this->conn, $stmt);
    //             if(mysqli_num_rows($result)==0){
    //             	if(intval($itemstatus) == 1 || intval($itemstatus) == 3){   // Available or Out of Stock
    //                     $item_status=1;
				// 		$stmt_insert="insert into items_offered_by_service_provider (item_code, sp_code, item_cost, item_status) values (
				// 		'".$itemcode."', '".$serv_prov_code."', '".$itemcost."','".$item_status."'
				// 		)";
						
				// 		$result_insert=mysqli_query($this->conn, $stmt_insert);
				// 		if($result_insert){
				// 			$response["error"]=1;
				// 			$response["message"]="Inserted Successfully";
				// 		}else{
				// 			$response["error"]=0;
				// 			$response["message"]="Insert Unsuccessfully";
				// 		}
    //                 }
    //                 else{
    //                 	// do nothing, probably price change of non available items tried
    //                 	$response["error"]=1;
    //                 }
    //             }
    //             else if(mysqli_num_rows($result)==1){
    //                 //TODO update the item details
    //                 if(intval($itemstatus) == 1 || intval($itemstatus) == 3){   // Available or Out of Stock
    //                 	$query = "update items_offered_by_service_provider set item_cost = $itemcost , item_status = $itemstatus where item_code = $itemcode and sp_code = $serv_prov_code"
    //                 	." and sc_code = $serv_prov_cat";
    //                 	$stmt_update = $this->conn->prepare($query);
    //                 	$result_update = $stmt_update->execute();
    //                 	if($result_update){
				// 			$
				// 		}else{
				// 			$response["error"]=0;
				// 			$response["message"]="Update Unsuccessfully";
				// 		}
    //                 }
    //                 else if (intval($itemstatus) == 2){ // Not Available 
    //                 	$stmt_delete = $this->conn->prepare("delete from  items_offered_by_service_provider where item_code = $itemcode and sp_code = $serv_prov_code"
    //                 	." and sc_code = $serv_prov_cat");
    //                 	$stmt_delete->execute();
    //                 	$result_delete = $stmt_delete->execute();
    //                 	if($result_delete){
				// 			$response["error"]=1;
				// 			$response["message"]="Removed Successfully";
				// 		}else{
				// 			$response["error"]=0;
				// 			$response["message"]="Remove Unsuccessfully";
				// 		}
    //                 } 
    //             }
    //             else{
				// 	$response["error"]=0;
				// 	$response["message"]="Multiple Entries";
				// }
    //             if($response["error"] != 0)$updatecount = $updatecount+1;
    //         }
    //     }else{
    //         $response["error"]=0;
    //         $response["message"]="Service Provider Not Available";
    //     }
    //     $response["Update_Count"] = $updatecount;
        $params = array(
            "sp_code" => $serv_prov_code,
            "sc_code" => $serv_prov_cat,
            "JsonDataArray" => $confirmItemDataArray,
        );
        $net_api = $this->getNetworkApi();

        $response = $net_api->confirm_items_service_provider($params);
        return $response;
    }
    
    public function ConfirmItemsServiceProviderInSearchGSON($confirmItemDataArray, $serv_prov_code){
        $response = array();
        $updatecount = 0;
        
        if($this->isServiceProviderValid($serv_prov_code)){
        	$response["error"]=1;
            foreach($confirmItemDataArray as $obj){
                $itemcode=$obj['item_code'];
                $itemname=$obj['item_name'];
                $itemimgflag=$obj['item_img_flag'];
                $itemimgadd=$obj['item_img_add'];
                $itemunit=$obj['item_unit'];
                $itemshortdesc=$obj['item_short_desc'];
                $itemdesc=$obj['item_desc'];
                $itemstatus=$obj['item_status'];
                $itemcost=$obj['item_cost'];
                $itemtypecode=$obj['item_type_code'];
                $itemquantity=$obj['item_qunatity'];
                $item_serv_prov_cat = $obj["item_sc_code"];
                $stmt="select item_code from items_offered_by_service_provider where item_code='".$itemcode."' and sp_code='".$serv_prov_code."' and sc_code='".$item_serv_prov_cat."'";
                $result=mysqli_query($this->conn, $stmt);
                if(mysqli_num_rows($result)==0){
                	if(intval($itemstatus) == 1 || intval($itemstatus) == 3){   // Available or Out of Stock
                        $item_status = 1;
						$stmt_insert="insert into items_offered_by_service_provider(item_code, sp_code, item_cost, item_status) values (
						'".$itemcode."', '".$serv_prov_code."', '".$itemcost."','".$item_status."'
						)";
						
						$result_insert=mysqli_query($this->conn, $stmt_insert);
						if($result_insert){
							$response["error"]=1;
							$response["message"]="Inserted Successfully";
						}else{
							$response["error"]=0;
							$response["message"]="Insert Unsuccessfully";
						}
                    }
                    else{
                    	// do nothing, probably price change of non available items tried
                    	$response["error"]=1;
                    	$response["message"] = "no Op";
                    }
                }
                else if(mysqli_num_rows($result)==1){
                    //TODO update the item details
                    if(intval($itemstatus) == 1 || intval($itemstatus) == 3){   // Available or Out of Stock
                    	$query = "update items_offered_by_service_provider set item_cost = $itemcost , item_status = $itemstatus 
                        where item_code = $itemcode and sp_code = $serv_prov_code";
                    	$stmt_update = $this->conn->prepare($query);
                    	$result_update = $stmt_update->execute();
                    	if($result_update){
							$response["error"]=1;
							$response["message"]="Updated Successfully";
						}else{
							$response["error"]=0;
							$response["message"]="Update Unsuccessfully";
						}
                    }
                    else if (intval($itemstatus) == 2){ // Not Available 
                    	$stmt_delete = $this->conn->prepare("delete from  items_offered_by_service_provider where item_code = $itemcode and sp_code = $serv_prov_code"
                    	." and sc_code = $item_serv_prov_cat");
                    	$stmt_delete->execute();
                    	$result_delete = $stmt_delete->execute();
                    	if($result_delete){
							$response["error"]=1;
							$response["message"]="Removed Successfully";
						}else{
							$response["error"]=0;
							$response["message"]="Remove Unsuccessfully";
						}
                    } 
                }
                else{
					$response["error"]=0;
					$response["message"]="Multiple Entries";
				}
                if($response["error"] != 0)$updatecount = $updatecount+1;
            }
        }else{
            $response["error"]=0;
            $response["message"]="Service Provider Not Available";
        }
        $response["Update_Count"] = $updatecount;
        return $response;
    }
    
    public function getUserOrderBill($order_string){
    	$order_code = $this->getOrderCodeFromOrderString($order_string);
        $response = array();
        $stmt="SELECT os.order_string AS order_code, uad.user_name, uad.user_address, obd.order_time as order_date , 
        cod.post_total_amount as order_total_cost, cod.total_amount as order_total_cost, obd.delivery_charges as 
        order_delivery_cost, cod.post_discount_amount as order_discount_cost, cod.post_taxes as taxes, oip.sp_code
        FROM order_basic_details obd, order_in_progress oip, user_address_details uad, 
        order_strings os, confirmed_order_details cod
        WHERE obd.order_code = '".$order_code."'
        AND os.o_id = obd.id
        AND uad.user_code = obd.del_add_user_code
        AND uad.user_address_code = obd.del_add_code
        AND oip.order_code = obd.order_code
        AND cod.order_code=obd.order_code";

        $result=mysqli_query($this->conn, $stmt);

        if($result){
            $row = mysqli_fetch_array($result);
            $response["order_code"]=$row["order_code"];
            $response["user_name"]=$row["user_name"];
            $response["user_address"]=$row["user_address"];
            $response["order_date"]=$row["order_date"];
            $response["order_tot_amount"]=$row["order_tot_amount"];
            $response["order_total_cost"]=$row["order_total_cost"];
            $response["order_delivery_cost"]=$row["order_delivery_cost"];
            $response["order_discount_cost"]=$row["order_discount_cost"];
            $response["taxes"] = $row["taxes"];
            $response["sp_code"]=$row["sp_code"];
            $sp_code=$row["sp_code"];
            
            $stmt_order_item_details = $this->conn->prepare("SELECT iobsp.item_code, id.item_name, id.item_desc,
            id.item_short_desc, oid.item_cost, oid.item_quantity from order_item_details oid, order_item_served_by_sp oisbsp,
            items_offered_by_service_provider iobsp, item_details id where oisbsp.order_code=? and iobsp.sp_code=? and oisbsp.item_code=iobsp.item_code and oid.order_code=oisbsp.order_code and oid.item_code=oisbsp.item_code and id.item_code=iobsp.item_code");

            $response["order_item_details"] = array();

            $stmt_order_item_details->bind_param("ss", $order_code, $sp_code);

            $stmt_order_item_details->execute();

            $stmt_order_item_details->bind_result($itemcode, $itemname, $itemdesc, $itemshortdesc, $itemcost, $itemquantity);

            // looping through result and preparing tasks array
            while ($stmt_order_item_details->fetch()) {
                $tmp = array();
                $tmp["item_code"] = $itemcode;
                $tmp["item_name"] = $itemname;
                $tmp["item_short_desc"] = $itemshortdesc;
                $tmp["item_quantity"]=$itemquantity;
                $tmp["item_cost"]=$itemcost;
                $tmp["item_desc"]=$itemdesc;
                array_push($response["order_item_details"], $tmp);
            }

            $response["error"] = 1;

        }
        else{
            $response["error"] = 0;
        }
        return $response;
    }
    
    
//*************Seller Registration Process API's*******************
    private function isServProvUniqueIDPresent($sellerPhone){
    
    	$stmt = $this->conn->prepare("SELECT id from service_providers WHERE sp_number = ?");
		$stmt->bind_param("s", $sellerPhone);
		if(FALSE== $stmt->execute() )
			return 0;
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		$stmt->close();
		return $num_rows == 0;
    }
    
    /**
     * Checking for duplicate user by phone number
     * @param String $sellerPhone phone number used as unique id to check in db
     * @return $response array
     */
    public function CheckAvailabilityServProvUniqueID($sellerPhone) {
        $response = array();
        if($sellerPhone == null)
        {
        	$response["error"] = 0;
        }
        else{
				
			$response["error"] = 1;
			$response["availability_code"] = $this->isServProvUniqueIDPresent($sellerPhone)? 1:2;
        }
        return $response;
    }
    
    public function GetBasicInventoryforShopType($shop_type) {
    
        $response = array();
        $response["error"] = 1;   // 1 for false
        $response["items"] = array();
        $stmt = $this->conn->prepare("SELECT id, item_code, item_name, item_cost, item_desc, item_img_flag, item_img_add FROM item_details where st_code = ? and availability_class = 'CLASS_A'");

		$stmt->bind_param("s",strval($shop_type));

        $result = $stmt->execute();
        if(!$result)
		{
			$response["error"] = 0;
			return $response;
		}	
        $stmt->bind_result($id, $item_code, $item_name,$item_cost,$item_desc,$item_img_flag,$item_img_add);

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["item_code"] = intval($item_code);
            $tmp["item_name"] = $item_name;
            $tmp["item_cost"] = $item_cost;
            $tmp["item_desc"] = $item_desc;
            $tmp["item_img_flag"] = $item_img_flag;
            $tmp["item_img_add"] = $item_img_add;
            array_push($response["items"], $tmp);
        }
        $stmt->close();
        return $response;
    
    }
    
    public function GetServiceTypesforSeller(){
    	// old api without city_code
    	// returns Service types active in Gurgaon i.e. for city_code 1
    	$city_code = 1;
    	
        $response = array();
        $response["error"] = 1;   // 1 for false
        $response["service_types"] = array();
        
        $query = "SELECT st.id, st.st_code, st.st_name FROM service_types st, service_type_status sts 
        where st.st_code = sts.st_code and sts.city_code = ? and sts.launched_for_seller = TRUE";

		$stmt = $this->conn->prepare($query);

		$stmt->bind_param("i",$city_code);
        $stmt->execute();

        $stmt->bind_result($id, $stcode, $stname);

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["st_code"] = $stcode;
            $tmp["st_name"] = $stname;
            array_push($response["service_types"], $tmp);
        }
        $stmt->close();
        return $response;
    }
    
    public function GetServiceTypesforCityForSeller($city_code){
    
        $response = array();
        $response["error"] = 1;   // 1 for false
        $response["service_types"] = array();
        $stmt = $this->conn->prepare("SELECT st.id, st.st_code, st.st_name FROM service_types st, service_type_status sts 
        where st.st_code = sts.st_code and sts.city_code = ? and sts.launched_for_seller = TRUE");

		$stmt->bind_param("i",$city_code);
        $stmt->execute();

        $stmt->bind_result($id, $stcode, $stname);

        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["st_code"] = $stcode;
            $tmp["st_name"] = $stname;
            array_push($response["service_types"], $tmp);
        }
        $stmt->close();
        return $response;
    }
    
    public function PostCompleteRegistrationDetails($JsonServiceAreasArray, $JsonInventoryDetailsArray,
    		$seller_name, $seller_phone, $password, $shop_name, $shop_address, $shop_city, $shop_type, $shop_open_time,
    		 $shop_close_time, $delivery_time, $min_delivery_amt){
    		 
		$response = array();
		$response["error"] = 1;   // 1 for false
		
		if($seller_phone == null)
		{
			return $this->getServerFailureJsonResponse();
		}
		
		if(!$this->isServProvUniqueIDPresent($seller_phone))
		{
			$response["error"] = 1;
			$response["register_code"] = 2;
			return $response;
		}
		
		$stmt = $this->conn->prepare("SELECT sp_code from service_providers ORDER BY id DESC LIMIT 1");
		if(FALSE== $stmt->execute() )
			return $this->getServerFailureJsonResponse(0);
		$stmt->bind_result($sp_code);
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		if($num_rows > 0)
			$stmt->fetch();
		$stmt->close();
		
		if($num_rows > 0) $sp_code = intval($sp_code) + 1;
		else $sp_code  = 1;

		$defaultArea = $this->defaultAreaCodeforCity($shop_city);
		
		$stmt = $this->conn->prepare("INSERT into service_providers (sp_code, sp_name, 
		sp_city_code, sp_area_code, sp_number, sp_password, sp_service_type_code, sp_delivery_time,
		 sp_min_order, seller_name, shop_address, shop_open_time, shop_close_time,
		  sp_latitude, sp_longitude) 
		 (select ?,?,?,?,?,?,?,?,?,?,?,?,?, Y(area_geocode), X(area_geocode) from area_details where area_code = ?)");

        $delivery_time_mins = $delivery_time." mins";
		$stmt->bind_param("isiissisissiii",$sp_code,$this->conn->real_escape_string($shop_name), $shop_city, $defaultArea, $seller_phone,
			$this->conn->real_escape_string($password), $shop_type, $delivery_time_mins, $min_delivery_amt,
			  $this->conn->real_escape_string($seller_name), $this->conn->real_escape_string($shop_address), $shop_open_time,
			  $shop_close_time, $shop_area );
		
		if(FALSE== $stmt->execute() )
			return $this->getServerFailureJsonResponse(0);
		$stmt->close();
		 
		$sql = null;
		foreach($JsonServiceAreasArray as $eachArea)
		{
			$sql[] = '("'.$sp_code.'", "'.$eachArea["area_code"].'", "'.$delivery_time." mins".'", "'.$min_delivery_amt.'")';
		}
		$query = "INSERT into service_providers_delivery_details (sp_code, area_code,
		sp_delivery_time, sp_min_order) values ".implode(',', $sql);
		 
		if(FALSE== $this->conn->query($query) )
			return $this->getServerFailureJsonResponse(1);
			
 		
		$sql = array();
		$i = 0;
        $baseInsertQueryforInventory = "INSERT into items_offered_by_service_provider (item_code,sp_code, item_cost, item_status ) values ";
		foreach($JsonInventoryDetailsArray as $eachItem)
		{
			$query = "SELECT * from items_offered_by_service_provider where item_code = '".$eachItem["item_code"]."' LIMIT 1";
			if(FALSE== ($result = $this->conn->query($query)) )
				return $this->getServerFailureJsonResponse(2);
			if(0 < $result->num_rows){
				$row = $result->fetch_array();

                $item_status=1;
                $inventoryEntry = '("'.$eachItem["item_code"].'", "'.$sp_code.'", "'.$row["item_cost"].', "'.$item_status.'")';
												
				if($inventoryEntry != null)
					$sql[] = $inventoryEntry;
			}
			$result->close();
			
			$i = $i + 1;
			
			if($i == 300)
			{
				if($sql != null){
					$query = $baseInsertQueryforInventory.implode(',', $sql);
					if(FALSE== $this->conn->query($query) )
						return $this->getServerFailureJsonResponse(3);
				}
				$i = 0;
				$sql = array();
			}
		}
		if($i != 0 && $sql != null)
		{
			$query = $baseInsertQueryforInventory.implode(',', $sql);
			if(FALSE== $this->conn->query($query) )
				return $this->getServerFailureJsonResponse(3);
		}

		$response["error"] = 1;
		$response["register_code"] = 1;
		return $response;
    		 
    }
    
    public function PostCompleteRegistrationDetails_v2($JsonServiceAreasArray, $seller_name, $seller_phone, $password,
     $shop_name, $shop_address, $shop_city, $shop_area, $shop_type, $shop_open_time, $shop_close_time, 
     $delivery_time, $min_delivery_amt){
    		 
		$response = array();
		$response["error"] = 1;   // 1 for false
		
		if($seller_phone == null)
		{
			return $this->getServerFailureJsonResponse();
		}
		
		if(!$this->isServProvUniqueIDPresent($seller_phone))
		{
			$response["error"] = 1;
			$response["register_code"] = 2;
			return $response;
		}
		
		$stmt = $this->conn->prepare("SELECT sp_code from service_providers ORDER BY id DESC LIMIT 1");
		if(FALSE== $stmt->execute() )
			return $this->getServerFailureJsonResponse(0);
		$stmt->bind_result($sp_code);
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		if($num_rows > 0)
			$stmt->fetch();
		$stmt->close();
		
		if($num_rows > 0) $sp_code = intval($sp_code) + 1;
		else $sp_code  = 1;

		
		$stmt = $this->conn->prepare("INSERT into service_providers (sp_code, sp_name, 
		sp_city_code, sp_area_code, sp_number, sp_password, sp_service_type_code, sp_delivery_time,
		 sp_min_order, seller_name, shop_address, shop_open_time, shop_close_time,
		  sp_latitude, sp_longitude) 
		 (select ?,?,?,?,?,?,?,?,?,?,?,?,?, Y(area_geocode), X(area_geocode) from area_details where area_code = ?)");

        $delivery_time_mins = $delivery_time." mins";
		$stmt->bind_param("isiissisissiii",$sp_code,$this->conn->real_escape_string($shop_name), $shop_city, $shop_area, $seller_phone,
			$this->conn->real_escape_string($password), $shop_type, $delivery_time_mins, $min_delivery_amt,
			  $this->conn->real_escape_string($seller_name),$this->conn->real_escape_string($shop_address), $shop_open_time,
			  $shop_close_time, $shop_area );
		
		if(FALSE== $stmt->execute() )
			return $this->getServerFailureJsonResponse(0);
		$stmt->close();
		 
		$sql = null;
		foreach($JsonServiceAreasArray as $eachArea)
		{
			$sql[] = '("'.$sp_code.'", "'.$eachArea["area_code"].'", "'.$delivery_time." mins".'", "'.$min_delivery_amt.'")';
		}
		$query = "INSERT into service_providers_delivery_details (sp_code, area_code,
		sp_delivery_time, sp_min_order) values ".implode(',', $sql);
		 
		if(FALSE== $this->conn->query($query) )
			return $this->getServerFailureJsonResponse(1);
			
 		
 		$responseInventory = $this->GetBasicInventoryforShopType($shop_type);
 		if($responseInventory["error"] == 1)
 			$JsonInventoryDetailsArray = $responseInventory["items"];
 		else
 			$JsonInventoryDetailsArray = array();
 		
		$sql = array();
		$i = 0;
        $baseInsertQueryforInventory = "INSERT into items_offered_by_service_provider (item_code,sp_code, item_cost, item_status ) values ";
		foreach($JsonInventoryDetailsArray as $eachItem)
		{
			$query = "SELECT * from items_offered_by_service_provider where item_code = '".$eachItem["item_code"]."' LIMIT 1";
			if(FALSE== ($result = $this->conn->query($query)) )
				return $this->getServerFailureJsonResponse(2);
			if(0 < $result->num_rows){
				$row = $result->fetch_array();

                $item_status=1;
                $inventoryEntry = '("'.$eachItem["item_code"].'", "'.$sp_code.'", "'.$row["item_cost"].', "'.$item_status.'")';
								
				if($inventoryEntry != null)
					$sql[] = $inventoryEntry;
			}
			$result->close();
			
			$i = $i + 1;
			
			if($i == 300)
			{
				if($sql != null){
					$query = $baseInsertQueryforInventory.implode(',', $sql);
					if(FALSE== $this->conn->query($query) )
						return $this->getServerFailureJsonResponse(3);
				}
				$i = 0;
				$sql = array();
			}
		}
		if($i != 0 && $sql != null)
		{
			$query = $baseInsertQueryforInventory.implode(',', $sql);
			if(FALSE== $this->conn->query($query) )
				return $this->getServerFailureJsonResponse(3);
		}

		$response["error"] = 1;
		$response["register_code"] = 1;
		return $response;
    		 
    }
    
    public function PostCompleteRegistrationDetails_v3($JsonServiceAreasArray, $seller_name, $seller_phone, 
     $phone_validated, $password, $shop_name, $shop_address, $shop_city, $shop_area, $shop_type, $shop_open_time,
     $shop_close_time, $delivery_time, $min_delivery_amt, $shop_open_bits, $inventory_by_us){
    		 
		$response = array();
		$response["error"] = 1;   // 1 for false
		
		if($seller_phone == null)
		{
			return $this->getServerFailureJsonResponse();
		}
		
		if(!$this->isServProvUniqueIDPresent($seller_phone))
		{
			$response["error"] = 1;
			$response["register_code"] = 2;
			return $response;
		}
		
		$stmt = $this->conn->prepare("SELECT sp_code from service_providers ORDER BY id DESC LIMIT 1");
		if(FALSE== $stmt->execute() )
			return $this->getServerFailureJsonResponse(0);
		$stmt->bind_result($sp_code);
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		if($num_rows > 0)
			$stmt->fetch();
		$stmt->close();
		
		if($num_rows > 0) $sp_code = intval($sp_code) + 1;
		else $sp_code  = 1;

		//Seller shop details
		$stmt = $this->conn->prepare("INSERT into service_providers (sp_code, sp_name, 
		sp_city_code, sp_area_code, sp_number, sp_number_val_flag, sp_password, sp_service_type_code, sp_delivery_time,
		 sp_min_order, seller_name, shop_address, shop_open_time, shop_close_time, shop_open_bits, inventory_by_us,
		  sp_latitude, sp_longitude) 
		 (select ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, Y(area_geocode), X(area_geocode) from area_details where area_code = ?)");

        $delivery_time_mins = $delivery_time." mins";
		$stmt->bind_param("isiisisisissiiiii",$sp_code,$this->conn->real_escape_string($shop_name), $shop_city, $shop_area, $seller_phone,
			$phone_validated, $this->conn->real_escape_string($password), $shop_type, $delivery_time_mins, $min_delivery_amt,
			  $this->conn->real_escape_string($seller_name),$this->conn->real_escape_string($shop_address), $shop_open_time,
			  $shop_close_time, $shop_open_bits, $inventory_by_us, $shop_area );
		
		if(FALSE== $stmt->execute() )
			return $this->getServerFailureJsonResponse(0);
		$stmt->close();
		 
		 
		//Seller Service areas 
		$sql = null;
		foreach($JsonServiceAreasArray as $eachArea)
		{
			$sql[] = '("'.$sp_code.'", "'.$eachArea["area_code"].'", "'.$delivery_time." mins".'", "'.$min_delivery_amt.'")';
		}
		$query = "INSERT into service_providers_delivery_details (sp_code, area_code,
		sp_delivery_time, sp_min_order) values ".implode(',', $sql);
		 
		if(FALSE== $this->conn->query($query) )
			return $this->getServerFailureJsonResponse(1);
			
 		
 		//Seller's basic Inventory
 		
 		$responseInventory = $this->GetBasicInventoryforShopType($shop_type);
 		if($responseInventory["error"] == 1)
 			$JsonInventoryDetailsArray = $responseInventory["items"];
 		else
 			$JsonInventoryDetailsArray = array();
 		
		$sql = array();
		$i = 0;
        $baseInsertQueryforInventory = "INSERT into items_offered_by_service_provider (item_code,sp_code, item_cost, item_status ) values ";
		foreach($JsonInventoryDetailsArray as $eachItem)
		{
			$query = "SELECT * from items_offered_by_service_provider where item_code = '".$eachItem["item_code"]."' LIMIT 1";
			if(FALSE== ($result = $this->conn->query($query)) )
				return $this->getServerFailureJsonResponse(2);
			if(0 < $result->num_rows){
				$row = $result->fetch_array();

                $item_status=1;
                $inventoryEntry = '("'.$eachItem["item_code"].'", "'.$sp_code.'", "'.$row["item_cost"].', "'.$item_status.'")';
				if($inventoryEntry != null)
					$sql[] = $inventoryEntry;
			}
			$result->close();
			
			$i = $i + 1;
			
			if($i == 300)
			{
				if($sql != null){
					$query = $baseInsertQueryforInventory.implode(',', $sql);
					if(FALSE== $this->conn->query($query) )
						return $this->getServerFailureJsonResponse(3);
				}
				$i = 0;
				$sql = array();
			}
		}
		if($i != 0 && $sql != null)
		{
			$query = $baseInsertQueryforInventory.implode(',', $sql);
			if(FALSE== $this->conn->query($query) )
				return $this->getServerFailureJsonResponse(3);
		}

		$response["error"] = 1;
		$response["register_code"] = 1;
		return $response;
    		 
    }
    
    public function PostCompleteRegistrationDetails_v4($JsonServiceAreasArray, $seller_name, $seller_phone, 
     $phone_validated, $password, $shop_name, $shop_address, $shop_city, $shop_area, $shop_type, $shop_open_time,
     $shop_close_time, $delivery_time, $min_delivery_amt, $shop_open_bits, $inventory_by_us
     , $logistics_services, $sodexo_coupons, $midnight_delivery){
    		 
		$response = array();
		$response["error"] = 1;   // 1 for false
		
		if($seller_phone == null)
		{
			return $this->getServerFailureJsonResponse(0);
		}

		if(!$this->isServProvUniqueIDPresent($seller_phone))
		{
			$response["error"] = 1;
			$response["register_code"] = 2;
			return $response;
		}
		
		$stmt = $this->conn->prepare("SELECT sp_code from service_providers ORDER BY id DESC LIMIT 1");
		if(FALSE== $stmt->execute() )
			return $this->getServerFailureJsonResponse(0);
		$stmt->bind_result($sp_code);
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		if($num_rows > 0)
			$stmt->fetch();
		$stmt->close();
		
		if($num_rows > 0) $sp_code = intval($sp_code) + 1;
		else $sp_code  = 1;

		//Seller shop details
		$stmt = $this->conn->prepare("INSERT into service_providers (sp_code, sp_name, 
		sp_city_code, sp_area_code, sp_number, sp_number_val_flag, sp_password, sp_service_type_code, sp_delivery_time,
		 sp_min_order, seller_name, shop_address, shop_open_time, shop_close_time, shop_open_bits, inventory_by_us
		 , logistics_services, sodexo_coupons, midnight_delivery, sp_latitude, sp_longitude) 
		 (select ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, Y(area_geocode), X(area_geocode) from area_details where area_code = ?)");

        $delivery_time_mins = $delivery_time." mins";
		$stmt->bind_param("isiisisisissiiiiiiii",$sp_code,$this->conn->real_escape_string($shop_name), $shop_city, $shop_area, $seller_phone,
			$phone_validated, $this->conn->real_escape_string($password), $shop_type, $delivery_time_mins, $min_delivery_amt,
			  $this->conn->real_escape_string($seller_name), $this->conn->real_escape_string($shop_address), $shop_open_time,
			  $shop_close_time, $shop_open_bits, $inventory_by_us
			  , $logistics_services, $sodexo_coupons, $midnight_delivery, $shop_area );
		
		if(FALSE== $stmt->execute() )
			return $this->getServerFailureJsonResponse(0);
		$stmt->close();
		 
		 
		//Seller Service areas 
		$sql = null;
		foreach($JsonServiceAreasArray as $eachArea)
		{
			$sql[] = '("'.$sp_code.'", "'.$eachArea["area_code"].'", "'.$delivery_time." mins".'", "'.$min_delivery_amt.'")';
		}
		$query = "INSERT into service_providers_delivery_details (sp_code, area_code,
		sp_delivery_time, sp_min_order) values ".implode(',', $sql);
		 
		if(FALSE== $this->conn->query($query) )
			return $this->getServerFailureJsonResponse(1);
			
 		
 		//Seller's basic Inventory
 		
 		$responseInventory = $this->GetBasicInventoryforShopType($shop_type);
 		if($responseInventory["error"] == 1)
 			$JsonInventoryDetailsArray = $responseInventory["items"];
 		else
 			$JsonInventoryDetailsArray = array();
 		
		$sql = array();
		$i = 0;
        $baseInsertQueryforInventory = "INSERT into items_offered_by_service_provider (item_code,sp_code, item_cost, item_status ) values ";
		foreach($JsonInventoryDetailsArray as $eachItem)
		{
			$query = "SELECT * from items_offered_by_service_provider where item_code = '".$eachItem["item_code"]."' LIMIT 1";
			if(FALSE== ($result = $this->conn->query($query)) )
				return $this->getServerFailureJsonResponse(2);
			if(0 < $result->num_rows){
				$row = $result->fetch_array();

                $item_status=1;
                $inventoryEntry = '("'.$eachItem["item_code"].'", "'.$sp_code.'", "'.$row["item_cost"].', "'.$item_status.'")';
				
				if($inventoryEntry != null)
					$sql[] = $inventoryEntry;
			}
			$result->close();
			
			$i = $i + 1;
			
			if($i == 300)
			{
				if($sql != null){
					$query = $baseInsertQueryforInventory.implode(',', $sql);
					if(FALSE== $this->conn->query($query) )
						return $this->getServerFailureJsonResponse(3);
				}
				$i = 0;
				$sql = array();
			}
		}
		if($i != 0 && $sql != null)
		{
			$query = $baseInsertQueryforInventory.implode(',', $sql);
			if(FALSE== $this->conn->query($query) )
				return $this->getServerFailureJsonResponse(3);
		}

		$response["error"] = 1;
		$response["register_code"] = 1;
		return $response;
    		 
    }

    public function PostCompleteRegistrationDetails_v5($JsonServiceAreasArray, $seller_name, $seller_phone, 
     $phone_validated, $password, $shop_name, $shop_address, $shop_city, $shop_area, $shop_type, $shop_open_time,
     $shop_close_time, $delivery_time, $min_delivery_amt, $shop_open_bits, $inventory_by_us
     , $logistics_services, $sodexo_coupons, $midnight_delivery, $referral_code, $verification_source){
    		 
		$response = array();
		$response["error"] = 1;   // 1 for false
		
		if($seller_phone == null)
		{
			return $this->getServerFailureJsonResponse(0);
		}

		if(!$this->isServProvUniqueIDPresent($seller_phone))
		{
			$response["error"] = 1;
			$response["register_code"] = 2;
			return $response;
		}
		
		$stmt = $this->conn->prepare("SELECT sp_code from service_providers ORDER BY id DESC LIMIT 1");
		if(FALSE== $stmt->execute() )
			return $this->getServerFailureJsonResponse(0);
		$stmt->bind_result($sp_code);
		$stmt->store_result();
		$num_rows = $stmt->num_rows;
		if($num_rows > 0)
			$stmt->fetch();
		$stmt->close();
		
		if($num_rows > 0) $sp_code = intval($sp_code) + 1;
		else $sp_code  = 1;

		//Seller shop details
		$stmt = $this->conn->prepare("INSERT into service_providers (sp_code, sp_name, 
		sp_city_code, sp_area_code, sp_number, sp_number_val_flag, sp_password, sp_service_type_code, sp_delivery_time,
		 sp_min_order, seller_name, shop_address, shop_open_time, shop_close_time, shop_open_bits, inventory_by_us
		 , logistics_services, sodexo_coupons, midnight_delivery, referral_code_used, verification_source,sp_latitude, sp_longitude) 
		 (select ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,Y(area_geocode), X(area_geocode) from area_details where area_code = ?)");

        $delivery_time_mins = $delivery_time." mins";
		$stmt->bind_param("isiisisisissiiiiiiisi",$sp_code,$this->conn->real_escape_string($shop_name), $shop_city, $shop_area, $seller_phone,
			$phone_validated, $this->conn->real_escape_string($password), $shop_type, $delivery_time_mins, $min_delivery_amt,
			  $this->conn->real_escape_string($seller_name), $this->conn->real_escape_string($shop_address), $shop_open_time,
			  $shop_close_time, $shop_open_bits, $inventory_by_us
			  , $logistics_services, $sodexo_coupons, $midnight_delivery, $referral_code, $verification_source, $shop_area );
		
		if(FALSE== $stmt->execute() )
			return $this->getServerFailureJsonResponse(0);
		$stmt->close();
		 
		 
		//Seller Service areas 
		$sql = null;
		foreach($JsonServiceAreasArray as $eachArea)
		{
			$sql[] = '("'.$sp_code.'", "'.$eachArea["area_code"].'", "'.$delivery_time." mins".'", "'.$min_delivery_amt.'")';
		}
		$query = "INSERT into service_providers_delivery_details (sp_code, area_code,
		sp_delivery_time, sp_min_order) values ".implode(',', $sql);
		 
		if(FALSE== $this->conn->query($query) )
			return $this->getServerFailureJsonResponse(1);
			
 		
 		//Seller's basic Inventory
 		
 		$responseInventory = $this->GetBasicInventoryforShopType($shop_type);
 		if($responseInventory["error"] == 1)
 			$JsonInventoryDetailsArray = $responseInventory["items"];
 		else
 			$JsonInventoryDetailsArray = array();
 		
		$sql = array();
		$i = 0;	
		$baseInsertQueryforInventory = "INSERT into items_offered_by_service_provider (item_code,sp_code, item_cost, item_status ) values ";
		foreach($JsonInventoryDetailsArray as $eachItem)
		{
			$query = "SELECT * from items_offered_by_service_provider where item_code = '".$eachItem["item_code"]."' LIMIT 1";
			if(FALSE== ($result = $this->conn->query($query)) )
				return $this->getServerFailureJsonResponse(2);
			if(0 < $result->num_rows){
				$row = $result->fetch_array();
			    $item_status=1;
				$inventoryEntry = '("'.$eachItem["item_code"].'", "'.$sp_code.'", "'.$row["item_cost"].', "'.$item_status.'")';
				
				if($inventoryEntry != null)
					$sql[] = $inventoryEntry;
			}
			$result->close();
			
			$i = $i + 1;
			
			if($i == 300)
			{
				if($sql != null){
					$query = $baseInsertQueryforInventory.implode(',', $sql);
					if(FALSE== $this->conn->query($query) )
						return $this->getServerFailureJsonResponse(3);
				}
				$i = 0;
				$sql = array();
			}
		}
		if($i != 0 && $sql != null)
		{
			$query = $baseInsertQueryforInventory.implode(',', $sql);
			if(FALSE== $this->conn->query($query) )
				return $this->getServerFailureJsonResponse(3);
		}

		$this->performReferralSeller($referral_code, $sp_code);

		$response["error"] = 1;
		$response["register_code"] = 1;
		return $response;
    		 
    }
    
    private function performReferralSeller($referral_code, $sp_code){
    	if(null == $referral_code || "" == $referral_code)
    		return true;
    	return $this->performReferral($referral_code, $sp_code, "1");
    }
    
    private function performReferralCustomer($referral_code, $user_code){
    	if(null == $referral_code || "" == $referral_code)
    		return true;
    	return $this->performReferral($referral_code, $user_code, "0");
    }
    
    private function performReferral($referral_code, $owner_code, $owner_type){
    	$transaction_params = array(
			"user_code" => $owner_code,
			"owner_type" => $owner_type,
			"referral_code" => $referral_code
			);
		$net_api = $this->getNetworkApi();

		$response = $net_api->performReferral($transaction_params);
		if(null == $response || false == $response['success'])
			$settlement_succeed = false;
		else
			$settlement_succeed = true;
		
		return $settlement_succeed;
    }
    
    private function getServerFailureJsonResponse($level = 0){
    	$response = array();
    	$response["error"] = 0;
    	$response["sql_error"] = "";
    	$response["level"] = 0;
    	if(0 != $level) {
    		$response["level"] = $level;
    		$response["sql_error"] = $this->conn->error;
    	}
		return $response;
    }
    
    private function defaultAreaCodeforCity($city_code){
    	$city_code = intval($city_code);
    	
    	switch($city_code)
    	{
    		case 1 : return 1;
    		case 3 : return 80;
    		case 4 : return 121;
    		case 5 : return 124;
    		default : return 1; // for gurgaon
    	}
    }
 
 	public function getServiceTypesforCityforBuyers($cityCode, $userCode){
        $response = array();
        $response["error"] = 1;   // 1 for false
        $response["service_types"] = array();
        $stmt = $this->conn->prepare("SELECT  st.st_code, st.st_name, st.st_img_flag, st.st_img_add,
            sts.seller_automated, st.items_common, st.is_service FROM service_types st, service_type_status sts
            where st.st_code = sts.st_code and sts.city_code = ? and
            sts.launched_for_buyer = TRUE and st.is_service = 0 order by st.sort_order ");

		$stmt->bind_param("i", $cityCode);
        $stmt->execute();

        $stmt->bind_result( $stcode, $stname, $st_img_flag, $st_img_add, $seller_automated, $items_common, $is_service);

		$id = 1;
        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["st_code"] = $stcode;
            $tmp["st_name"] = $stname;
            $tmp["st_img_flag"] = $st_img_flag;
            $tmp["st_img_add"] = $st_img_add;
            $tmp["seller_automated"] = $seller_automated;
            $tmp["items_common"] = $items_common;
            $tmp["is_service"] = $is_service;
            array_push($response["service_types"], $tmp);

            $id = $id +1;
        }
        
        if(0 != $userCode){
			$stmt = $this->conn->prepare("update user_details set user_current_city = $cityCode where user_code = $userCode");
			$stmt->execute();
        }
        
        return $response;
    }

    public function getServiceTypesforCityforBuyers_v2($cityCode, $userCode){
        $response = array();
        $response["error"] = 1;   // 1 for false
        $response["service_types"] = array();
        $stmt = $this->conn->prepare("SELECT  st.st_code, st.st_name, st.st_img_flag, st.st_img_add,
            sts.seller_automated, st.items_common, st.is_service FROM service_types st, service_type_status sts
            where st.st_code = sts.st_code and sts.city_code = ? and
            sts.launched_for_buyer = TRUE order by st.sort_order ");

        $stmt->bind_param("i", $cityCode);
        $stmt->execute();

        $stmt->bind_result( $stcode, $stname, $st_img_flag, $st_img_add, $seller_automated, $items_common, $is_service);

        $id = 1;
        // looping through result and preparing tasks array
        while ($stmt->fetch()) {
            $tmp = array();
            $tmp["id"] = $id;
            $tmp["st_code"] = $stcode;
            $tmp["st_name"] = $stname;
            $tmp["st_img_flag"] = $st_img_flag;
            $tmp["st_img_add"] = $st_img_add;
            $tmp["seller_automated"] = $seller_automated;
            $tmp["items_common"] = $items_common;
            $tmp["is_service"] = $is_service;
            array_push($response["service_types"], $tmp);

            $id = $id +1;
        }
        
        if(0 != $userCode){
            $stmt = $this->conn->prepare("update user_details set user_current_city = $cityCode where user_code = $userCode");
            $stmt->execute();
        }
        
        return $response;
    }
    
    public function getCurrentOrdersLimited($serv_prov_id, $reg_id, $before_order_string){
    	$before_order_code = $this->getOrderCodeFromOrderString($before_order_string);
    
    	$order_count_to_send = 5;
    	
		if($this->isServiceProviderValid($serv_prov_id)){
				$response = array();
				$response["error"] = 1;
				$response["current_orders_service_providers"] = array();
				
				$query = "SELECT os.order_string, obd.expected_del_time, uad.user_address, ud.user_code, obd.order_time,
                cod.post_total_amount, uad.user_name, uad.user_phone_number
                FROM order_in_progress oin, order_basic_details obd, user_details ud, user_address_details uad, 
                order_strings os, confirmed_order_details cod
                WHERE os.o_id = obd.id
                AND oin.sp_code = ?
                AND oin.status =  ?
                AND obd.order_code = oin.order_code
                AND obd.user_code = ud.user_code
                AND uad.user_code = obd.del_add_user_code
                AND uad.user_address_code = obd.del_add_code
                AND cod.order_code=oin.order_code";
				
				if(intval($before_order_code) == 0)
					$order_condition = "";
				else 
					$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
				$sort_condition = "order by CAST(obd.order_code as UNSIGNED) desc ";
				$limit_condition = " limit $order_count_to_send ";
				
				$final_query = $query.$order_condition.$sort_condition.$limit_condition;

				$stmt = $this->conn->prepare($final_query);
				$status="Pending";
				$stmt->bind_param("ss", $serv_prov_id, $status);

				$stmt->execute();

				$stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber);

				// looping through result and preparing tasks array
				while ($stmt->fetch()) {
					$tmp = array();
					$tmp["current_order_code"] = $ordercode;
					$tmp["order_time"] = $orderdate;
					$tmp["delivery_address"] = $deladdress;
					$tmp["user_code"] = $usercode;
					$tmp["total_amount"] = $ordertotamount;
					$tmp["expected_del_time"] = $expecteddeltime;
					$tmp["sp_code"]=$serv_prov_id;
					$tmp["user_name"]=$username;
					$tmp["user_phone_number"]=$userphonenumber;
					array_push($response["current_orders_service_providers"], $tmp);
				}
 				$stmt->close();
				//$this->pushServiceProviderOrderNotification($serv_prov_id, $reg_id);
				return $response;
		}
		else{
				$response = array();
				$response["message"] = "Service Provider Doesn't Exists";
				return $response;
		}
	}
	
	public function getCurrentOrdersLimited_v2($serv_prov_id, $reg_id, $before_order_string){
	$before_order_code = $this->getOrderCodeFromOrderString($before_order_string);

	$order_count_to_send = 5;
	
	if($this->isServiceProviderValid($serv_prov_id)){
			$response = array();
			$response["error"] = 1;
			$response["current_orders_service_providers"] = array();
			$query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , uad.user_address, 
            ud.user_code, obd.order_time, cod.post_total_amount, uad.user_name, uad.user_phone_number
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, 
            service_providers sp, user_details ud, order_strings os, confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.sp_code = ?
            AND sp.sp_code = oin.sp_code
            AND oin.status = ?
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND obd.user_code = ud.user_code
            AND cod.order_code=obd.order_code";
			
			$whether_now_or_not = " and UNIX_TIMESTAMP(NOW()) > (UNIX_TIMESTAMP(obd.expected_del_time)
			 - REPLACE(  sp.sp_delivery_time ,  ' mins',  '' )*60) ";
			
			if(intval($before_order_code) == 0)
				$order_condition = "";
			else 
				$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
			$sort_condition = " order by CAST(obd.order_code as UNSIGNED) desc ";
			$limit_condition = " limit $order_count_to_send ";
			
			$final_query = $query.$whether_now_or_not.$order_condition.$sort_condition.$limit_condition;

			$stmt = $this->conn->prepare($final_query);
			$status="Pending";
			$stmt->bind_param("ss", $serv_prov_id, $status);

			$stmt->execute();

			$stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber);

			// looping through result and preparing tasks array
			while ($stmt->fetch()) {
				$tmp = array();
				$tmp["current_order_code"] = $ordercode;
				$tmp["order_time"] = $orderdate;
				$tmp["delivery_address"] = $deladdress;
				$tmp["user_code"] = $usercode;
				$tmp["total_amount"] = $ordertotamount;
				$tmp["expected_del_time"] = $expecteddeltime;
				$tmp["sp_code"]=$serv_prov_id;
				$tmp["user_name"]=$username;
				$tmp["user_phone_number"]=$userphonenumber;
				array_push($response["current_orders_service_providers"], $tmp);
			}
			$stmt->close();
			//$this->pushServiceProviderOrderNotification($serv_prov_id, $reg_id);
			return $response;
		}
		else{
				$response = array();
				$response["error"] = 0;
				$response["message"] = "Service Provider Doesn't Exists";
				return $response;
		}
	}
		
	public function getPendingOrdersLimited($serv_prov_id, $reg_id, $before_order_string){
		$before_order_code = $this->getOrderCodeFromOrderString($before_order_string);
		
    	$order_count_to_send = 10;
    	
		if($this->isServiceProviderValid($serv_prov_id)){
            $response = array();
            $response["error"] = 1;
            $response["pending_orders_service_providers"] = array();
           
            $query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , uad.user_address, 
            ud.user_code, obd.order_time, cod.post_total_amount, uad.user_name, uad.user_phone_number
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, 
            service_providers sp, user_details ud, order_strings os, confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.sp_code = ?
            AND sp.sp_code = oin.sp_code
            AND oin.status =  ?
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND obd.user_code = ud.user_code
            AND cod.order_code=obd.order_code";
			if(intval($before_order_code) == 0)
				$order_condition = "";
			else 
				$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
			$sort_condition = "order by CAST(obd.order_code as UNSIGNED) desc ";
			$limit_condition = " limit $order_count_to_send ";
			
			$final_query = $query.$order_condition.$sort_condition.$limit_condition;

			$stmt = $this->conn->prepare($final_query);
			$status="Confirmed";
            $stmt->bind_param("ss", $serv_prov_id, $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber);



            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["pending_order_code"] = $ordercode;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["user_code"] = $usercode;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["expected_del_time"] = $expecteddeltime;
                $tmp["sp_code"]=$serv_prov_id;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                array_push($response["pending_orders_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
	}
	
	public function getPendingOrdersLimited_v2($serv_prov_id, $reg_id, $before_order_string){
		$before_order_code = $this->getOrderCodeFromOrderString($before_order_string);
	
		$order_count_to_send = 10;
	
		if($this->isServiceProviderValid($serv_prov_id)){
            $response = array();
            $response["error"] = 1;
            $response["pending_orders_service_providers"] = array();
           
            $query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , uad.user_address,
                ud.user_code, obd.order_time, cod.post_total_amount, uad.user_name, uad.user_phone_number, 
                lod.logistics_services_used
                FROM order_in_progress oin, order_basic_details obd, user_address_details uad, 
                service_providers sp, user_details ud, order_strings os, confirmed_order_details cod, 
                logistics_order_details lod
                WHERE os.o_id = obd.id
                AND oin.sp_code = ?
                AND sp.sp_code = oin.sp_code
                AND oin.status =  ?
                AND obd.order_code = oin.order_code
                AND uad.user_code = obd.del_add_user_code
                AND uad.user_address_code = obd.del_add_code
                AND obd.user_code = ud.user_code
                AND cod.order_code = obd.order_code
                AND lod.order_code = obd.order_code";
			
			$whether_now_or_not = " and UNIX_TIMESTAMP(NOW()) > (UNIX_TIMESTAMP(obd.expected_del_time) - 5400
			 - REPLACE(  sp.sp_delivery_time ,  ' mins',  '' )*60) ";
			
			if(intval($before_order_code) == 0)
				$order_condition = "";
			else 
				$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
			$sort_condition = "order by CAST(obd.order_code as UNSIGNED) desc ";
			$limit_condition = " limit $order_count_to_send ";
			
			$final_query = $query.$whether_now_or_not.$order_condition.$sort_condition.$limit_condition;

			$stmt = $this->conn->prepare($final_query);
			$status="Confirmed";
            $stmt->bind_param("ss", $serv_prov_id, $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber, $logisticused);



            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["pending_order_code"] = $ordercode;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["user_code"] = $usercode;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["expected_del_time"] = $expecteddeltime;
                $tmp["sp_code"]=$serv_prov_id;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                $tmp["logistics_used"] = $logisticused;
                array_push($response["pending_orders_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
	}
	
	public function getScheduledOrdersLimited($serv_prov_id, $reg_id, $before_order_string){
		$before_order_code = $this->getOrderCodeFromOrderString($before_order_string);
		
		$order_count_to_send = 5;
	
		if($this->isServiceProviderValid($serv_prov_id)){
			$response = array();
			$response["error"] = 1;
			$response["scheduled_orders_service_providers"] = array();
			
			$query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , uad.user_address, ud.user_code, obd.order_time, cod.post_total_amount, uad.user_name, uad.user_phone_number
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, service_providers sp, user_details ud, order_strings os, confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.sp_code = ?
            AND sp.sp_code = oin.sp_code
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND ud.user_code = obd.user_code
            AND cod.order_code = obd.order_code";
			
			$order_status_condition = " and oin.status in (?,?) ";
			
			$whether_now_or_not = " and UNIX_TIMESTAMP(NOW()) < (UNIX_TIMESTAMP(obd.expected_del_time)
			 - REPLACE(  sp.sp_delivery_time ,  ' mins',  '' )*60) ";
			
			if(intval($before_order_code) == 0)
				$order_condition = "";
			else 
				$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
			$sort_condition = "order by CAST(obd.order_code as UNSIGNED) desc ";
			$limit_condition = " limit $order_count_to_send ";
			
			$final_query = $query.$order_status_condition.$whether_now_or_not.$order_condition
				.$sort_condition.$limit_condition;

			$stmt = $this->conn->prepare($final_query);

			$status_confirmed="Confirmed";
			$status_pending = "Pending";
            $stmt->bind_param("sss", $serv_prov_id, $status_pending, $status_confirmed);

			$stmt->execute();

			$stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $orderdate, $ordertotamount, $username, $userphonenumber);

			// looping through result and preparing tasks array
			while ($stmt->fetch()) {
				$tmp = array();
				$tmp["scheduled_order_code"] = $ordercode;
				$tmp["order_time"] = $orderdate;
				$tmp["delivery_address"] = $deladdress;
				$tmp["user_code"] = $usercode;
				$tmp["total_amount"] = $ordertotamount;
				$tmp["expected_del_time"] = $expecteddeltime;
				$tmp["sp_code"]=$serv_prov_id;
				$tmp["user_name"]=$username;
				$tmp["user_phone_number"]=$userphonenumber;
				array_push($response["scheduled_orders_service_providers"], $tmp);
			}
			$stmt->close();
			//$this->pushServiceProviderOrderNotification($serv_prov_id, $reg_id);
			return $response;
		}
		else{
				$response = array();
				$response["message"] = "Service Provider Doesn't Exists";
				return $response;
		}
	}
	
	public function getScheduledOrdersLimited_v2($serv_prov_id, $reg_id, $before_order_string){
		$before_order_code = $this->getOrderCodeFromOrderString($before_order_string);
	
		$order_count_to_send = 5;
	
		if($this->isServiceProviderValid($serv_prov_id)){
			$response = array();
			$response["error"] = 1;
			$response["scheduled_orders_service_providers"] = array();
			
			$query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , 
            uad.user_address, ud.user_code, obd.order_time, cod.post_total_amount, oin.status, 
            uad.user_name, uad.user_phone_number
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, 
            service_providers sp, user_details ud, order_strings os, confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.sp_code = ?
            AND sp.sp_code = oin.sp_code
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND ud.user_code = obd.user_code
            AND cod.order_code = obd.order_code ";
			
			$order_status_condition = " and oin.status in (?,?) ";
			
			$whether_now_or_not = " and UNIX_TIMESTAMP(NOW()) < (UNIX_TIMESTAMP(obd.expected_del_time)
			 - REPLACE(  sp.sp_delivery_time ,  ' mins',  '' )*60) ";
			
			if(intval($before_order_code) == 0)
				$order_condition = "";
			else 
				$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
			$sort_condition = "order by CAST(obd.order_code as UNSIGNED) desc ";
			$limit_condition = " limit $order_count_to_send ";
			
			$final_query = $query.$order_status_condition.$whether_now_or_not
				.$order_condition.$sort_condition.$limit_condition;

			$stmt = $this->conn->prepare($final_query);

			$status_confirmed="Confirmed";
			$status_pending = "Pending";
            $stmt->bind_param("sss", $serv_prov_id, $status_pending, $status_confirmed);

			$stmt->execute();

			$stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $orderdate, $ordertotamount, $orderstatus,
			 $username, $userphonenumber);

			// looping through result and preparing tasks array
			while ($stmt->fetch()) {
				$tmp = array();
				$tmp["scheduled_order_code"] = $ordercode;
				$tmp["order_time"] = $orderdate;
				$tmp["order_status"] = $orderstatus;
				$tmp["delivery_address"] = $deladdress;
				$tmp["user_code"] = $usercode;
				$tmp["total_amount"] = $ordertotamount;
				$tmp["expected_del_time"] = $expecteddeltime;
				$tmp["sp_code"]=$serv_prov_id;
				$tmp["user_name"]=$username;
				$tmp["user_phone_number"]=$userphonenumber;
				array_push($response["scheduled_orders_service_providers"], $tmp);
			}
			$stmt->close();
			//$this->pushServiceProviderOrderNotification($serv_prov_id, $reg_id);
			return $response;
		}
		else{
				$response = array();
				$response["message"] = "Service Provider Doesn't Exists";
				return $response;
		}
	}
	
	public function getScheduledOrdersForLazyLadsLimited($before_order_string){
		$before_order_code = $this->getOrderCodeFromOrderString($before_order_string);
	
		$order_count_to_send = 10;
    
            $response = array();
            $response["error"] = 1;
            $response["scheduled_orders_service_providers"] = array();
            
            $query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , uad.user_address, 
                ud.user_code, ud.phone_number, obd.order_time, cod.post_total_amount, uad.user_name, 
                uad.user_phone_number, sps.sp_name
                FROM order_in_progress oin, order_basic_details obd, user_address_details uad, 
                service_providers sps, user_details ud, order_strings os, confirmed_order_details cod
                WHERE os.o_id = obd.id
                AND obd.order_code = oin.order_code
                AND uad.user_code = obd.del_add_user_code
                AND uad.user_address_code = obd.del_add_code
                AND ud.user_code = obd.user_code
                AND sps.sp_code = oin.sp_code
                AND cod.order_code = obd.order_code";
            
            $order_status_condition = " and oin.status in (?,?) ";
            
            $whether_now_or_not = " and UNIX_TIMESTAMP(NOW()) < (UNIX_TIMESTAMP(obd.expected_del_time)
			 - REPLACE(  sps.sp_delivery_time ,  ' mins',  '' )*60) ";
			
			if(intval($before_order_code) == 0)
				$order_condition = "";
			else 
				$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
			$sort_condition = "order by CAST(obd.order_code as UNSIGNED) desc ";
			$limit_condition = " limit $order_count_to_send ";
			
			$final_query = $query.$order_status_condition.$whether_now_or_not.$order_condition
				.$sort_condition.$limit_condition;
            
            $stmt = $this->conn->prepare($final_query);
            
            $status_confirmed="Confirmed";
			$status_pending = "Pending";
            $stmt->bind_param("ss", $status_pending, $status_confirmed);
            $stmt->execute();

            $stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $phonenumber, $orderdate,
             $ordertotamount, $username, $userphonenumber, $serv_prov_name);



            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["scheduled_order_code"] = $ordercode;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["user_code"] = $usercode;
                $tmp["phone_number"] = $phonenumber;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["expected_del_time"] = $expecteddeltime;
                $tmp["sp_name"]=$serv_prov_name;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                array_push($response["scheduled_orders_service_providers"], $tmp);
            }
            //$this->pushServiceProviderOrderNotification($serv_prov_id, $reg_id);
            return $response;
		
	}

	public function getScheduledOrdersForLazyLadsLimited_v2($before_order_string, $order_filter){
		$before_order_code = $this->getOrderCodeFromOrderString($before_order_string);
	
		$order_count_to_send = 10;
    
            $response = array();
            $response["error"] = 1;
            $response["scheduled_orders_service_providers"] = array();
            
            $query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , uad.user_address,
            ud.user_code, ud.phone_number, obd.order_time, cod.post_total_amount, oin.status, 
            uad.user_name, uad.user_phone_number, sps.sp_name, obd.coupon_code, lod.logistics_services_used,
             lod.rider_assigned, lod.rider_accepted
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad,
            service_providers sps, user_details ud, order_strings os, logistics_order_details lod, 
            confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND ud.user_code = obd.user_code
            AND sps.sp_code = oin.sp_code
            AND cod.order_code = obd.order_code
            AND lod.order_code = obd.order_code";
            
            if($order_filter && isset($order_filter[self::ORDER_FILTER_CITIES]) &&
            	count($order_filter[self::ORDER_FILTER_CITIES]) ){
            	$order_city_condition = " and uad.user_city in (select city_name from city_details where city_code 
            	in (".implode(',',$order_filter[self::ORDER_FILTER_CITIES]).")) ";
            	/*$order_city_condition = "";	*/
            }
            else
            	$order_city_condition = "";
            
            $order_status_condition = " and oin.status in (?,?) ";
            $whether_now_or_not = " and UNIX_TIMESTAMP(NOW()) < (UNIX_TIMESTAMP(obd.expected_del_time) - 5400
			 - REPLACE(  sps.sp_delivery_time ,  ' mins',  '' )*60) ";
			
			if(intval($before_order_code) == 0)
				$order_condition = "";
			else 
				$order_condition = " and CAST(obd.order_code as UNSIGNED) < $before_order_code ";
			$sort_condition = "order by CAST(obd.order_code as UNSIGNED) desc ";
			$limit_condition = " limit $order_count_to_send ";
			
			$final_query = $query.$order_city_condition.$order_status_condition.$whether_now_or_not.$order_condition
				.$sort_condition.$limit_condition;
            
            $stmt = $this->conn->prepare($final_query);

            $status_confirmed="Confirmed";
			$status_pending = "Pending";
            $stmt->bind_param("ss", $status_pending, $status_confirmed);
            $stmt->execute();

            $stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $phonenumber, $orderdate, $ordertotamount, $orderstatus,
            	 $username, $userphonenumber, $serv_prov_name, $coupon_code, $rider_requested, $rider_assigned, $rider_accepted);

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["scheduled_order_code"] = $ordercode;
                $tmp["order_time"] = $orderdate;
                $tmp["order_status"] = $orderstatus;
                $tmp["delivery_address"] = $deladdress;
                $tmp["user_code"] = $usercode;
                $tmp["phone_number"] = $phonenumber;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["expected_del_time"] = $expecteddeltime;
                $tmp["sp_name"]=$serv_prov_name;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                $tmp["coupon_code"] = $coupon_code;
                $tmp["rider_requested"] = $rider_requested;
                $tmp["rider_assigned"] = $rider_assigned;
                $tmp["rider_accepted"] = $rider_accepted;
                
                array_push($response["scheduled_orders_service_providers"], $tmp);
            }
            //$this->pushServiceProviderOrderNotification($serv_prov_id, $reg_id);
            return $response;
		
	}
	
	public function getSearchOrdersForLazyLadsLimited($search_string, $previous_order_string, $order_filter){
		$previous_order_code = $this->getOrderCodeFromOrderString($previous_order_string);
		$search_order_code = $this->getOrderCodeFromOrderString($search_string);
	
		$order_count_to_send = 10;
    
		$response = array();
		$response["error"] = 1;
		$response["orders_service_providers"] = array();
		
		$query = "SELECT os.order_string, UNIX_TIMESTAMP( obd.expected_del_time ) , uad.user_address, 
        ud.user_code, ud.phone_number, obd.order_time, cod.post_total_amount, oin.status, 
        uad.user_name, uad.user_phone_number, sps.sp_name, obd.coupon_code, 
        REPLACE( sps.sp_delivery_time,  ' mins',  '' ) *60 AS sps_sp_delivery_time, 
        lod.logistics_services_used, lod.rider_assigned, lod.rider_accepted
        FROM order_in_progress oin, order_basic_details obd, user_address_details uad, 
        service_providers sps, user_details ud, order_strings os, confirmed_order_details cod, 
        logistics_order_details lod
        WHERE os.o_id = obd.id
        AND obd.order_code = oin.order_code
        AND uad.user_code = obd.del_add_user_code
        AND uad.user_address_code = obd.del_add_code
        AND ud.user_code = obd.user_code
        AND sps.sp_code = oin.sp_code ";
		
		if(!$search_string || trim($search_string) == "")
			$search_condition = "";
		else
			$search_condition = " and (obd.order_code = ?) ";
		
		if($order_filter && isset($order_filter[self::ORDER_FILTER_CITIES]) &&
            	count($order_filter[self::ORDER_FILTER_CITIES]) ){
            $order_city_condition = " and uad.user_city in (select city_name from city_details where city_code 
            	in (".implode(',',$order_filter[self::ORDER_FILTER_CITIES]).")) ";
            /*$order_city_condition = "";	*/
        }
        else
            $order_city_condition = "";
        
		if(!$previous_order_code || intval($previous_order_code) == 0)
			$order_condition = "";
		else 
			$order_condition = " and CAST(obd.order_code as UNSIGNED) < $previous_order_code ";
			
		$sort_condition = " order by CAST(obd.order_code as UNSIGNED) desc ";
		$limit_condition = " limit $order_count_to_send ";
		
		$final_query = $query.$search_condition.$order_city_condition.$order_condition.$sort_condition.$limit_condition;

		$stmt = $this->conn->prepare($final_query);
		
		if(! (!$search_string || trim($search_string) == "") ){
			$query_ordercode = $search_order_code;
// 			$query_phonenumber = $search_string;
			$stmt->bind_param("s", $query_ordercode);
		}

		$stmt->execute();

		$stmt->bind_result($ordercode, $expecteddeltime, $deladdress, $usercode, $phonenumber, $orderdate, $ordertotamount, $orderstatus,
			 $username, $userphonenumber, $serv_prov_name, $coupon_code, $serv_prov_del_time, 
			 $rider_requested, $rider_assigned, $rider_accepted);

		// looping through result and preparing tasks array
		while ($stmt->fetch()) {
			$tmp = array();
			$tmp["scheduled_order_code"] = $ordercode;
			$tmp["order_time"] = $orderdate;
			$tmp["order_status"] = $orderstatus;
			$tmp["delivery_address"] = $deladdress;
			$tmp["user_code"] = $usercode;
			$tmp["phone_number"] = $phonenumber;
			$tmp["total_amount"] = $ordertotamount;
			$tmp["expected_del_time"] = $expecteddeltime;
			$tmp["sp_name"]=$serv_prov_name;
			$tmp["sp_del_time"] = $serv_prov_del_time;
			$tmp["user_name"]=$username;
			$tmp["user_phone_number"]=$userphonenumber;
			$tmp["coupon_code"] = $coupon_code;
			$tmp["rider_requested"] = $rider_requested;
			$tmp["rider_assigned"] = $rider_assigned;
			$tmp["rider_accepted"] = $rider_accepted;
			
			array_push($response["orders_service_providers"], $tmp);
		}
		//$this->pushServiceProviderOrderNotification($serv_prov_id, $reg_id);
		return $response;
		
	}
	
	public function createOfferLazylads($offer_tag, $offer_desc, $offer_launch_time, 
	$offer_validity, $offer_img_flag, $offer_img_add, $shall_show){
		$response = array();
		$response["error"] = 1;
		 
		if(	$offer_tag == null ||
			$offer_desc == null ||
			$offer_validity == null||
			($offer_launch_time != null && strtotime($offer_launch_time) == FALSE) ||
			intval($offer_validity) == 0
		)
		{
			$response["error"] = 0;
			$response["message"] = "Paremeters failed Validation";
			return $response;
		}
		
		$baseinsertquery = "insert into offer_details";
		$variable_names = "offer_tag, offer_desc";
		$variable_values = "'$offer_tag', '".$this->conn->real_escape_string($offer_desc)."'";
		
		if($offer_launch_time != null)
		{
			$variable_names .= ", offer_launch_time";
			$variable_values .= ", ".strtotime($offer_launch_time);
		}
		
		$variable_names .= ", offer_validity";
		$variable_values .= ", '".intval($offer_validity)."'";
		
		if($offer_img_flag != null && $offer_img_add != null)
		{
			$variable_names .= ", offer_img_flag";
			$variable_values .= ", '".(intval($offer_img_flag) == 0 ? "0" : "1")."'";
			
			$variable_names .= ", offer_img_add";
			$variable_values .= ", '".$this->conn->real_escape_string($offer_img_add)."'";
			
		}
		
		if($shall_show != null)
		{
			$variable_names .= ", shall_show";
			$variable_values .= ", ".(intval($shall_show) == 0? "0":"1")."'";
		}
		
		$query = $baseinsertquery."(".$variable_names.") values(".$variable_values.")";
		
		$stmt = $this->conn->prepare($query);
		$result = $stmt->execute();
		if($result)
		{
			$response["error"] = 1;
			$response["message"] = "Offer Created";
		}
		else {
			$response["error"] = 0;
			$response["message"] = "Offer could not Created";
		}
		
		return $response;
	}
	
	//php format "D M d, Y G:i" matches to mysql "%a %b %d, %Y %k:%i"
	public function getDeliveredOrdersforDayDate($serv_prov_id,$day_of_orders,$date_of_orders){
		date_default_timezone_set("Asia/Kolkata");
		if($serv_prov_id == null || 
			($day_of_orders == null && $date_of_orders == null)
			)
			{
				$response = array();
            	$response["error"] = 0;
            	$response["message"] = "Parameters not as required";
            	return $response;
			}
		if($this->isServiceProviderValid($serv_prov_id)){
		
			if($day_of_orders != null && intval($day_of_orders) != -1)
			{
				$orders_date = date("Y/m/d",strtotime("-$day_of_orders days"));
			}
			else
				$orders_date = date("Y/m/d",strtotime($date_of_orders));
		
            $response = array();
            $response["error"] = 1;
            $response["delivered_orders_service_providers"] = array();
            
            $base_select_query = "SELECT os.order_string, obd.expected_del_time, uad.user_address, ud.user_code, obd.order_time, cod.post_total_amount, uad.user_name, uad.user_phone_number
            FROM order_in_progress oin, order_basic_details obd, user_address_details uad, user_details ud, order_strings os, confirmed_order_details cod
            WHERE os.o_id = obd.id
            AND oin.sp_code = ?
            AND oin.status =  ?
            AND obd.order_code = oin.order_code
            AND uad.user_code = obd.del_add_user_code
            AND uad.user_address_code = obd.del_add_code
            AND ud.user_code = obd.user_code
            AND cod.order_code = obd.order_code";
            
            $order_date_query = " and date_format(obd.order_time , '%Y/%m/%d') = '$orders_date' ";
			$sorting_order = " order by CAST(obd.order_code as UNSIGNED) desc ";
			
            $final_query = $base_select_query . $order_date_query . $sorting_order;
            $stmt = $this->conn->prepare($final_query);

            $status="Delivered";

            $stmt->bind_param("ss", $serv_prov_id, $status);

            $stmt->execute();

            $stmt->bind_result($ordercode, $deliverytime, $deladdress, $usercode, $orderdate, $ordertotamount,
             $username, $userphonenumber);

            // looping through result and preparing tasks array
            while ($stmt->fetch()) {
                $tmp = array();
                $tmp["delivered_order_code"] = $ordercode;
                $tmp["delivery_time"] = $deliverytime;
                //$tmp["total_time_taken"] = $tottimetaken;
                $tmp["order_time"] = $orderdate;
                $tmp["delivery_address"] = $deladdress;
                $tmp["total_amount"] = $ordertotamount;
                $tmp["sp_code"]=$serv_prov_id;
                $tmp["user_name"]=$username;
                $tmp["user_phone_number"]=$userphonenumber;
                array_push($response["delivered_orders_service_providers"], $tmp);
            }
            return $response;
        }
        else{
            $response = array();
            $response["error"] = 0;
            $response["message"] = "Service Provider Doesn't Exists";
            return $response;
        }
	}
	
	public function addNewAddressDetailedAndGetAddressCode($user_code, $customer_name, $customer_number,
	 $customer_email, $customer_flat,$customer_subarea,$customer_area,$customer_city, $address_lat, $address_long){
    	$usercode=intval($user_code);
        $response = array();

		$customer_address = "";
		if($customer_flat != null && $customer_flat != "")
			$customer_address .= $customer_flat.",";
		if($customer_flat != null && $customer_flat != "")
			$customer_address .= $customer_subarea.",";
		
		$customer_address .= $customer_area.",".$customer_city;
							
        if($usercode==0){
            /*
             *
             * First Time User
             * */
            $stmt="select id, user_code from user_details ORDER BY id DESC LIMIT 1";
            $result=mysqli_query($this->conn, $stmt);
            if(mysqli_num_rows($result)==0){
                /*
                 * First User
                 * */
                $userid=1;
                $stmt_insert_user_details="insert into user_details(user_code) values ('".$userid."')";
                $result_insert_user_details=mysqli_query($this->conn, $stmt_insert_user_details);
                if($result_insert_user_details){
                    $addressCode=1;
                    $stmt_insert_user_address="insert into user_address_details(user_code, user_address_code,
                     user_flat, user_subarea, user_area, user_city, user_address, user_name, user_email_id, user_phone_number,
                     address_lat, address_long)
                      values ($userid, $addressCode, '".$customer_flat."', '".$customer_subarea."', '"
                      .$customer_area."', '".$customer_city."', '".$customer_address."', '".$customer_name."', '"
                      .$customer_email."', '".$customer_number."', ".(($address_lat == null)?"NULL":$address_lat).", ".(($address_long == null)?"NULL":$address_long)." )";
                   
                    $result_insert_user_address=mysqli_query($this->conn, $stmt_insert_user_address);
                    if($result_insert_user_address){
                        $response["error"] = 1;
                        $response["user_details"] = array();
                        $tmp = array();
                        $tmp["user_code"] = $userid;
                        $tmp["user_address_code"] = $addressCode;
                        array_push($response["user_details"], $tmp);
                        return $response;
                    }else{
                        $response["error"] = 0;
                        $response["user_details"] = array();
                        $tmp = array();
                        $tmp["message"] = "Error";
                        array_push($response["user_details"], $tmp);
                        //TODO error handling
                    }
                }else{
                    $response["error"] = 0;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["message"] = "Error";
                    array_push($response["user_details"], $tmp);
                    //TODO Error Handling
                }
            }else{
                /*
                 * Not First User
                 * */
                $row_user_code_fetch=mysqli_fetch_array($result);
                $userid=$row_user_code_fetch['user_code'];
                $userid=$userid+1;
                $stmt_insert_user_details="insert into user_details(user_code) values ('".$userid."')";
                $result_insert_user_details=mysqli_query($this->conn, $stmt_insert_user_details);
                if($result_insert_user_details){
                    $addressCode=1;
                    $stmt_insert_user_address="insert into user_address_details(user_code, user_address_code,
                     user_flat, user_subarea, user_area, user_city, user_address, user_name, user_email_id, user_phone_number,
                     address_lat, address_long)
                      values ($userid, $addressCode, '".$customer_flat."', '".$customer_subarea."', '"
                      .$customer_area."', '".$customer_city."', '".$customer_address."', '".$customer_name."', '"
                      .$customer_email."', '".$customer_number."', ".(($address_lat == null)?"NULL":$address_lat).", ".(($address_long == null)?"NULL":$address_long)." )";
                    
                    $result_insert_user_address=mysqli_query($this->conn, $stmt_insert_user_address);
                    if($result_insert_user_address){
                        $response["error"] = 1;
                        $response["user_details"] = array();
                        $tmp = array();
                        $tmp["user_code"] = $userid;
                        $tmp["user_address_code"] = $addressCode;
                        array_push($response["user_details"], $tmp);
                        return $response;
                    }else{
                        $response["error"] = 0;
                        $response["user_details"] = array();
                        $tmp = array();
                        $tmp["message"] = "Error";
                        array_push($response["user_details"], $tmp);
                        //TODO error handling
                    }
                }else{
                    $response["error"] = 0;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["message"] = "Error";
                    array_push($response["user_details"], $tmp);
                    //TODO Error Handling
                }
            }
        }else{
            /*
             * Not First Time user
             * */
            $stmt_select_user_address="select id, user_address_code from user_address_details where user_code=$usercode ORDER BY id DESC LIMIT 1";
            $result_select_user_address=mysqli_query($this->conn, $stmt_select_user_address);

            if(mysqli_num_rows($result_select_user_address)==0){
                $address_code=1;
                $stmt_insert_user_add="insert into user_address_details(user_code, user_address_code,
                     user_flat, user_subarea, user_area, user_city, user_address, user_name, user_email_id, user_phone_number,
                     address_lat, address_long)
                      values ($usercode, $address_code, '".$customer_flat."', '".$customer_subarea."', '"
                      .$customer_area."', '".$customer_city."', '".$customer_address."', '".$customer_name."', '"
                      .$customer_email."', '".$customer_number."', ".(($address_lat == null)?"NULL":$address_lat).", ".(($address_long == null)?"NULL":$address_long)." )";
                    
                $result_insert_user_add=mysqli_query($this->conn, $stmt_insert_user_add);
                if($result_insert_user_add){
                    $response["error"] = 1;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["user_code"] = $usercode;
                    $tmp["user_address_code"] = $address_code;
                    array_push($response["user_details"], $tmp);
                    return $response;
                }else{
                    $response["error"] = 0;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["message"] = "Error";
                    array_push($response["user_details"], $tmp);
                    //TODO error handling
                }
            }else{
                $row_address_code_fetch=mysqli_fetch_array($result_select_user_address);
                $address_code=$row_address_code_fetch['user_address_code'];
                $address_code=$address_code+1;
                $stmt_insert_user_add="insert into user_address_details(user_code, user_address_code,
                     user_flat, user_subarea, user_area, user_city, user_address, user_name, user_email_id, user_phone_number,
                     address_lat, address_long)
                      values ($usercode, $address_code, '".$customer_flat."', '".$customer_subarea."', '"
                      .$customer_area."', '".$customer_city."', '".$customer_address."', '".$customer_name."', '"
                      .$customer_email."', '".$customer_number."', ".(($address_lat == null)?"NULL":$address_lat).", ".(($address_long == null)?"NULL":$address_long)." )";
                
                $result_insert_user_add=mysqli_query($this->conn, $stmt_insert_user_add);
                if($result_insert_user_add){
                    $response["error"] = 1;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["user_code"] = $usercode;
                    $tmp["user_address_code"] = $address_code;
                    array_push($response["user_details"], $tmp);
                    return $response;
                }else{
                    $response["error"] = 0;
                    $response["user_details"] = array();
                    $tmp = array();
                    $tmp["message"] = "Error";
                    array_push($response["user_details"], $tmp);
                    //TODO error handling
                }
            }
        }
	}
	
	public function getAllUserAddresses($user_code){
		$response = array();
        $response["error"] = 1;
        $response["user_addresses"] = array();
        
        $query = "SELECT uad.user_code, uad.user_address_code, uad.user_flat, uad.user_subarea, uad.user_area,
        			 IF(STRCMP(uad.user_city,'') = 0, uad.user_address, uad.user_city),
        			 uad.user_name, uad.user_email_id, uad.user_phone_number, uad.address_lat, uad.address_long 
                     FROM user_address_details uad, user_details ud
					WHERE uad.user_code = ud.user_code
					AND (
					ud.user_code = ?
					OR ud.phone_number = ( 
					SELECT phone_number
					FROM user_details
					WHERE user_code = ? )
					) AND
					uad.shall_show = 1";
					
		$stmt = $this->conn->prepare($query);
        $stmt->bind_param("ss", $user_code, $user_code);
        $result = $stmt->execute();
        //echo $result;
			if($result){
			$stmt->bind_result($user_code, $user_address_code, $user_flat, $user_subarea, $user_area,
        			 $user_city, $user_name, $user_email_id, $user_phone_number, $address_lat, $address_long);

			// looping through result and preparing tasks array
			while ($stmt->fetch()) {
				$tmp = array();
				$tmp["user_code"] = $user_code;
				$tmp["user_address_code"] = $user_address_code;
				$tmp["user_flat"] = $user_flat;
				$tmp["user_subarea"] = $user_subarea;
				$tmp["user_area"] = $user_area;
				$tmp["user_city"] = $user_city;
				$tmp["user_name"] = $user_name;
				$tmp["user_email_id"] = $user_email_id;
				$tmp["user_phone_number"] = $user_phone_number;
				$tmp["address_lat"] = $address_lat;
				$tmp["address_long"] = $address_long;
				array_push($response["user_addresses"], $tmp);
			}
        }
        else
        	$response["error"] = 0;

        return $response;
	}
	
	public function editUserAddressDetailed($user_code, $address_code, $customer_name, $customer_number,
	 $customer_email, $customer_flat,$customer_subarea,$customer_area,$customer_city){

        $response = array();

		$customer_address = "";
		if($customer_flat != null && $customer_flat != "")
			$customer_address .= $customer_flat.",";
		if($customer_flat != null && $customer_flat != "")
			$customer_address .= $customer_subarea.",";
		
		$customer_address .= $customer_area.",".$customer_city;

		$stmt_update_user_add="update user_address_details set
			 user_flat = '".$customer_flat."', user_subarea = '".$customer_subarea."',
			  user_area = '".$customer_area."',user_city = '".$customer_city."', user_address = '".$customer_address."',
			   user_name = '".$customer_name."', user_email_id = '".$customer_email."', user_phone_number = '".$customer_number."' 
			  where user_code = $user_code and user_address_code = $address_code";
			
		$result_update_user_add=mysqli_query($this->conn, $stmt_update_user_add);
		if($result_update_user_add){
			$response["error"] = 1;
			
		}else{
			$response["error"] = 0;
		}
		return $response;

    }
    
    public function editUserAddressDetailedSynced($changing_user_code, $user_code, $address_code, $customer_name, $customer_number,
	 $customer_email, $customer_flat,$customer_subarea,$customer_area,$customer_city, $address_lat, $address_long){

        $response = array();

		$customer_address = "";
		if($customer_flat != null && $customer_flat != "")
			$customer_address .= $customer_flat.",";
		if($customer_flat != null && $customer_flat != "")
			$customer_address .= $customer_subarea.",";
		
		$customer_address .= $customer_area.",".$customer_city;

		$stmt_update_user_add="update user_address_details set
			 user_flat = '".$customer_flat."', user_subarea = '".$customer_subarea."',
			  user_area = '".$customer_area."',user_city = '".$customer_city."', user_address = '".$customer_address."',
			   user_name = '".$customer_name."', user_email_id = '".$customer_email."', user_phone_number = '".$customer_number."'" ;
			  
		if(null != $address_lat)
			$stmt_update_user_add .= ", address_lat = $address_lat ";
		if(null != $address_long)
			$stmt_update_user_add .= ", address_long = $address_long ";
		
		$where_condition = " where user_code = $user_code and user_address_code = $address_code";
		$stmt_update_user_add .= $where_condition;
			
		$result_update_user_add=mysqli_query($this->conn, $stmt_update_user_add);
		if($result_update_user_add){
			$response["error"] = 1;
			
		}else{
			$response["error"] = 0;
		}
		return $response;

    }
    
    public function deleteUserAddress($user_code,$address_code){
    	$response = array();

		$stmt_delete_user_add="update user_address_details set shall_show = 0
			  where user_code = $user_code and user_address_code = $address_code";
			
		$result_delete_user_add=mysqli_query($this->conn, $stmt_delete_user_add);
		if($result_delete_user_add){
			$response["error"] = 1;
			
		}else{
			$response["error"] = 0;
		}
		return $response;
    }
    
    public function deleteUserAddressSynced($changing_user_code, $user_code,$address_code){
    	$response = array();

		$stmt_delete_user_add="update user_address_details set shall_show = 0
			  where user_code = $user_code and user_address_code = $address_code";
			
		$result_delete_user_add=mysqli_query($this->conn, $stmt_delete_user_add);
		if($result_delete_user_add){
			$response["error"] = 1;
			
		}else{
			$response["error"] = 0;
		}
		return $response;
    }
    
    //php format "D M d, Y G:i" matches to mysql "%a %b %d, %Y %k:%i"
    public function offersListLimited($before_offer_code){
    
    	$offer_count_to_send = 10;
    
    	$response = array();
		$response["error"] = 1;
		$response["offer_details"] = array();
		
		$current_datetime = mktime();
	   
		$query = "select offer_code, offer_desc, UNIX_TIMESTAMP(offer_launch_time) as offer_launch_timestamp, 
			offer_validity, offer_img_flag,
			offer_img_add from offer_details where 1";
		if(intval($before_offer_code) == 0)
			$where_order_condition = " ";
		else 
			$where_order_condition = " and CAST(offer_code as UNSIGNED) < $before_offer_code ";
		$shall_show_condition = " and shall_show = 1 ";
		$where_launched_date_condition = " and UNIX_TIMESTAMP(offer_launch_time) < UNIX_TIMESTAMP(NOW()) ";
		$sort_condition = " order by CAST(offer_code as UNSIGNED) desc ";
		$limit_condition = " limit $offer_count_to_send ";
		
		$final_query = $query.$where_order_condition.$shall_show_condition.$where_launched_date_condition
			.$sort_condition.$limit_condition;

		$stmt = $this->conn->prepare($final_query);

		$result = $stmt->execute();
		if(!$result){
			$response = array();
			$response["error"] = 0;
			return $response;
		}
		
		$stmt->bind_result($offercode, $offerdesc, $offerlaunchtime, $offervalidity, $offerimgflag, $offerimgadd);

		// looping through result and preparing offers array
		while ($stmt->fetch()) {
			$tmp = array();
			$tmp["offer_code"] = $offercode;
			$tmp["offer_desc"] = $offerdesc;
			$tmp["offer_launch_time"] = $offerlaunchtime;
			
			$remainingTime = ($offervalidity*3600) - ($current_datetime - $offerlaunchtime);
			$tmp["offer_validity"] = $remainingTime > 0 ? $remainingTime : 0 ;
			$tmp["offer_img_flag"] = $offerimgflag;
			$tmp["offer_img_add"] = $offerimgadd;
			
			array_push($response["offer_details"], $tmp);

		}
		$stmt->close();
        return $response;
    }
	
	//php format "D M d, Y G:i" matches to mysql "%a %b %d, %Y %k:%i"
    public function activeOffersListLatestOnly(){
    
    	$offer_count_to_send = 10;
    
    	$response = array();
		$response["error"] = 1;
		$response["offer_details"] = array();
		
		$current_datetime = mktime();
	   
		$query = "select offer_img_add from offer_details where 1";
		
		$shall_show_condition = " and shall_show = 1 ";
		$image_present_condition = " and offer_img_flag = 1 ";
		$where_launched_date_condition = " and UNIX_TIMESTAMP(offer_launch_time) < UNIX_TIMESTAMP(NOW()) ";
		$where_active_offer_condition = " and (UNIX_TIMESTAMP(offer_launch_time) + 60*60*offer_validity) > UNIX_TIMESTAMP(NOW()) ";
		$sort_condition = " order by CAST(offer_code as UNSIGNED) desc ";
		$limit_condition = " limit $offer_count_to_send ";
		
		$final_query = $query.$shall_show_condition.$where_launched_date_condition.$where_active_offer_condition
			.$sort_condition.$limit_condition;

		$stmt = $this->conn->prepare($final_query);

		$result = $stmt->execute();
		if(!$result){
			$response = array();
			$response["error"] = 0;
			return $response;
		}
		
		$stmt->bind_result($offerimgadd);

		// looping through result and preparing offers array
		while ($stmt->fetch()) {
			$tmp = array();
			$tmp["offer_img_add"] = $offerimgadd;
			
			array_push($response["offer_details"], $tmp);
		}
		$stmt->close();
        return $response;
    }
    
    public function updateUserProfileDetails($user_code, $user_email, $user_first_name, $user_last_name){
    	$response = array();
    	
    	if(-1 != ($profile_id = $this->CreateorGetProfileId($user_code)) ){
			$query = "update user_profile set ";
			$update = null;
			if(null != $user_email)
				$update[] = " user_email = '".mysqli_real_escape_string($this->conn, $user_email)."' ";
			
			if(null != $user_first_name)
				$update[] = " first_name = '".mysqli_real_escape_string($this->conn, $user_first_name)."' ";
				
			if(null != $user_last_name)
				$update[] = " last_name = '".mysqli_real_escape_string($this->conn, $user_last_name)."' ";
				
			$where_profile_id = " where profile_id = '$profile_id' ";
			
			if(null != $update){
				$final_query = $query.implode(',',$update).$where_profile_id;
				$result = mysqli_query($this->conn, $final_query);
				if($result && 0 != mysqli_affected_rows($this->conn))
					$response["error"] = 1;
				else
					$response["error"] = 0;
			}
			else
				$response["error"] = 1; // no data to update implies success
    	}
    	else
    		$response["error"] = 0;
    		
    	return $response;
    }
    

    public function updateUserProfileDetailsv2($user_code, $user_email, $user_first_name, $user_last_name, $referralCode){
        $response = array();
        if(null != $referralCode){
            $this->performReferralCustomer($referralCode, $user_code);
        }
        if(-1 != ($profile_id = $this->CreateorGetProfileId($user_code)) ){
            $query = "update user_profile set ";
            $update = null;
            if(null != $user_email)
                $update[] = " user_email = '".mysqli_real_escape_string($this->conn, $user_email)."' ";
            
            if(null != $user_first_name)
                $update[] = " first_name = '".mysqli_real_escape_string($this->conn, $user_first_name)."' ";
                
            if(null != $user_last_name)
                $update[] = " last_name = '".mysqli_real_escape_string($this->conn, $user_last_name)."' ";
                
            $where_profile_id = " where profile_id = '$profile_id' ";
            
            if(null != $update){
                $final_query = $query.implode(',',$update).$where_profile_id;
                $result = mysqli_query($this->conn, $final_query);
                if($result && 0 != mysqli_affected_rows($this->conn))
                    $response["error"] = 1;
                else
                    $response["error"] = 1;
            }
            else
                $response["error"] = 1; // no data to update implies success
        }
        else
            $response["error"] = 0;
            
        return $response;
    }


    public function getUserProfileDetails($user_code){
    	$response = array();
    	$response['error'] = 1;
    	
    	if(-1 != ($profile_id = $this->CreateorGetProfileId($user_code)) ){
    		$query = " select user_email, first_name, last_name from user_profile where profile_id = ?";
    		$stmt = $this->conn->prepare($query);
    		$stmt->bind_param("s", $profile_id);
    		$result = $stmt->execute();
    		if($result){
    			$stmt->bind_result($user_email, $user_first_name, $user_last_name);
    			if($stmt->fetch()){
    				$tmp = array();
    				$tmp['user_email'] = $user_email;
    				$tmp['user_first_name'] = $user_first_name;
    				$tmp['user_last_name'] = $user_last_name;
    				
    				$response['user_profile_details'] = $tmp;
    			}
    			else 
    				$response['error'] = 0;
    		}
    		else
    			$response['error'] = 0;
    	}
    	else
    		$response['error'] = 0;
    		
    	return $response;
    }
    
    private function getOrderPaymentDetails($order_code){
    	$payment_params = array(
			"urid" => $order_code,
			);
		$net_api = $this->getNetworkApi();

		$response = $net_api->orderPaymentDetails($payment_params);
		return $response;
    }
    
    public function getOrderDetailsUser($user_code, $order_string){
    	$order_code = $this->getOrderCodeFromOrderString($order_string);
    	
    	$response = array();
    	$response['error'] = 1;
        $net_api = $this->getNetworkApi();
    	
    	$query = "SELECT uad.user_name, uad.user_phone_number, uad.user_address, cod.post_total_amount, obd.discount_amount
            FROM user_address_details uad, order_basic_details obd, confirmed_order_details cod
            WHERE obd.del_add_code = uad.user_address_code
            AND obd.del_add_user_code = uad.user_code
            AND obd.order_code = ?
            AND obd.user_code
            IN (

            SELECT ud1.user_code
            FROM user_details ud1, user_details ud2
            WHERE ud1.phone_number = ud2.phone_number
            AND ud2.user_code = ?
            )
            AND cod.order_code = obd.order_code";
    	
    	$stmt = $this->conn->prepare($query);
    	$stmt->bind_param("ss", $order_code, $user_code);
    	$result = $stmt->execute();
    	
		if($result){
			$stmt->bind_result($user_name,$user_phone_number,$user_address, $order_total_amount, $order_discount_cost);
			if($stmt->fetch()){
				$tmp = array();
				$tmp["user_name"] = $user_name;
				$tmp["user_phone_number"] = $user_phone_number;
				$tmp["user_address"] = $user_address;
				
				$response['order_delivery_details'] = $tmp;
				
				$pay_details = array();
                $order_params = array(
                    "order_code" => $order_code,
                );

                $pay_details['paid'] =  $net_api->orderOfferDiff($order_params);
				$pay_details['to_be_paid'] = $order_total_amount;
				
				$response_payment = $this->getOrderPaymentDetails($order_code);
				
				if(null != $response_payment && true == $response_payment['success']){
					$order_payment_details = $response_payment['order_payments'];
					
					$paid_amount = 0;
					foreach ($order_payment_details as $payment){
						if("Success" == $payment["p_status"])
							$paid_amount += $payment["amount"];
					}
					
					if($paid_amount < $order_total_amount)
						$to_be_paid = $order_total_amount - $paid_amount;
					else 
						$to_be_paid = 0;
						
					$pay_details['paid'] += ($paid_amount + $order_discount_cost);
					$pay_details['to_be_paid'] = $to_be_paid;
					
					$response['order_code'] = $order_string;
				}
				
				$response['order_payment_details'] = $pay_details;
			}
		}
		$stmt->close();
	
		return $response;
    }
    
    public function commentOnSellerRegister($seller_phone, $comment_text){
    	$response = array();
    	
    	$query = "update service_providers set comment_text = ? where sp_number = ? ";
    	$stmt = $this->conn->prepare($query);
    	$stmt->bind_param("ss", $comment_text, $seller_phone);
    	$result = $stmt->execute();
    	if($result)
    		$response["error"] = 1;
    	else
    		$response["error"] = 0;
    		
    	return $response;
    }
    
    public function aboutUsCategoriesforSeller(){
    	$response = array();
    	$response['error'] = 1;
    	$response['info_list'] = array();
    	
    	$query = "select info_id, info_name from about_us_seller where shall_show = 1 order by info_position ";
    	$stmt = $this->conn->prepare($query);
    	$stmt->execute();
    	$stmt->bind_result($info_id, $info_name);
    	while ($stmt->fetch()) {
			$tmp = array();
			$tmp["info_id"] = $info_id;
			$tmp["info_name"] = $info_name;
			
			array_push($response["info_list"], $tmp);
		}
		$stmt->close();
        return $response;
    	
    }
    
    public function aboutUsInfoforId($info_id){
    	$response = array();
    	$response['error'] = 1;
    	
    	$query = "select info_text from about_us_seller where info_id = $info_id";
    	$stmt = $this->conn->prepare($query);
    	$stmt->execute();
    	$stmt->bind_result($info_text);
    	if($stmt->fetch())
    		$response['info_text'] = $info_text;
    	else
    		$response['info_text'] = "";
    		
    	$stmt->close();
        return $response;
    }
    
  //   public function configDetails_v2($user_code, $lazylad_version_code, $user_current_city, $user_current_area, $imei_number,
		// $mac_address, $os_type, $model_name, $phone_manufacturer){
    	
  //   	$query = " update user_details set ";
  //   	$where_user_code = " where user_code = $user_code ";

		// $updateData = null;
		// if(null != $lazylad_version_code)
  //           $lazylad_version_code = (int) $lazylad_version_code;
		// 	$updateData[] = " lazylad_version_code = $lazylad_version_code ";
		// if(null != $user_current_city)
  //           $user_current_city = $user_current_city;
		// 	$updateData[] = " user_current_city = $user_current_city ";
		// if(null != $user_current_area)
  //           $user_current_area =  $user_current_area;
		// 	$updateData[] = " user_current_area = $user_current_area ";
		// if(null != $imei_number)
		// 	$updateData[] = " imei_number = '$imei_number' ";
		// if(null != $mac_address)
		// 	$updateData[] = " mac_address = '$mac_address' ";
		// if(null != $os_type)
  //           $os_type = (int) $os_type;
		// 	$updateData[] = " os_type = $os_type ";
		// if(null != $model_name)
		// 	$updateData[] = " model_name = '$model_name' ";
		// if(null != $phone_manufacturer)
		// 	$updateData[] = " phone_manufacturer = '$phone_manufacturer' ";
		
		// if(null != $updateData){
		// 	$final_query = $query.implode(',',$updateData).$where_user_code;
		// 	$result = mysqli_query($this->conn, $final_query);
		// }
		
		// $response = array();
		// $response['error'] = 1;
		
		// $query = "SELECT config_value_integer, config_value_string, config_name FROM lazylad_config
  //                 WHERE config_name =  'shall_number_verify_customer'
  //                 OR config_name =  'call_us_number'";
		// $stmt = $this->conn->prepare($query);
		// $result = $stmt->execute();
		// $stmt->bind_result($config_value_integer, $config_value_string, $config_name);
		// if($result){
  //           while($stmt->fetch()){
  //               if($config_name == 'shall_number_verify_customer'){
  //                   $response['number_verification'] = array('shall_number_verify' => $config_value_integer);
  //               }
  //               elseif($config_name == 'call_us_number'){
  //                   $response['lazylad_contact'] = array('call_us_number' => $config_value_string);
  //               }
  //           }
		// }
		// else
		// 	$response['number_verification'] = array('shall_number_verify' => 1);
		
		// return $response;
  //   }

        public function configDetails_v2($user_code, $lazylad_version_code, $user_current_city, $user_current_area, $imei_number,
        $mac_address, $os_type, $model_name, $phone_manufacturer){
        
        $config_params = array(
            "user_code"=>$user_code, 
            "lazylad_version_code"=>$lazylad_version_code, 
            "city_code_selected"=>$user_current_city, 
            "area_code_selected"=>$user_current_area, 
            "imei_number"=>$imei_number,
            "mac_address"=>$mac_address, 
            "os_type"=>$os_type, 
            "model_name"=>$model_name, 
            "phone_manufacturer"=>$phone_manufacturer,
        );

        $net_api = $this->getNetworkApi();

        $response = $net_api->config_api_call($config_params);
        return $response;        
    }

    
    
    public function lazyPointsinProfile($user_code){
    	$response = array();
    	$response['error'] = 0;
    	
    	$query = "select lazy_points_balance from user_profile up, user_details ud 
    		where up.phone_number = ud.phone_number and ud.user_code = $user_code ";
    	$result = mysqli_query($this->conn, $query);
    	if($result && ($row = mysqli_fetch_array($result)) ){
    		$lazy_points = $row['lazy_points_balance'];
    		
    		$response['error'] = 1;
    		$response['lazy_points'] = $lazy_points;
    	}
    	
    	return $response;
    }
    
    public function getAllCSCoupons($user_code, $city_code, $area_code, $previous_coupon_id){
    	$coupon_count_to_send = 10;
    
    	$response = array();
		$response["error"] = 1;
		$response["coupons"] = array();
	   
	   	$query = " select cs.coupon_id, coupon_name, coupon_short_desc, coupon_desc, coupon_img_flag, coupon_img_add, lazy_points
	   	 from cs_coupons cs, cs_coupon_city cc where 1 and cs.num_coupons_used < cs.num_coupons and cc.coupon_id = cs.coupon_id and cc.city_code = $city_code 
	   	 and (cc.b_citywide = true or exists(select 1 from cs_coupon_area where coupon_id = cc.coupon_id and city_code = cc.city_code and area_code = $area_code) ) ";
	   
		if(null == $previous_coupon_id ||intval($previous_coupon_id) == 0){
			$where_couponId_condition = " ";
			$limit_condition = " ";
		}
		else {
			$where_couponId_condition = " and CAST(cs.coupon_id as UNSIGNED) > $previous_coupon_id ";
			$limit_condition = " limit $coupon_count_to_send ";
		}
		$shall_show_condition = " and shall_show = 1 ";
		$where_launched_date_condition = " and UNIX_TIMESTAMP(cs.launch_time) < UNIX_TIMESTAMP(NOW()) ";
		$where_end_date_condition = " and UNIX_TIMESTAMP(cs.end_time) > UNIX_TIMESTAMP(NOW()) ";
		$sort_condition = " order by CAST(cs.coupon_id as UNSIGNED) ";
		
		$final_query = $query.$where_couponId_condition.$shall_show_condition.$where_launched_date_condition
			.$where_end_date_condition.$sort_condition.$limit_condition;

		$stmt = $this->conn->prepare($final_query);

		$result = $stmt->execute();
		if(!$result){
			$response = array();
			$response["error"] = 0;
			return $response;
		}
		
		$stmt->bind_result($coupon_id, $coupon_name, $desc, $short_desc, $coupon_img_flag, $coupon_img_add, $lazy_points);

		// looping through result and preparing offers array
		while ($stmt->fetch()) {
			$tmp = array();
			$tmp["coupon_id"] = $coupon_id;
			$tmp["coupon_name"] = $coupon_name;
			$tmp["desc"] = $desc;
			$tmp["short_desc"] = $short_desc;
			$tmp["coupon_img_flag"] = $coupon_img_flag;
			$tmp["coupon_img_add"] = $coupon_img_add;
			$tmp["lazy_points"] = $lazy_points;
			
			array_push($response["coupons"], $tmp);

		}
		$stmt->close();
        return $response;
    }
    
    public function getBoughtCouponsForCustomer($user_code, $previous_bought_id){
    	$coupon_count_to_send = 10;
    
    	$response = array();
		$response["error"] = 1;
		$response["bought_coupons"] = array();
		
		$query = " select css.bought_id, css.coupon_code, cs.coupon_name, cs.coupon_desc, cs.coupon_short_desc,
		cs.coupon_img_flag, cs.coupon_img_add, UNIX_TIMESTAMP(css.validity_time), css.b_redeemed,
		IF(css.b_redeemed || UNIX_TIMESTAMP(css.validity_time) > UNIX_TIMESTAMP(NOW()), 0, 1) as b_expired 
		from cs_coupons_sold css, cs_coupons cs, user_details ud where css.coupon_id = cs.coupon_id and css.buyer_type = 'COUPON_BUYER_CUSTOMER' 
		and css.buyer_urid = ud.phone_number and ud.user_code = $user_code ";
		
		if(null == $previous_bought_id ||intval($previous_bought_id) == 0){
			$where_couponId_condition = " ";
			$limit_condition = " ";
		}
		else {
			$where_couponId_condition = " and CAST(css.bought_id as UNSIGNED) < $previous_bought_id ";
			$limit_condition = " limit $coupon_count_to_send ";
		}

		$sort_condition = " order by CAST(cs.coupon_id as UNSIGNED) desc ";
		
		$final_query = $query.$where_couponId_condition.$sort_condition.$limit_condition;

		$stmt = $this->conn->prepare($final_query);

		$result = $stmt->execute();
		if(!$result){
			$response = array();
			$response["error"] = 0;
			return $response;
		}
		
		$stmt->bind_result($bought_id, $coupon_code, $coupon_name, $desc, $short_desc, $coupon_img_flag, $coupon_img_add,
		 $validity_time, $b_redeemed, $b_expired);

		// looping through result and preparing offers array
		while ($stmt->fetch()) {
			$tmp = array();
			$tmp["bought_id"] = $bought_id;
			$tmp["coupon_code"] = $coupon_code;
			$tmp["coupon_name"] = $coupon_name;
			$tmp["desc"] = $desc;
			$tmp["short_desc"] = $short_desc;
			$tmp["coupon_img_flag"] = $coupon_img_flag;
			$tmp["coupon_img_add"] = $coupon_img_add;
			$tmp["validity_time"] = $validity_time;
			$tmp["b_redeemed"] = $b_redeemed;
			$tmp["b_expired"] = $b_expired;
			
			array_push($response["bought_coupons"], $tmp);
		}
		$stmt->close();
        return $response;
    }
    
    public function getDetailsCSCoupon($user_code, $coupon_id, $city_code, $area_code){
		
		$query = " select coupon_id, coupon_name, coupon_short_desc, coupon_desc, terms, coupon_img_flag, coupon_img_add, lazy_points 
			from cs_coupons where coupon_id = $coupon_id ";
		$stmt = $this->conn->prepare($query);
		
		$result = $stmt->execute();
		if(!$result){
			$response = array();
			$response["error"] = 0;
			return $response;
		}
		
		$stmt->bind_result($coupon_id, $coupon_name, $short_desc, $desc, $terms, $coupon_img_flag, $coupon_img_add, $lazy_points);
		
		if($stmt->fetch()){
			$tmp = array();
			$tmp["coupon_id"] = $coupon_id;
			$tmp["coupon_name"] = $coupon_name;
			$tmp["desc"] = $desc;
			$tmp["short_desc"] = $short_desc;
			$tmp["terms"] = $terms;
			$tmp["coupon_img_flag"] = $coupon_img_flag;
			$tmp["coupon_img_add"] = $coupon_img_add;
			$tmp["lazy_points"] = $lazy_points;
			
			$response = array();
			$response["error"] = 1;
			$response["coupon_details"] = $tmp;
		}
		else{
			$response = array();
			$response["error"] = 0;
		}
		$stmt->close();
		
		return $response;
    }
    
    public function getDetailsBoughtCoupon($user_code, $bought_id){
    	
    	$query = " select css.bought_id, css.coupon_code, cs.coupon_name, cs.coupon_desc, cs.coupon_short_desc, cs.terms,
		cs.coupon_img_flag, cs.coupon_img_add, UNIX_TIMESTAMP(css.validity_time), css.b_redeemed,
		IF(css.b_redeemed || UNIX_TIMESTAMP(css.validity_time) > UNIX_TIMESTAMP(NOW()), 0, 1) as b_expired,
		css.b_reported, css.report_stmt, css.b_replied, css.replied_stmt 
		from cs_coupons_sold css, cs_coupons cs, user_details ud where css.coupon_id = cs.coupon_id and css.bought_id = $bought_id and css.buyer_type = 'COUPON_BUYER_CUSTOMER' 
		and css.buyer_urid = ud.phone_number and ud.user_code = $user_code ";
		
		$stmt = $this->conn->prepare($query);

		$result = $stmt->execute();
		if(!$result){
			$response = array();
			$response["error"] = 0;
			return $response;
		}
		
		$stmt->bind_result($bought_id, $coupon_code, $coupon_name, $desc, $short_desc, $terms, $coupon_img_flag, $coupon_img_add,
		 $validity_time, $b_redeemed, $b_expired, $b_reported, $report_stmt, $b_replied, $replied_stmt);
		 
		if($stmt->fetch()){
			$tmp = array();
			$tmp["bought_id"] = $bought_id;
			$tmp["coupon_code"] = $coupon_code;
			$tmp["coupon_name"] = $coupon_name;
			$tmp["desc"] = $desc;
			$tmp["short_desc"] = $short_desc;
			$tmp["terms"] = $terms;
			$tmp["coupon_img_flag"] = $coupon_img_flag;
			$tmp["coupon_img_add"] = $coupon_img_add;
			$tmp["validity_time"] = $validity_time;
			$tmp["b_redeemed"] = $b_redeemed;
			$tmp["b_expired"] = $b_expired;
			
			$response = array();
			$response["error"] = 1;
			$response["bought_coupon_details"] = $tmp;
		}
		else{
			$response = array();
			$response["error"] = 0;
		}
		$stmt->close();
		
		return $response;
    }
    
    public function buyCSCoupon($user_code, $coupon_id, $city_code, $area_code){
    	$response = array();
    	$response['error'] = 1;
    	
    	$query = " select cs.b_group_coupon, cs.num_coupons_used from cs_coupons cs, cs_coupon_city cc, user_profile up, user_details ud 
    	where cs.coupon_id = $coupon_id and ud.user_code = $user_code and up.phone_number = ud.phone_number  and cc.coupon_id = cs.coupon_id and cc.city_code = $city_code 
	   	 and (cc.b_citywide = true or exists(select 1 from cs_coupon_area where coupon_id = cc.coupon_id and city_code = cc.city_code and area_code = $area_code) )
	   	 and cs.shall_show = 1 and UNIX_TIMESTAMP(cs.launch_time) < UNIX_TIMESTAMP(NOW()) 
	   	 and UNIX_TIMESTAMP(cs.end_time) > UNIX_TIMESTAMP(NOW()) and cs.num_coupons_used < cs.num_coupons 
	   	 and cs.lazy_points <= up.lazy_points_balance ";
	   	 
	   	$stmt = $this->conn->prepare($query);
		$result = $stmt->execute();
		$stmt->bind_result($b_groupCoupon, $num_coupons_used);
		if($result && $stmt->fetch()){
			$stmt->close();
			
			$time  = time();
			if($b_groupCoupon){
				$query = "update cs_coupons cs, cs_group_coupons gcs, user_profile up, user_details ud 
					set cs.num_coupons_used = cs.num_coupons_used + 1, up.lazy_points_balance = up.lazy_points_balance - cs.lazy_points, gcs.b_sold = 1  
					where  cs.coupon_id = $coupon_id and gcs.group_coupon_id = cs.coupon_id and gcs.coupon_num = $num_coupons_used + 1 and ud.user_code = $user_code and up.phone_number = ud.phone_number";
				$stmt = $this->conn->prepare($query);
				$stmt->execute();
				$stmt->close();
			
				$query = " insert into cs_coupons_sold (buyer_urid, coupon_id, coupon_code, buy_time, validity_time, lazy_points_spent)
				(select ud.phone_number, cs.coupon_id, gcs.coupon_code, FROM_UNIXTIME($time), 
				CASE cs.validity_type
				WHEN 'VALIDITY_TYPE_TIME' THEN cs.validity_time
				WHEN 'VALIDITY_TYPE_DAYS' THEN DATE_ADD( FROM_UNIXTIME($time), INTERVAL cs.validity_days DAY )
				END
				,cs.lazy_points  from cs_coupons cs, cs_group_coupons gcs, user_details ud where cs.coupon_id = $coupon_id 
				and gcs.group_coupon_id = cs.coupon_id and gcs.coupon_num = $num_coupons_used + 1 and ud.user_code = $user_code)";
				$stmt = $this->conn->prepare($query);
				$result = $stmt->execute();
				if(!$result || 0 == $stmt->affected_rows){
					$response = array();
					$response["error"] = 0;
				}
				else
					$response['bought_id'] = $stmt->insert_id;
			}
			else{
				$query = "update cs_coupons cs, user_profile up, user_details ud 
					set num_coupons_used = num_coupons_used + 1, up.lazy_points_balance = up.lazy_points_balance - cs.lazy_points
					where  cs.coupon_id = $coupon_id and ud.user_code = $user_code and up.phone_number = ud.phone_number";
				$stmt = $this->conn->prepare($query);
				$stmt->execute();
				$stmt->close();
			
				$query = " insert into cs_coupons_sold (buyer_urid, coupon_id, coupon_code, buy_time, validity_time, lazy_points_spent)
				(select ud.phone_number, cs.coupon_id, cs.coupon_code, FROM_UNIXTIME($time), 
				CASE cs.validity_type
				WHEN 'VALIDITY_TYPE_TIME' THEN cs.validity_time
				WHEN 'VALIDITY_TYPE_DAYS' THEN DATE_ADD( FROM_UNIXTIME($time), INTERVAL cs.validity_days DAY )
				END
				,cs.lazy_points  from cs_coupons cs, user_details ud where coupon_id = $coupon_id and ud.user_code = $user_code )";
				$stmt = $this->conn->prepare($query);
				$result = $stmt->execute();
				if(!$result || 0 == $stmt->affected_rows){
					$response = array();
					$response["error"] = 0;
				}
				else
					$response['bought_id'] = $stmt->insert_id;
			}
		}
		else{
			$response = array();
			$response["error"] = 0;
		}
		$stmt->close();
		return $response;
    }
    
    public function redeemBoughtCoupon($user_code, $bought_id, $city_code, $area_code){
    	$response = array();
    	$response["error"] = 1;
    	
    	$query = " update cs_coupons_sold css, user_details ud, cs_coupons cs, cs_coupon_city cc 
    	set b_redeemed = 1 where css.bought_id = $bought_id and css.buyer_type = 'COUPON_BUYER_CUSTOMER' 
    	and css.buyer_urid = ud.phone_number and ud.user_code = $user_code and cs.coupon_id = css.coupon_id and
    	UNIX_TIMESTAMP(css.validity_time) > UNIX_TIMESTAMP(NOW()) and b_redeemed = 0 
    	and cc.coupon_id = cs.coupon_id and cc.city_code = $city_code and (cc.b_citywide = true or 
    	exists(select 1 from cs_coupon_area where coupon_id = cc.coupon_id and city_code = cc.city_code and area_code = $area_code)
    	 ) ";
    	 $response['query'] = $query;
    	$stmt = $this->conn->prepare($query);
		$result = $stmt->execute();
		if(!$result || 0 == $stmt->affected_rows){
			
			$response["error"] = 0;
		}
		
		return $response;
    }
    
    public function reportProblemBoughtCoupon($user_code, $bought_id, $report_stmt){
    	$response = array();
    	$response["error"] = 1;
    	
    	$query = " update cs_coupons_sold css, user_details ud set css.b_reported = 1, css.report_stmt =  ? 
    	where css.bought_id = $bought_id and css.buyer_type = 'COUPON_BUYER_CUSTOMER' 
    	and css.buyer_urid = ud.phone_number and ud.user_code = $user_code and css.b_reported = 0";
    	
    	$stmt = $this->conn->prepare($query);
    	$stmt->bind_param('s',$report_stmt);
		$result = $stmt->execute();
		if(!$result || 0 == $stmt->affected_rows){
			
			$response["error"] = 0;
		}
		
		return $response;
    }
    
    public function verifyCoupon($coupon_code, $user_code, $total_amount){
        $payment_params = array(
            "user_code" => $user_code,
            "coupon_code" => $coupon_code,
            "total_amount" => $total_amount,
        );
        $net_api = $this->getNetworkApi();

        $response = $net_api->verifyCoupon($payment_params);
        return $response;
     }

    public function pushNotificationsThroughGCM($pass, $reg_ids, $message){
        $response = array();
        if($pass == 42){
            require_once dirname(__FILE__) . '/GCMCUSTOMER.php';
            $gcm=new GCMCUSTOMER();
            $gcm->send_notification($reg_ids, $message);
            $response["error"]=0;
            $response["success"] = true;
            $response["message"] = "Notification Pushed Successfully";
            return $response;
        }
        else{
            $response["error"]=1;
            $response["success"] = false;
            $response["message"] = "Notification Not Pushed";
        }
        return $response;
    }
} 
      
