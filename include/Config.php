                                <?php
/**
 * Database configuration
 */

define('LL_DEVELOPMENT_MACRO', 'DEVELOPMENT');
define('LL_RELEASE_MACRO', 'RELEASE');
define('LL_ENVIRONMENT', LL_RELEASE_MACRO);
 
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
define('DB_HOST', 'localhost');
define('DB_NAME', 'anguluxf_lazylad_ncom_db');

define('USER_CREATED_SUCCESSFULLY', 0);
define('USER_CREATE_FAILED', 1);
define('USER_ALREADY_EXISTED', 2);
?>

                            
