<?php
/**
 * Created by Saurabh Singla.
 * User: Saurabh
 * Date: 10/12/14
 * Time: 9:30 PM
 */
include('GCM.php');
include('GCMCUSTOMER.php');
if (isset($_GET["reg_id"]) && isset($_GET["message"]) && isset($_GET["app"])) {
    $regId = $_GET["reg_id"];
    $message = $_GET["message"];
	$app = $_GET["app"];

	if(1 == $app)
    	$gcm = new GCM();
    else if(0 == $app)
    	$gcm = new GCMCUSTOMER();
    else
    	$gcm = null;

    $registatoin_ids = array($regId);
    $message = array("price" => $message);

	if($gcm)
	    $result = $gcm->send_notification($registatoin_ids, $message);

    echo $result;
}  
?>
